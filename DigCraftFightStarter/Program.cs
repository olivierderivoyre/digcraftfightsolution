﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;

namespace DigCraftFightStarter
{
	static class Program
	{
		/// <summary>
		/// The exe does not start on some PC, look that XNA is installed before starting the game in order to have a nice MsgBox.
		/// </summary>
		[STAThread]
		static void Main()
		{
			try
			{
				if (!isXnaInstalled())
				{
					DialogResult r = MessageBox.Show("XNA 4.0 is not installed.\nIn order to work this program need that you install the \"Microsoft XNA Framework Redistributable 4.0\"." +
						" Do you want to download and install it now?", "XNA 4.0 is not installed.", MessageBoxButtons.YesNo);
					if (r == DialogResult.Yes)
					{
						Process.Start("http://go.microsoft.com/fwlink/?LinkID=148786");
					}
					return;
				}
				if (!File.Exists("./DigCraftFight.exe"))
				{
					throw new Exception("File DigCraftFight.exe not found");
				}
				Process.Start("./DigCraftFight.exe");
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex);
				System.Windows.Forms.MessageBox.Show(ex.ToString(), "Error");
			}
		}
		private static bool isXnaInstalled()
		{
			RegistryKey xnaRegistry = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\XNA\Framework\v4.0");
			if (xnaRegistry == null)
			{
				xnaRegistry = Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Microsoft\XNA\Framework\v4.0");
			}
			if (xnaRegistry == null)
			{
				return false;
			}
			if(!object.Equals(1, xnaRegistry.GetValue("Installed")))
			{
				return false;
			}
			return true;
		}
	}
}
