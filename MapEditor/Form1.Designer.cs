﻿namespace MapEditor
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.gridView = new System.Windows.Forms.DataGridView();
			this.toolStripGeneral = new System.Windows.Forms.ToolStrip();
			this.newButton = new System.Windows.Forms.ToolStripButton();
			this.SaveAsButton = new System.Windows.Forms.ToolStripButton();
			this.saveButton = new System.Windows.Forms.ToolStripButton();
			this.loadButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripItems = new System.Windows.Forms.ToolStrip();
			this.cleanButton = new System.Windows.Forms.ToolStripButton();
			this.panel1 = new System.Windows.Forms.Panel();
			this.propertyGridFurniture = new System.Windows.Forms.PropertyGrid();
			this.propertyGridCarpet = new System.Windows.Forms.PropertyGrid();
			this.applyPGButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
			this.toolStripGeneral.SuspendLayout();
			this.toolStripItems.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// gridView
			// 
			this.gridView.AllowUserToAddRows = false;
			this.gridView.AllowUserToDeleteRows = false;
			this.gridView.AllowUserToResizeColumns = false;
			this.gridView.AllowUserToResizeRows = false;
			this.gridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridView.ColumnHeadersVisible = false;
			this.gridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridView.Location = new System.Drawing.Point(0, 50);
			this.gridView.Name = "gridView";
			this.gridView.ReadOnly = true;
			this.gridView.RowHeadersVisible = false;
			this.gridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.gridView.Size = new System.Drawing.Size(1066, 578);
			this.gridView.TabIndex = 0;
			this.gridView.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.gridView_CellPainting);
			this.gridView.CurrentCellChanged += new System.EventHandler(this.gridView_CurrentCellChanged);
			// 
			// toolStripGeneral
			// 
			this.toolStripGeneral.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newButton,
            this.SaveAsButton,
            this.saveButton,
            this.loadButton});
			this.toolStripGeneral.Location = new System.Drawing.Point(0, 0);
			this.toolStripGeneral.Name = "toolStripGeneral";
			this.toolStripGeneral.Size = new System.Drawing.Size(1330, 25);
			this.toolStripGeneral.TabIndex = 1;
			this.toolStripGeneral.Text = "toolStrip1";
			// 
			// newButton
			// 
			this.newButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.newButton.Image = ((System.Drawing.Image)(resources.GetObject("newButton.Image")));
			this.newButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.newButton.Name = "newButton";
			this.newButton.Size = new System.Drawing.Size(35, 22);
			this.newButton.Text = "New";
			this.newButton.Click += new System.EventHandler(this.newButton_Click);
			// 
			// SaveAsButton
			// 
			this.SaveAsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.SaveAsButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveAsButton.Image")));
			this.SaveAsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.SaveAsButton.Name = "SaveAsButton";
			this.SaveAsButton.Size = new System.Drawing.Size(49, 22);
			this.SaveAsButton.Text = "Save as";
			this.SaveAsButton.Click += new System.EventHandler(this.SaveAsButton_Click);
			// 
			// saveButton
			// 
			this.saveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
			this.saveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(35, 22);
			this.saveButton.Text = "Save";
			this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
			// 
			// loadButton
			// 
			this.loadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.loadButton.Image = ((System.Drawing.Image)(resources.GetObject("loadButton.Image")));
			this.loadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.loadButton.Name = "loadButton";
			this.loadButton.Size = new System.Drawing.Size(37, 22);
			this.loadButton.Text = "Load";
			this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
			// 
			// toolStripItems
			// 
			this.toolStripItems.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cleanButton});
			this.toolStripItems.Location = new System.Drawing.Point(0, 25);
			this.toolStripItems.Name = "toolStripItems";
			this.toolStripItems.Size = new System.Drawing.Size(1330, 25);
			this.toolStripItems.TabIndex = 2;
			this.toolStripItems.Text = "toolStrip2";
			// 
			// cleanButton
			// 
			this.cleanButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.cleanButton.Image = ((System.Drawing.Image)(resources.GetObject("cleanButton.Image")));
			this.cleanButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.cleanButton.Name = "cleanButton";
			this.cleanButton.Size = new System.Drawing.Size(41, 22);
			this.cleanButton.Text = "Clean";
			this.cleanButton.Click += new System.EventHandler(this.cleanButton_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.propertyGridFurniture);
			this.panel1.Controls.Add(this.propertyGridCarpet);
			this.panel1.Controls.Add(this.applyPGButton);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(1066, 50);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(264, 578);
			this.panel1.TabIndex = 3;
			// 
			// propertyGridFurniture
			// 
			this.propertyGridFurniture.Dock = System.Windows.Forms.DockStyle.Top;
			this.propertyGridFurniture.Location = new System.Drawing.Point(0, 240);
			this.propertyGridFurniture.Name = "propertyGridFurniture";
			this.propertyGridFurniture.Size = new System.Drawing.Size(264, 240);
			this.propertyGridFurniture.TabIndex = 0;
			// 
			// propertyGridCarpet
			// 
			this.propertyGridCarpet.Dock = System.Windows.Forms.DockStyle.Top;
			this.propertyGridCarpet.Location = new System.Drawing.Point(0, 0);
			this.propertyGridCarpet.Name = "propertyGridCarpet";
			this.propertyGridCarpet.Size = new System.Drawing.Size(264, 240);
			this.propertyGridCarpet.TabIndex = 1;
			// 
			// applyPGButton
			// 
			this.applyPGButton.Location = new System.Drawing.Point(6, 541);
			this.applyPGButton.Name = "applyPGButton";
			this.applyPGButton.Size = new System.Drawing.Size(75, 23);
			this.applyPGButton.TabIndex = 2;
			this.applyPGButton.Text = "Apply";
			this.applyPGButton.UseVisualStyleBackColor = true;
			this.applyPGButton.Click += new System.EventHandler(this.applyPGButton_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1330, 628);
			this.Controls.Add(this.gridView);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.toolStripItems);
			this.Controls.Add(this.toolStripGeneral);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
			this.toolStripGeneral.ResumeLayout(false);
			this.toolStripGeneral.PerformLayout();
			this.toolStripItems.ResumeLayout(false);
			this.toolStripItems.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView gridView;
		private System.Windows.Forms.ToolStrip toolStripGeneral;
		private System.Windows.Forms.ToolStripButton newButton;
		private System.Windows.Forms.ToolStrip toolStripItems;
		private System.Windows.Forms.ToolStripButton cleanButton;
		private System.Windows.Forms.ToolStripButton SaveAsButton;
		private System.Windows.Forms.ToolStripButton saveButton;
		private System.Windows.Forms.ToolStripButton loadButton;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PropertyGrid propertyGridCarpet;
		private System.Windows.Forms.PropertyGrid propertyGridFurniture;
		private System.Windows.Forms.Button applyPGButton;

	}
}

