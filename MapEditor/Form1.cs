﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DigCraftFight;
using System.IO;
using System.Xml.Serialization;

namespace MapEditor
{
	public partial class Form1 : Form
	{

		private Dictionary<Item, Bitmap> items = new Dictionary<Item, Bitmap>();
		private DataTable table;


		public Form1()
		{
			InitializeComponent();

			Bitmap tilesetGround64 = (Bitmap) Image.FromFile(@"..\..\..\DigCraftFight\DigCraftFightContent\Tileset\TilesetGround64.png");

			foreach (ItemType itemType in Enum.GetValues(typeof(ItemType)))
			{
				if (itemType <= ItemType.DELIMITER_GROUND
				|| itemType >= ItemType.DELIMITER_BLOCK_END)
				{
					continue;
				}
				if (itemType == ItemType.GROUND_MONSTER_GENERATOR
					|| itemType == ItemType.BOSS_ZONE
					|| itemType == ItemType.WORKSTATION_ORE
					|| itemType == ItemType.ORE)
				{
					continue;
				}
				Item item = Item.New(itemType);
				TextureRect textureRect = item.GetTextureRect();
				if (textureRect != null && textureRect.spriteFile == TextureRect.SpriteFile.TilesetGround)
				{
					Bitmap img = Crop(tilesetGround64, new Rectangle(textureRect.Rectangle.X, textureRect.Rectangle.Y, textureRect.Rectangle.Width, textureRect.Rectangle.Height));
					this.items.Add(item, img);
				}
			}

			foreach (Item item in this.items.Keys)
			{
				ToolStripButton button = new ToolStripButton();
				button.Text = item.ItemType.ToString();
				button.Image = this.items[item];
				button.DisplayStyle = ToolStripItemDisplayStyle.Image;
				Item currentItem = item;///Create specific delegate
				button.Click += delegate { this.itemButton_Click(currentItem); };
				this.toolStripItems.Items.Add(button);
			}

			newMap();
		}

		

		/// <summary>
		/// http://stackoverflow.com/questions/734930/how-to-crop-an-image-using-c
		/// </summary>
		private static Bitmap Crop(Image source, Rectangle cropRect)
		{
			Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);
			using (Graphics g = Graphics.FromImage(target))
			{
				g.DrawImage(source, new Rectangle(0, 0, target.Width, target.Height),
								 cropRect,
								 GraphicsUnit.Pixel);
			}
			return target;
		}

		private void panel1_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.DrawImage(this.items.Values.First(), e.ClipRectangle);
		}

		private void newMap()
		{
			this.showMap(new ItemMap(32, 24));			
		}

		private void gridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
		{
			if (e.RowIndex == -1 || e.ColumnIndex == -1)
			{
				return;
			}
			e.Handled = true;
			Tuple<Item, Item> ground = new Tuple<Item, Item>(null, null);
			DataRow row = this.table.Rows[e.RowIndex];
			if (!row.IsNull(e.ColumnIndex))
			{
				ground = (Tuple<Item, Item>)row[e.ColumnIndex];
			}
			e.Graphics.FillRectangle(new SolidBrush(Color.White), e.CellBounds);
			if (ground.Item1 != null)
			{
				if (this.items.ContainsKey(ground.Item1))
				{
					e.Graphics.DrawImage(this.items[ground.Item1], e.CellBounds);
				}
				else
				{
					e.Graphics.DrawString("?", this.Font, new SolidBrush(Color.Black), e.CellBounds);
				}
			}
			if (ground.Item2 != null)
			{
				if (this.items.ContainsKey(ground.Item2))
				{
					e.Graphics.DrawImage(this.items[ground.Item2], e.CellBounds);
				}
				else
				{
					e.Graphics.DrawString("?", this.Font, new SolidBrush(Color.Black), e.CellBounds);
				}
			}

			if ((e.State & DataGridViewElementStates.Selected) != 0)
			{
				e.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Blue)),
					new Rectangle(e.CellBounds.X, e.CellBounds.Y, e.CellBounds.Width - 1, e.CellBounds.Height - 1));
			}


		}
		private static bool hasValue(object dataRowCellValue)
		{
			if (dataRowCellValue == DBNull.Value)
			{
				return false;
			}
			Tuple<Item, Item> ground = (Tuple<Item, Item>)dataRowCellValue;
			return ground.Item1 != null || ground.Item2 != null;
		}
		private ItemMap convertTableToItemMap(DataTable table)
		{
			int rowStart = 0;
			int rowEnd = this.table.Rows.Count - 1;
			int columnStart = 0;
			int columnEnd = this.table.Columns.Count - 1;
			for (; rowStart < table.Rows.Count; rowStart++)
			{
				DataRow row = this.table.Rows[rowStart];
				if (table.Columns.Cast<DataColumn>().Any(c => hasValue(row[c])))
				{
					break;
				}
			}
			for (; rowEnd >= 0; rowEnd--)
			{
				DataRow row = table.Rows[rowEnd];
				if (table.Columns.Cast<DataColumn>().Any(c => hasValue(row[c])))
				{
					break;
				}
			}
			for (; columnStart < this.table.Columns.Count; columnStart++)
			{
				DataColumn column = this.table.Columns[columnStart];
				if (table.Rows.Cast<DataRow>().Any(r => hasValue(r[column])))
				{
					break;
				}
			}
			for (; columnEnd >= 0; columnEnd--)
			{
				DataColumn column = table.Columns[columnEnd];
				if (table.Rows.Cast<DataRow>().Any(r => hasValue(r[column])))
				{
					break;
				}
			}
			ItemMap returned = new ItemMap(columnEnd + 1 - columnStart, rowEnd + 1 - rowStart);
			for (int j = 0; j < returned.Height; j++)
			{
				for (int i = 0; i < returned.Width; i++)
				{

					object cellValue = this.table.Rows[rowStart + j][columnStart + i];
					if (cellValue != DBNull.Value)					
					{
						Tuple<Item, Item> ground = (Tuple<Item, Item>)cellValue;
						returned.SetCarpet(ground.Item1, new IntVector2(i, j));
						returned.SetFurniture(ground.Item2, new IntVector2(i, j));						
					}
				}
			}
			return returned;
		}

		private void showMap(ItemMap itemMap)
		{

			this.table = new DataTable();
			Enumerable.Range(0, itemMap.Width).ToList().ForEach(i => table.Columns.Add("c" + i, typeof(Tuple<Item, Item>)));
			Enumerable.Range(0, itemMap.Height).ToList().ForEach(i => table.Rows.Add(table.NewRow()));
			for (int j = 0; j < itemMap.Height; j++)
			{
				for (int i = 0; i < itemMap.Width; i++)
				{
					this.table.Rows[j][i] = new Tuple<Item, Item>(
						itemMap.GetCarpet(new IntVector2(i, j)),
						itemMap.GetFurniture(new IntVector2(i, j)));		
				}
			}


			this.gridView.DataSource = this.table;
			foreach (DataGridViewColumn column in this.gridView.Columns)
			{
				column.Width = 24;
			}
		}

		private void newButton_Click(object sender, EventArgs e)
		{
			newMap();			
		}

		private void itemButton_Click(Item item)
		{
			foreach (DataGridViewCell cell in this.gridView.SelectedCells)
			{
				Tuple<Item, Item> ground = new Tuple<Item, Item>(null, null);
				DataRow row = this.table.Rows[cell.RowIndex];
				if (!row.IsNull(cell.ColumnIndex))
				{
					ground = (Tuple<Item, Item>)row[cell.ColumnIndex];
				}
				if (item.ItemType > ItemType.DELIMITER_GROUND && item.ItemType < ItemType.DELIMITER_GROUND_END)
				{
					row[cell.ColumnIndex] = new Tuple<Item, Item>(item, ground.Item2);
				}
				else
				{
					row[cell.ColumnIndex] = new Tuple<Item, Item>(ground.Item1, item);				
				}
			}
		}

		
		private void cleanButton_Click(object sender, EventArgs e)
		{
			foreach (DataGridViewCell cell in this.gridView.SelectedCells)
			{
				this.table.Rows[cell.RowIndex][cell.ColumnIndex] = new Tuple<Item, Item>(null, null);
			}
		}

		private string filename;
		private XmlSerializer serializer = new XmlSerializer(typeof(ComItemMap));
		private void save()
		{
			ItemMap itemMap = convertTableToItemMap(this.table);
			File.Delete(this.filename);
			using (var stream = File.OpenWrite(this.filename))
			{
				serializer.Serialize(stream, itemMap.toCom());
			}			
		}

		private void saveAs()
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = ".xml|*.xml";
			var r = saveFileDialog.ShowDialog();
			if (r != System.Windows.Forms.DialogResult.OK)
			{
				return;
			}
			this.filename = saveFileDialog.FileName;
			save();
		}

		
		private void SaveAsButton_Click(object sender, EventArgs e)
		{
			saveAs();
		}

		private void saveButton_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.filename))
			{
				saveAs();
			}
			else
			{
				save();
			}
		}

		private void loadButton_Click(object sender, EventArgs e)
		{
			OpenFileDialog fileDialog = new OpenFileDialog();
			fileDialog.Filter = ".xml|*.xml";
			var r = fileDialog.ShowDialog();
			if (r != System.Windows.Forms.DialogResult.OK)
			{
				return;
			}
			this.filename = fileDialog.FileName;
			ComItemMap comItemMap;
			using (var stream = File.OpenRead(this.filename))
			{
				comItemMap = (ComItemMap) serializer.Deserialize(stream);
			}
			ItemMap itemMap = new ItemMap(comItemMap);
			this.showMap(itemMap);
		}

		private void gridView_CurrentCellChanged(object sender, EventArgs e)
		{
			this.refreshPropertyGrid();
			DataGridViewCell cell = this.gridView.CurrentCell;
			if (cell != null)
			{
				this.Text = cell.ColumnIndex + " x " + cell.RowIndex;
			}
			else
			{
				this.Text = "Map editor";
			}
		}

		private void refreshPropertyGrid()
		{
			Item carpet = null;
			Item furniture = null;
			DataGridViewCell cell = this.gridView.CurrentCell;
			if (cell != null)
			{
				object cellValue = this.table.Rows[cell.RowIndex][cell.ColumnIndex];
				if (cellValue != DBNull.Value)
				{
					Tuple<Item, Item> tuple = (Tuple<Item, Item>)cellValue;
					carpet = tuple.Item1;
					furniture = tuple.Item2;
				}
			}
			this.propertyGridCarpet.SelectedObject = ItemPG.New(carpet);
			this.propertyGridFurniture.SelectedObject = ItemPG.New(furniture);
		}

		public class ItemPG
		{
			public ComItem ComItem;
			private string[] parameterList;

			public ItemType ItemType { get { return this.ComItem.ItemType; } set { this.ComItem.ItemType = value; } }
			public string[] Parameters { get { return this.parameterList; } set { this.parameterList = value; } }


			private ItemPG(ComItem comItem)
			{
				this.ComItem = comItem;
				if (this.ComItem.Parameters != null)
				{
					this.parameterList = this.ComItem.Parameters.Select(v => v.ToString()).ToArray();
				}
			}

			public static ItemPG New(Item item)
			{
				if (item == null)
				{
					return null;
				}
				return new ItemPG ( Item.ToComItem(item) );
			}

			public static Item ReadItem(object value)
			{
				if (value == null)
				{
					return null;
				}
				ItemPG itemPG = (ItemPG)value;			
				return Item.FromComItem(new ComItem{ItemType = itemPG.ItemType, Parameters = convert(itemPG.Parameters)});
			}
			private static object[] convert(string[] strList)
			{
				if (strList == null || strList.Length == 0)
				{
					return null;
				}
				object[] r = strList.Select(str => convert(str)).ToArray();				
				return r;
			}

			private static object convert(string str)
			{
				bool b;
				if (bool.TryParse(str, out b))
				{
					return b;					
				}
				int i;
				if (int.TryParse(str, out i))
				{
					return i;
				}
				return str;
			}
		}

		private void applyPGButton_Click(object sender, EventArgs e)
		{
			DataGridViewCell cell = this.gridView.CurrentCell;
			if (cell == null)
			{
				return;
			}
			this.table.Rows[cell.RowIndex][cell.ColumnIndex] = new Tuple<Item, Item>(
				ItemPG.ReadItem(this.propertyGridCarpet.SelectedObject),
				ItemPG.ReadItem(this.propertyGridFurniture.SelectedObject));
		}

	}

		
}
