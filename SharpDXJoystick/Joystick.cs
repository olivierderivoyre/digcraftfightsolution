﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.DirectInput;
using System.Diagnostics;

namespace SharpDXJoystick
{
    /// <summary>
    /// https://code.google.com/p/sharpdx/source/browse/Samples/DirectInput/JoystickApp/Program.cs?r=c2a5b8e19628866fc4e13df4e18aee701d2e48f1
    /// http://stackoverflow.com/questions/3929764/taking-input-from-a-joystick-with-c-sharp-net
    /// </summary>
    public class JoystickControler
    {
        DirectInput directInput;
        List<DeviceInstance> deviceInstances = new List<DeviceInstance>();
        List<Joystick> joysticks = new List<Joystick>();

        public JoystickControler()
        {
            directInput = new DirectInput();
            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                deviceInstances.Add(deviceInstance);
            // If Gamepad not found, look for a Joystick
            if (deviceInstances.Count == 0)
                foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                     deviceInstances.Add(deviceInstance);
            foreach (var deviceInstance in deviceInstances)
            {

                var joystick = new Joystick(directInput, deviceInstance.InstanceGuid);
                ///http://www.codeproject.com/Articles/151917/A-Simple-Snake-Game-Engerek                               
                foreach (DeviceObjectInstance doi in joystick.GetObjects(DeviceObjectTypeFlags.Axis))
                {
                    joystick.GetObjectPropertiesById(doi.ObjectId).Range = new InputRange(-5000, 5000);/// Up when state.Y less than -40, down when more than 40
                }                               
                joystick.Properties.BufferSize = 128;
                joystick.Properties.AxisMode = DeviceAxisMode.Absolute;
               // joystick.SetCooperativeLevel(parent, (CooperativeLevel.Nonexclusive | CooperativeLevel.Background));
        
               // Acquire the joystick
                joystick.Acquire();
                this.joysticks.Add(joystick);
            }
             
        }

        public class JoystickState
        {
            public bool Left;
            public bool Right;
            public bool Up;
            public bool Down;
			public bool[] Buttons = new bool[0];
            public bool IsButtonPressed(int buttonBumber)
            {
				int i = buttonBumber - 1;
                if (i >= 0 && i < this.Buttons.Length)
                {
                    return this.Buttons[i];
                }
                return false;
            }
        }

        public JoystickState GetState(int padIndex)
        {
            JoystickState r = new JoystickState();
            if (padIndex >= this.joysticks.Count)
                return r;
            var joystick = this.joysticks[padIndex];
            joystick.Poll();
            var state = joystick.GetCurrentState();
            
            r.Buttons = state.Buttons;
            if (state.X < -1000)
            {
                r.Left = true;
            }
            else if(state.X > 1000)
            {
                r.Right = true;
            }
            if (state.Y < -1000)
            {
                r.Up = true;
            }
            else if (state.Y > 1000)
            {
                r.Down = true;
            }
           return r;
        }


    }
}
