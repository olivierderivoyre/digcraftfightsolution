﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
	public class BossZone
	{
        public readonly TextureRect TextureRectBossWaiting = TextureRect.NewGround(1, 3);
        public readonly TextureRect TextureRectBossDefeated = TextureRect.NewGround(2, 3);
        public readonly TextureRect TextureRectBossAlive = TextureRect.NewGround(3, 3);
        
        public bool BossDefeated = false;
		public bool BossAlive = false;

		public Rectangle LockCells;
		private Mob mob;

   

		public BossZone(Rectangle LockCells)
		{
			this.LockCells = LockCells;
		}

		
		public object[] getComParameter()
		{
			return new object[] {this.LockCells.X, this.LockCells.Y, this.LockCells.Width, this.LockCells.Height };
		}

		public BossZone(object[] comParameter)
		{
			this.LockCells = new Rectangle((int)comParameter[0], (int)comParameter[1], (int)comParameter[2], (int)comParameter[3]);
		}

		public void Update(World world, LongVector2 position)
		{
			if (!this.BossDefeated && !this.BossAlive)
			{
				if (world.HasPlayerInside(position + new IntVector2(-2, -2) * Tools.CellSizeInCoord, 5 * Tools.CellSizeInCoord, 5 * Tools.CellSizeInCoord))
				{
					this.CreateBoss(world, position);
				}
			}
			else if (this.mob != null)
			{
				if (this.mob.IsDead)
				{
					if (this.BossAlive)
					{
						this.BossAlive = false;
						this.BossDefeated = true;
						Trace.WriteLine("The boss is dead");
						world.Ground.RefreshLockedBlock(position);
                        world.Boss = null;
					}
				}				
			}
		}

		public void CreateBoss(World world, LongVector2 position)
		{
			this.BossAlive = true;
			Trace.WriteLine("The boss is alive");
			this.mob = new Mob(world, position, MobType.Skeleton);
			world.Add(this.mob);
            world.Boss = mob;
		}
	}
}