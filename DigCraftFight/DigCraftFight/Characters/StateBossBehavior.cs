﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace DigCraftFight
{
    public class StateBossBehavior : MobBehavior
    {
        private enum State {Intro, Part1, Invoke1, Part2, Invoke2, Part3, RaisePower, SuperFight, Dying }

        private State CurrentState = State.Intro;
        private int StateTickStart = -1;

		private Weapon shoot1;
		private Weapon shoot2;
		private Weapon shoot3;
		private Weapon shoot4;
        private HashSet<Mob> generatedMobs = new HashSet<Mob>();

        public StateBossBehavior(Mob mob)
            : base(mob)
        {
			this.shoot1 = Weapon.CreateMobWeapon(ItemType.WEAPON_BULLET_RANDOM_DIRECTION, mob.DamagePerSecond, 41, 24);
			this.shoot2 = Weapon.CreateMobWeapon(ItemType.WEAPON_BULLET_RANDOM_DIRECTION, mob.DamagePerSecond, 30, 24);
			this.shoot3 = Weapon.CreateMobWeapon(ItemType.WEAPON_BULLET_RANDOM_DIRECTION, mob.DamagePerSecond, 20, 24);
			this.shoot4 = Weapon.CreateMobWeapon(ItemType.WEAPON_BULLET_RANDOM_DIRECTION, mob.DamagePerSecond, 20, 24);
        }

        private void switchState(State state)
        {
            this.CurrentState = state;
            this.StateTickStart = -1;
            this.mob.speechBallon = null;      
            Trace.WriteLine("Boss: " + state);
        }

        public override void Update(World world, Screen screen)
        {           
            this.generatedMobs.RemoveWhere(m => m.IsDead);
            if (this.CurrentState == State.Intro)
            {
                if (this.StateTickStart == -1)
                {
                    this.StateTickStart = world.Tick;
                    this.mob.Immortal = true;
					this.mob.speechBallon = new SpeechBalloon(world, this.mob.Position, Item.GetTextureRect(ItemType.EMOTICON_FLAIL));
                }
                if (world.Tick > this.StateTickStart + 60 * 7)
                {
                    this.switchState(State.Part1);
                    return;
                }
            }
            else if (this.CurrentState == State.Part1)
            {
                if (this.StateTickStart == -1)
                {
                    this.mob.Immortal = false;
                    this.StateTickStart = world.Tick;
                }
                if (this.mob.CharacterAttributes.LifeBasisPoint < 7500)
                {
                    this.switchState(State.Invoke1);
                    return;
                }
                updateDuringPart1(world);
            }
            else if (this.CurrentState == State.Invoke1)
            {
                if (this.StateTickStart == -1)
                {
                    this.mob.speechBallon = new SpeechBalloon(world, this.mob.Position, Item.GetTextureRect(ItemType.TOOL_PICK1));
                    this.mob.Immortal = true;
                    this.StateTickStart = world.Tick;
                }
                if (world.Tick > this.StateTickStart + 60 * 20)
                {
                    this.switchState(State.Part2);
                    return;
                }
                updateDuringInvoke1(world);
            }
            else if (this.CurrentState == State.Part2)
            {
                if (this.StateTickStart == -1)
                {
                    this.StateTickStart = world.Tick;
                    this.mob.Immortal = false;
                }
				if (this.mob.CharacterAttributes.LifeBasisPoint < 5000)
                {
                    this.switchState(State.Invoke2);
                    return;
                }
                updateDuringPart2(world);
            }
            else if (this.CurrentState == State.Invoke2)
            {
                if (this.StateTickStart == -1)
                {
                    this.mob.speechBallon = new SpeechBalloon(world, this.mob.Position, Item.GetTextureRect(ItemType.TOOL_PICK1));
                    this.mob.Immortal = true;
                    this.StateTickStart = world.Tick;
                }
                if (world.Tick > this.StateTickStart + 60 * 20)
                {
                    this.switchState(State.Part3);
                    return;
                }
                updateDuringInvoke2(world);
            }
            else if (this.CurrentState == State.Part3)
            {
                if (this.StateTickStart == -1)
                {
                    this.mob.Immortal = false;
                    this.StateTickStart = world.Tick;
                }
				if (this.mob.CharacterAttributes.LifeBasisPoint < 2500)
                {
                    this.switchState(State.RaisePower);
                    return;
                }
                this.updateDuringPart3(world);
            }
            else if (this.CurrentState == State.RaisePower)
            {
                if (this.StateTickStart == -1)
                {
                     this.StateTickStart = world.Tick;
                    this.mob.Immortal = true;
                    this.updateDuringRaisePower(world);
                }
                if (world.Tick > this.StateTickStart + 60 * 10)
                {
                    this.switchState(State.SuperFight);
                    return;
                }
            }
            else if (this.CurrentState == State.SuperFight)
            {
                if (this.StateTickStart == -1)
                {
                    this.StateTickStart = world.Tick;
                    this.mob.Immortal = false;
                }
				if (this.mob.CharacterAttributes.LifeBasisPoint <= 0)
                {
                    this.switchState(State.Dying);
                    return;
                }
                this.updateDuringSuperFight(world);
            }
            else if (this.CurrentState == State.Dying)
            {
                if (this.StateTickStart == -1)
                {
                    this.mob.Immortal = true;
                    this.StateTickStart = world.Tick;
                    foreach (Mob mob in this.generatedMobs)
                    {
						mob.CharacterAttributes.IsDead = true;
                        world.Remove(mob);
                    }
                    this.generatedMobs.Clear();
                    this.updateDuringDying(world);                   
                }

                if (world.Tick > this.StateTickStart + 60 * 5)
                {
					mob.CharacterAttributes.IsDead = true;
                    world.Remove(this.mob);
                    this.generateDrop(world);
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }





       
        private void updateDuringPart1(World world)
        {
			this.shoot1.TryActivate(world, this.mob, null);
        }
        private void updateDuringInvoke1(World world)
        {
            if (world.Tick < this.StateTickStart + 60 * 5)
            {
                if (world.Tick % 120 == 0)
                {
					Mob mob = new Mob(world, this.mob.Position, MobType.Bat);
                    generatedMobs.Add(mob);
                    world.Add(mob);
                }
            }
        }


        private void updateDuringPart2(World world)
        {
			this.shoot2.TryActivate(world, this.mob, null);
        }

        
        private void updateDuringInvoke2(World world)
        {
            if (world.Tick < this.StateTickStart + 60 * 8)
            {
                if ((world.Tick - this.StateTickStart) % 60 == 0)
                {
					Mob mob = new Mob(world, this.mob.Position, MobType.Bat);
                    generatedMobs.Add(mob);
                    world.Add(mob);
                }
            }
        }

        
        private void updateDuringPart3(World world)
        {
			this.shoot3.TryActivate(world, this.mob, null);
        }

        private void updateDuringRaisePower(World world)
        {
            this.mob.speechBallon = new SpeechBalloon(world, this.mob.Position, Item.GetTextureRect(ItemType.EMOTICON_SWORD));

        }
        private void updateDuringSuperFight(World world)
        {
			this.shoot4.TryActivate(world, this.mob, null);
            if (world.Tick % 120 == 0 && generatedMobs.Count < 4)
            {
				Mob mob = new Mob(world, this.mob.Position, MobType.Bat);
                generatedMobs.Add(mob);
                world.Add(mob);
            }
        }

        
        private void updateDuringDying(World world)
        {
            this.mob.speechBallon = new SpeechBalloon(world, this.mob.Position, Item.GetTextureRect(ItemType.BONE));
        }

		private void generateDrop(World world)
		{
			this.mob.addDrop(world, ItemType.BONE);
			this.mob.addDrop(world, ItemType.BONE);
		}

    }
}
