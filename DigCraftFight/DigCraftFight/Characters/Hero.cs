﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace DigCraftFight
{
	public class Hero
	{

		#region Hero names

		public static readonly string[] heroSprites = { "harajuku4", "moderngirl02", "tokinoiori03", "tokinoiori05", "modernguy02" };

		private static readonly string[] heroNames = {
			"Afton", "Carann", "Gilingon", "Belee",
			"Aharank", "Cham", "Maudiere", "Podan", 
			"Aigreth", "Chans", "Eothien", "Pola", 
			"Alanda", "Ciccionn", "Sitai", "Raglad", 
			"Alten", "Cirithe", "Hanton", "Ramalber", 
			"Ameryn", "Coiffin", "Horant", "Rathach", 
			"Amla", "Combe", "Hrant", "Recceleb", 
			"Anbor", "Cydenzel", "Ipes", "Rhodre", 
			"Arach", "Dalitte", "Katon", "Rohanne", 
			"Arba", "Damrysse", "Kellaine", "Romarog", 
			"Arbh", "Sone", "Lossenn", "Rose", 
			"Ardel", "Stres", "Maeluny", "Rost", 
			"Arvetuel", "Draut", "Manzion", "Seres", 
			"Asairgil", "Eanias", "Mazord", "Sigeruin", 
			"Banus", "Ecge", "Metorla", "Signy", 
			"Barith", "Edwyn", "Mevetuel", "Solva", 
			"Bocca", "Efleda", "Mildis", "Tapeleyn", 
			"Bohert", "Eliet", "Miline", "Tunbor", 
			"Bratha", "Emic", "Mithe", "Tunter", 
			"Brianu", "Esmus", "Mithi", "Tutius", 
			"Brodre", "Evric", "Morri", "Twyn", 
			"Brody", "Feiddine", "Mund", "Vequara", 
			"Bryd", "Fjoladye", "Neisil", "Viram", 
			"Bryn", "Gawa", "Ning", "Walon", 
			"Carana", "Gerry", "Ninn", "Yngyr"  
					};



		#endregion Hero names

		private static List<Hero> distinctHeroes = new List<Hero>();

		public static Hero GetHero(int index)
		{
			if (index < 0 || index > 5000)
			{
				throw new ArgumentException("index=" + index);
			}
			while(index >= distinctHeroes.Count)			
			{
				Hero.distinctHeroes.Add(new Hero(distinctHeroes.Count));
			}
			return distinctHeroes[index];
		}

        public static Hero CreateNewHero()
        {
            return GetHero(Hero.distinctHeroes.Count);
        }
		
		public static Hero Harajuku4 = Hero.GetHero(0);
		public static Hero Moderngirl02 = Hero.GetHero(1);
		public static Hero Tokinoiori03 = Hero.GetHero(2);
		public static Hero Tokinoiori05 = Hero.GetHero(3);
		
		

		public readonly int HeroId;
		private string spriteFileName;
		public readonly string DisplayName;
		private Player player;
		private Texture2D sprite;
		public readonly ItemSet GearSlots = new ItemSet(1, 3);
		public readonly ItemSet NpcWeaponSlot = new ItemSet(1, 1);
		public readonly ItemSet Bag = new ItemSet(14, 2);

		public Weapon Weapon;

		private Hero(int index)
		{
			this.HeroId = index;
			this.spriteFileName = Hero.heroSprites[index % Hero.heroSprites.Length];
			this.DisplayName = Hero.heroNames[index % Hero.heroNames.Length];
			this.Weapon = Weapon.NewNoobSword().Weapon;///Temp
		}

		public Texture2D GetSprite(Ressource ressource)
		{
			if (this.sprite == null)
			{
				this.sprite = ressource.heroTexureList[this.spriteFileName];
			}
			return this.sprite;
		}



		internal void init(Player player)
		{
			this.player = player;
		}
		public object[] getComParameter()
		{
			return new object[] { this.HeroId };
		}

		public static Hero FomComParameter(object[] comParameter)
		{
			if (comParameter == null)
			{
				throw new ArgumentException("Null parameter for Hero");
			}
			if (comParameter.Length == 0)
			{
				throw new ArgumentException("Empty parameter for Hero");
			}
			Hero hero = Hero.GetHero((int)comParameter[0]);
			return hero;
		}

		public override string ToString()
		{
			return this.DisplayName;
		}
	}
}
