﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	public class NpcTask
	{
		public enum TaskTypeEnum { Sleep, Work, Enjoying, Eat, RezFriend };
		public TaskTypeEnum TaskType;

		public Item TargetItem;
		public Character TargetCharacter;

		public LongVector2 TargetPosition;		
		public int DistanceInTick;
		public int JobDuration;
		public int SalaryInSleep;
		public int SalaryInWork;
		public int SalaryInJoy;
		public int SalaryInFood;
		
		public TextureRect SpeechIcon;

		

		/// <summary>
		/// For debug in VS.
		/// </summary>		
		public override string ToString()
		{
			if (this.TaskType == TaskTypeEnum.Work)
			{
				if (this.TargetItem.WorkStation != null)
				{
					return "Craft on " + this.TargetItem.ItemType;
				}
				else
				{
					return "Gather " + this.TargetItem.ItemType;
				}
			}
			return this.TaskType.ToString();
		}
	}
}
