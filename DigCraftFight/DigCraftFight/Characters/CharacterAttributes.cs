﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
	public class CharacterAttributes
	{
		public int LifeBasisPoint = 10000;
		public int LifeBasisPointDisplay = 10000;
		private double CurrentLife = 100;
		private double MaxLife = 100;
		private double RegenLifePerSec = 1;
		public int CurrentMana = 50;
		public int MaxMana = 100;
		public int RegenMana = 10;
		public int LastBeenHitTick = -100;
		public bool IsDead;
		public int HitInvulnerableDuration = 60;
		private int ResurectionCounter = 0;
		public bool IsResurecting = false;		

		public void Regen(World world)
		{
			if (world.Tick % 20 == 0)
			{
				if (world.Tick - this.LastBeenHitTick > 60 * 5)
				{
					this.CurrentLife = Math.Min(this.CurrentLife + this.MaxLife * 0.01 * RegenLifePerSec / 3, this.MaxLife);
				}
			}
			if (world.Tick % this.RegenMana == 0)
			{
				this.CurrentMana = Math.Min(this.CurrentMana + 1, this.MaxMana);
			}
			this.refreshPercents();
		}

		private void refreshPercents()
		{
			if (this.CurrentLife > this.MaxLife) 
			{
				this.CurrentLife = this.MaxLife;
			}
			this.LifeBasisPoint = Math.Max(0, (int) Math.Floor(this.CurrentLife / this.MaxLife * 10000));
			if (this.LifeBasisPoint < 5000)
			{
				this.LifeBasisPointDisplay = this.LifeBasisPoint / 2;
			}
			else
			{
				this.LifeBasisPointDisplay = Tools.InRange(2500 + (this.LifeBasisPoint - 5000) * 3 / 2, 0, 10000);
			}
			if (this.LifeBasisPoint <= 0)
			{
				this.IsDead = true;
				this.ResurectionCounter = 0;
			}
		}
		
		public void HitBy(Bullet bullet, World world)
		{
			if (world.Tick - this.LastBeenHitTick < this.HitInvulnerableDuration)
			{
				return;///TODO: improve to add hit power
			}
			this.LoseLife(bullet.attributes.Damage, world);		
		}
		
		private void LoseLife(double damage, World world)
		{
			
			this.LastBeenHitTick = world.Tick;
			this.CurrentLife = Math.Max(0, this.CurrentLife - damage);
			this.refreshPercents();
		}

		public void LoseLifePercentage(double damagePercent, World world)
		{
			this.LoseLife(damagePercent * this.MaxLife, world);		
		}

		public void RefreshFromStuff(ItemSet bag)
		{
			double maxLife = 100;
			foreach (IntVector2 slot in bag.GetAllSlots())
			{
				ItemStack stack = bag.GetBagItem(slot);
				if (stack != null && stack.Item != null && stack.Item.Gear != null)
				{
					maxLife += stack.Item.Gear.Health;
				}
			}
			this.MaxLife = maxLife;
			this.refreshPercents();
		}

		public void SetMobMaxLife(double maxLife)
		{
			this.MaxLife = maxLife;
			this.CurrentLife = maxLife;
			this.refreshPercents();
		}

		public void TryResurect(bool isResurecting)
		{
			this.IsResurecting = isResurecting;
			if (isResurecting)
			{
				this.ResurectionCounter++;
				if (this.ResurectionCounter > 60 * 5)
				{
					this.Resurect();
				}
			}
			else
			{
				this.ResurectionCounter = Math.Max(0, this.ResurectionCounter - 2);
			}
		}

		public void Resurect()
		{			
			this.ResurectionCounter = 0;
			this.CurrentLife = this.MaxLife / 3;
			this.IsDead = false;
			this.refreshPercents();
		}

		public Color GetLifeRedAlertIndicator(World world)
		{
			if (this.IsDead)
			{
				return Color.Red; ;
			}
			if (this.LifeBasisPoint < 1500)
			{
				if ((world.Tick / 10) % 2 == 0)
				{
					return Color.Red;
				}
				else
				{
					return Color.White;
				}
			}
			else if (this.LifeBasisPoint < 4000)
			{
				if ((world.Tick / 20) % 2 == 0)
				{
					return Color.Red;
				}
				else
				{
					return Color.White; ;
				}
			}
			return Color.White; ;
		}
	}
}
