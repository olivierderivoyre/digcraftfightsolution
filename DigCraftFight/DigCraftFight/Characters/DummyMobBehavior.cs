﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	/// <summary>
	/// Walk at random. Fire on player if he is near.
	/// </summary>
    public class DummyMobBehavior : MobBehavior
    {
        private IntVector2 currentDirection;
        private FixedTargetMoveAnimation quaterCellMoveAnimation;
		private readonly Weapon weapon;
		private bool isVagabond;
		private readonly LongVector2 home;
		private int MinWeaponRange;
		private int MaxWeaponRange;
		private int AggroRange;
		private int LostAggroRange;
		private Character targetPlayer;
		private LongVector2 inaccessiblePlayerCell;

		public DummyMobBehavior(Mob mob, Weapon weapon, bool isVagabond = false)
			: base(mob)
        {
			this.weapon = weapon;
			this.isVagabond = isVagabond;
			this.home = mob.Position;

			this.MaxWeaponRange = weapon.RangeInCoord;
			this.MinWeaponRange = this.MaxWeaponRange - 6 * Tools.QuaterCellSizeInCoord;
			this.AggroRange = 5 * Tools.CellSizeInCoord;
			this.LostAggroRange = this.AggroRange + 10 * Tools.CellSizeInCoord;
        }



		public override void Update(World world, Screen screen)
		{
			if (this.quaterCellMoveAnimation == null)
			{
				///Look for player
				if (this.targetPlayer == null)
				{
					 Character target = world.GetNearestPlayer(this.mob.Position, this.AggroRange);
					 if (target != null && Tools.CenterCharacterCell(target.Position) != this.inaccessiblePlayerCell)
					 {
						 this.targetPlayer = target;
					 }
				}
				else if (this.targetPlayer.IsDead)
				{
					this.targetPlayer = null;
				}
				if (this.targetPlayer != null)
				{
					int distance = IntVector2.Delta(this.targetPlayer.Position, this.mob.Position).GetDistance();
					if (distance > this.LostAggroRange)
					{
						this.targetPlayer = null;
					}
					else if (distance < this.MinWeaponRange)
					{
						if (this.currentDirection == IntVector2.Zero)
						{
							this.currentDirection = getRandomDirection();
						}
					}
					else
					{
						IntVector2 followPlayer = tryToGoToTargetPosition(world, this.targetPlayer.Position);
						if (followPlayer != IntVector2.Zero)
						{
							this.currentDirection = followPlayer;
						}
						else
						{
							if (Tools.RandomWin(8))
							{
								this.inaccessiblePlayerCell = Tools.CenterCharacterCell(this.targetPlayer.Position);
								this.targetPlayer = null;
							}
							this.currentDirection = getRandomDirection();
						}
					}
					if (distance < this.MaxWeaponRange + 2 * Tools.CellSizeInCoord)
					{
						this.weapon.TryActivate(world, this.mob, null);
					}
				}
				else if (this.isVagabond)
				{
					IntVector2 recenterScreenDirection = tryGoInside(world, screen.TopLeft, screen.WidthCoord, screen.HeightCoord);
					if (recenterScreenDirection != IntVector2.Zero)
					{
						this.currentDirection = recenterScreenDirection;
					}
				}
				else
				{
					IntVector2 recenterScreenDirection = tryGoInside(world, this.home - new IntVector2(6, 6) * Tools.CellSizeInCoord, 13 * Tools.CellSizeInCoord, 13 * Tools.CellSizeInCoord);
					if (recenterScreenDirection != IntVector2.Zero)
					{
						this.currentDirection = recenterScreenDirection;
					}
				}
				if (this.currentDirection == IntVector2.Zero)
				{
					this.currentDirection = getRandomDirection();
				}

				if (this.targetPlayer == null && Tools.Rand.Next(4) == 0)///Do some stupid move to do not stay blocked
				{
					this.currentDirection = getRandomDirection();
				}
				this.currentDirection = this.tryGo(world, this.currentDirection);
				if (this.currentDirection != IntVector2.Zero)
				{
					this.mob.LastMovedirection = this.currentDirection;
					this.quaterCellMoveAnimation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.mob.Position, this.currentDirection, 64);
				}
			}
			if (this.quaterCellMoveAnimation != null)
			{
				this.mob.Position = this.quaterCellMoveAnimation.GetUpdatedPosition();
				if (this.quaterCellMoveAnimation.IsFinish())
				{
					this.quaterCellMoveAnimation = null;
				}
			}
			
		}

		private IntVector2 tryGo(World world, IntVector2 direction)
		{
			return world.Ground.LimitCharacterMove(this.mob.Position, direction);
		}

		private IntVector2 getRandomDirection()
		{
			IntVector2 direction = new IntVector2(
				Tools.InRange(this.currentDirection.X + Tools.Rand.Next(-1, 2), -1, 1),
				Tools.InRange(this.currentDirection.Y + Tools.Rand.Next(-1, 2), -1, 1));

			if (direction.X != 0 && direction.Y != 0)
			{
				if (Tools.Rand.Next(2) == 0)
				{
					direction.X = 0;
				}
				else
				{
					direction.Y = 0;
				}
			}
			return direction;
		}
		
		private IntVector2 tryGoInside(World world, LongVector2 topLeft, int width, int height)
		{
			IntVector2 direction = IntVector2.Zero;
			if (this.mob.Position.X < topLeft.X + 2 * Tools.CellSizeInCoord)
			{
				direction = this.tryGo(world, new IntVector2(1, 0));
			}
			if (direction == IntVector2.Zero &&
				this.mob.Position.X > topLeft.X + width - 2 * Tools.CellSizeInCoord)
			{
				direction = this.tryGo(world, new IntVector2(-1, 0));
			}
			if (direction == IntVector2.Zero &&
				this.mob.Position.Y < topLeft.Y + 2 * Tools.CellSizeInCoord)
			{
				direction = this.tryGo(world, new IntVector2(0, 1));
			}
			if (direction == IntVector2.Zero &&
				this.mob.Position.Y > topLeft.Y + height - 2 * Tools.CellSizeInCoord)
			{
				direction = this.tryGo(world, new IntVector2(0, -1));
			}
			return direction;
		}
		private IntVector2 tryToGoToTargetPosition(World world, LongVector2 target)
		{
			IntVector2 delta = IntVector2.Delta(this.mob.Position, this.targetPlayer.Position);
						
			
			IntVector2 direction = this.tryGo(world, new IntVector2(Math.Sign(delta.X), Math.Sign(delta.Y)));
			if (direction != IntVector2.Zero)
			{
				return direction;
			}
			
			return IntVector2.Zero;
		}
    }
}
