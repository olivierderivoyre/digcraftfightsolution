﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace DigCraftFight
{
	public sealed class Player : Character
	{
		public InputController InputController;
		public Hero Hero;
		private int walkTickCount = 0;		
		private IntVector2 lastOrientation = IntVector2.Zero;
		private FixedTargetMoveAnimation quaterCellMoveAnimation;
		public readonly bool IsPlayer1;
		public readonly ItemSet ActionItems = new ItemSet(1, 4);
		public readonly ItemSet HeroAsBag = new ItemSet(1, 1);
		private Mob targetMob;
		public bool NeedToPressStartToPlay;
		public int NeedToPressStartToPlayTickDelay = 60;
		public bool IsActive { get { return !this.NeedToPressStartToPlay && !this.IsDead; } }


		public Player(InputController inputController, Hero hero, bool isPlayer1)
		{
			this.InputController = inputController;			
			this.Hero = hero;
			this.IsPlayer1 = isPlayer1;
			this.NeedToPressStartToPlay = !isPlayer1;
			this.HeroAsBag.SetEmptyCell(0, new Item(hero));
			this.Hero.init(this);
		}

		public static Rectangle GetSpriteSource(IntVector2 lastMovedirection, int walkTickCount)
		{
			int directionIndex = 0;
			if (lastMovedirection.X < 0)
			{
				directionIndex = 1;
			}
			else if (lastMovedirection.X > 0)
			{
				directionIndex = 2;
			}
			else if (lastMovedirection.Y < 0)
			{
				directionIndex = 3;
			}
			int walkTickIndex = 0;
			if (walkTickCount != 0)
			{
				walkTickIndex = (walkTickCount % 4);
			}
			return new Rectangle(walkTickIndex * 64, directionIndex * 64, 64, 64);
		}

		private IntVector2 getSpriteLookingDirection()
		{
			if (this.LastMovedirection.X != 0)
			{
				return new IntVector2(this.LastMovedirection.X, 0);
			}
			else
			{
				return new IntVector2(0, this.LastMovedirection.Y);
			}
		}

		public void Draw(World world, Screen g)
		{
			if (this.NeedToPressStartToPlay)
			{
				return;
			}
			Rectangle source = Player.GetSpriteSource(this.LastMovedirection, this.walkTickCount);
			DrawCharacter(world, g, this.Hero.GetSprite(g.Ressource), source, this.Position, this.CharacterAttributes);			
		}

		public static void DrawCharacter(World world, Screen g, Texture2D spriteTexture, Rectangle source, LongVector2 position, CharacterAttributes attr)
		{
			if (attr.IsDead)
			{
				Color color = Color.White;
				if (attr.IsResurecting)
				{
					if ((world.Tick / 8) % 2 == 0)
					{
						color = Color.Yellow;
					}
					else
					{
						color = Color.Green;
					}
				}
				g.Draw64(g.Ressource.TilesetGround, position, new Rectangle(256, 0, 64, 64), color);
				return;
			}
			else///Alive
			{
				bool hasJustBeenHit = world.Tick - attr.LastBeenHitTick < 5;
				Color color = attr.GetLifeRedAlertIndicator(world);
				if (hasJustBeenHit)
				{
					color = Color.Red;
				}
				g.Draw64(spriteTexture, position, source, color);
			}
		}


		public void Update(InputState inputState, InputState previousInputState, World world)
		{
			if (this.NeedToPressStartToPlay)
			{
				if (world.Tick < this.NeedToPressStartToPlayTickDelay)
				{
					return;///Press enter does not count
				}				
				if ( this.InputController.HasAnyActionButtonPressed(inputState))
				{
					if (!inputState.Keyboard.IsKeyDown(Keys.Enter))///I press enter to validate menus => I do not wants the 2nd player in this case.
					{
						this.NeedToPressStartToPlay = false;
						this.Position = this.IsPlayer1 ? world.player2.Position : world.player1.Position;
					}
				}
				return;
			}
			if (this.IsDead)
			{
				this.CharacterAttributes.TryResurect(world.HasAliveFriendlyUnitOver(this.Position));
				return;
			}

			if (this.quaterCellMoveAnimation == null)
			{
				IntVector2 direction = this.InputController.GetMoveDirection(inputState);
				if (direction == IntVector2.Zero)
				{
					this.walkTickCount = 0;
				}
				else
				{
					this.LastMovedirection = direction;
					this.lastOrientation = IntVector2.Zero;
					IntVector2 constrainedDirection = world.LimitPlayerMove(this.Position, direction);
					if (constrainedDirection == IntVector2.Zero)
					{
						this.walkTickCount = 0;
					}
					else
					{
						this.quaterCellMoveAnimation = FixedTargetMoveAnimation.CreateQuarterCellMove(this.Position, constrainedDirection, 24);
						this.walkTickCount++;
					}
				}
			}
			if (this.quaterCellMoveAnimation != null)
			{
				this.Position = this.quaterCellMoveAnimation.GetUpdatedPosition();
				if (this.quaterCellMoveAnimation.IsFinish())
				{
					this.quaterCellMoveAnimation = null;
				}
			}

			for (int i = this.InputController.ButtonPressedTaskList.Length - 1; i >= 0; i--)
			{
				if (this.InputController.ButtonPressedTaskList[i](inputState))
				{
					ItemStack itemStack = this.ActionItems.GetBagItem(i);
					if (itemStack == null || itemStack.Quantity <= 0)
					{
						continue;
					}
					this.activateItem(itemStack, inputState, world, !this.InputController.ButtonPressedTaskList[i](previousInputState));
					break;
				}
			}
			this.CharacterAttributes.Regen(world);

		}

		private void activateItem(ItemStack itemStack, InputState inputState, World world, bool buttonJustPressed)
		{
			Item item = itemStack.Item;
			if (item.ItemType == ItemType.HERO)
			{
				world.Add(new Npc(world, item.Hero, this.Position));
				itemStack.Quantity--;
				if (itemStack.Quantity <= 0)
				{
					this.ActionItems.Refresh();
				}
			}
			else if (item.Pick != null)
			{
				if (!item.Pick.Cooldown.TryStartCooldown(world.Tick))
				{
					return;
				}
				LongVector2 pickPosition = Tools.CenterCharacter(this.Position);
				Bullet.BulletAttributes bulletAttributes = new Bullet.BulletAttributes();
				bulletAttributes.Damage = 0;
				bulletAttributes.TargetType = TargetType.Furniture;
				bulletAttributes.Tool = item;
				bulletAttributes.TextureRect = item.GetTextureRect();

				Bullet bullet = new Bullet(bulletAttributes, new LinearMovingPoint(pickPosition, this.getSpriteLookingDirection() * Tools.PixelSizeInCoord, 1500, 5 * Tools.QuaterCellSizeInCoord));
				world.Add(bullet);
			}
			else if (item.ItemType == ItemType.TOOL_SPADE)
			{
				LongVector2 cell = Tools.CenterCharacterCell(this.Position);
				if (!world.Ground.IsLocked(cell))
				{
					Item carpet = world.Ground.GetCarpet(cell);
					if (carpet != null && carpet.ItemType != ItemType.GROUND_EARTH)
					{
						world.Ground.SetCarpet(Item.New(ItemType.GROUND_EARTH), cell);
						if (carpet.ItemType != ItemType.GROUND_GRASS && carpet.MonsterGenerator == null)
						{
							world.Bag.TryAdd(carpet);
						}
					}
				}
			}
			else if (item.Weapon != null)
			{
				if (buttonJustPressed || this.targetMob == null || this.targetMob.IsDead)
				{
					this.targetMob = this.getNearestMob(world);
				}
				item.Weapon.TryActivate(world, this, this.targetMob);
			}
			else// if (item.ItemId >= ItemType.DELIMITER_GROUND_BLOCKING && item.ItemId < ItemType.DELIMITER_TOOL)
			{
				bool success = world.TrySetItemOnGround(item, this.Position);
				if (success)
				{
					itemStack.Quantity--;
					if (itemStack.Quantity <= 0)
					{
						this.ActionItems.Refresh();
					}
				}
			}
		}




		private Mob getNearestMob(World world)
		{

			///First look behind the player in a 3x3 square.
			///If player look right, rectangle should be (0, -1, 3, 3)
			LongVector2 topLeftFrontSquare = this.Position + (new IntVector2(-1, -1) + this.LastMovedirection) * Tools.CellSizeInCoord;
			Mob mob = world.GetVisibleMobInside(topLeftFrontSquare, 3 * Tools.CellSizeInCoord);
			if (mob != null && !mob.Immortal)
			{
				return mob;
			}
			Mob mob2 = world.GetNearestMobInsideScreen(this.Position);
			if (mob2 != null && !mob2.Immortal)
			{
				return mob2;
			}
			return mob;
		}


		public void RefreshState()
		{
			this.ActionItems.Refresh();
			this.Hero = this.HeroAsBag.GetBagItem(0).Item.Hero;
			this.CharacterAttributes.RefreshFromStuff(this.Hero.GearSlots);
		}

		public override void HitBy(Bullet bullet, World world)
		{			
			this.CharacterAttributes.HitBy(bullet, world);
			if (this.CharacterAttributes.IsDead)
			{
				this.Position = Tools.CenterCharacterCell(this.Position);
				this.quaterCellMoveAnimation = null;
			}
		}

		public void Resurect(LongVector2 home)
		{
			this.quaterCellMoveAnimation = null;
			this.Position = home;
			if (this.IsDead)
			{
				this.CharacterAttributes.Resurect();
			}
		}

		public int GetBestWeaponLevel()
		{
			int best = -1;
			foreach (ItemStack stack in this.ActionItems.GetAllNonEmptyItemStacks())
			{
				if (stack.Item.Weapon != null)
				{
					best = Math.Max(best, stack.Item.Weapon.Level);
				}
			}
			return best;
		}

		public int GetBestGearLevel(int gearType)
		{
			int best = -1;
			foreach (ItemStack stack in this.Hero.GearSlots.GetAllNonEmptyItemStacks())
			{
				if (stack.Item.Gear != null && stack.Item.Gear.GearType == gearType)
				{
					best = Math.Max(best, stack.Item.Gear.Level);
				}
			}
			return best;
		}


		public override string ToString()
		{
			return this.IsPlayer1 ? "Player 1" : "Player 2";
		}

	}
}
