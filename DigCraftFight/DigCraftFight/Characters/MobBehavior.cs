﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public abstract class MobBehavior
    {

        protected Mob mob;

        protected MobBehavior(Mob mob)
        {
            this.mob = mob;
        }

        public abstract void Update(World world, Screen screen);
        
    }
}
