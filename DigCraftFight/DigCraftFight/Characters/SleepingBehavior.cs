﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class SleepingBehavior : MobBehavior
    {
        private int TickStart = Tools.Rand.Next(15000);
        private MobBehavior awakeBehavior;
        public SleepingBehavior(Mob mob, MobBehavior awakeBehavior)
            : base(mob)
        {
            this.awakeBehavior = awakeBehavior;
        }

        public override void Update(World world, Screen screen)
        {
            if ((TickStart + world.Tick) % 60 != 0)
            {
                return;
            }
            foreach (Player player in world.GetAlivePlayers())
            {
                if (world.HasClearShoot(this.mob.Position, player.Position, 5 * Tools.CellSizeInCoord))
                {
                    this.mob.targetCharacter = player;
                    this.mob.mobBehavior = this.awakeBehavior;
                    break;
                }
            }
           
        }

    }
}
