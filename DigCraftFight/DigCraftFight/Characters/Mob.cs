﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Threading.Tasks;

namespace DigCraftFight
{
	public sealed class Mob : Character
    {
		public readonly World world;
        public SpeechBalloon speechBallon;
		public double DamagePerSecond;
		public MobBehavior mobBehavior;        		
		private MobType mobType;
        public bool Immortal;
		public readonly TextureRect textureRect;
        public Player targetCharacter;
		public readonly bool IsElite;
		
        public Mob(World world, LongVector2 position, MobType mobType, bool isVagabond = false, bool isElite = false)
        {
			this.world = world;
            this.Position = position;
            this.mobType = mobType;
			this.IsElite = isElite;
			int level = world.Ground.GetGroundLevel(position);
			double maxLife = GameDifficulty.GetMobHealth(level);
			this.DamagePerSecond = GameDifficulty.GetMobDamage(level);
            if (this.mobType == MobType.Skeleton)
            {
				this.mobBehavior = new StateBossBehavior(this);
				maxLife = maxLife * 80;
            }
            else
            {
				if (this.IsElite)
				{
					maxLife = maxLife * 10;
				}
				Weapon weapon = Weapon.CreateMobWeapon(ItemType.WEAPON_CARDINAL_DIAGONAL_EXPLOSION, this.DamagePerSecond, 257, 8);
				this.mobBehavior = new DummyMobBehavior(this, weapon);              
            }
			this.CharacterAttributes.SetMobMaxLife(maxLife);
			this.CharacterAttributes.HitInvulnerableDuration = 0;
			int imageIndex = (int)this.mobType;
            this.textureRect = new TextureRect(TextureRect.SpriteFile.TilesetMob64, new Rectangle(imageIndex * 64, 0, 64, 64));
        }

        public void Draw(Screen screen)
        {			
			bool hasJustBeenHit = this.world.Tick - this.CharacterAttributes.LastBeenHitTick < 5;
			Color color = Color.White;
			if (hasJustBeenHit)
			{
				color = this.Immortal ? Color.Green : Color.Red;				
			}
			if (this.IsElite)
			{				
				screen.DrawScale(this.textureRect.GetTileset(screen.Ressource), 
					this.Position + new IntVector2(-32,-32) * Tools.PixelSizeInCoord, 128, this.textureRect.Rectangle, color);
			}
			else 
			{
				screen.Draw64(this.textureRect, this.Position, color);
			}
            if (this.speechBallon != null)
            {
                this.speechBallon.Draw(screen);
            }
        }

        public void Update(World world, Screen screen)
        {
            this.mobBehavior.Update(world, screen);                      
            if (this.speechBallon != null)
            {
                this.speechBallon.OriginPosition = this.Position;
            }
        }

		public override  void HitBy(Bullet bullet, World world)
        {
            if (this.Immortal)
            {
                return;
            }
            if (this.IsDead)
            {
                return;///Already dead
            }
			this.CharacterAttributes.HitBy(bullet, world);
            if (this.mobType != MobType.Skeleton)
            {
                if (this.IsDead)
                {
                    world.Remove(this);
                    this.generateDrop(world);
                }
            }
        }

        public void generateDrop(World world)
		{

            if (this.IsElite)
            {
                Hero hero = Hero.CreateNewHero();
                Npc npc = new Npc(world, hero, Tools.CenterCharacterCell(this.Position));
                world.Add(npc);
                return;
            }

			if (this.mobType == MobType.Rat)
			{
				if (Tools.RandomWin(2))
				{
					addDrop(world, ItemType.TEETH);
				}
			}
			if (this.mobType == MobType.Bat)
			{
				if (Tools.RandomWin(3))
				{
					addDrop(world, ItemType.GUANA);
				}
			}
			if (this.mobType == MobType.Skeleton)
			{
				addDrop(world, ItemType.BONE);

			}
		}

		public void addDrop(World world, ItemType itemType)
		{
			Drop.AddDropAround(world, Item.New(itemType), this.Position);			
		}		
    }
}
