﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections;
using System.Diagnostics;

namespace DigCraftFight
{
	public sealed class Npc : Character
    {
		private readonly World world;       
		public readonly Hero Hero;
		public LongVector2 Home;

		private int walkTickCount = 0;
		private SpeechBalloon speechBalloonDialog;
		private int speechBalloonDialogStopTick;

		private int sleepAmount;
        private int joyAmount;
		private int workAmount;
		private int foodReserve;
		
        private const int maxAmount = 60 * 60;
		private const int minSwitchAmount = 60 * 20;

		public Room Room;

		private readonly CoroutineCpu cpu = new CoroutineCpu();
		

        public Npc(World world, Hero hero, LongVector2 position)
        {
			this.world = world;
            this.Hero = hero;
            this.Position = position;
			this.Home = Tools.FloorToCell(position);

			this.initAmounts();
			this.foodReserve = maxAmount;///Do not start at startup
        }

		public void Draw(World world, Screen g)
        {
			Rectangle source = Player.GetSpriteSource(this.LastMovedirection, this.walkTickCount);
			Player.DrawCharacter(world, g, this.Hero.GetSprite(g.Ressource), source, this.Position, this.CharacterAttributes);			
			if (this.speechBalloonDialog != null)
			{
				if (this.world.Tick < this.speechBalloonDialogStopTick)
				{
					this.speechBalloonDialog.DrawOver(g, this.Position);
				}
			}
        }

        public void Update()
        {
			if (this.IsDead)
			{
				this.CharacterAttributes.TryResurect(world.HasAliveFriendlyUnitOver(this.Position));
				if (!this.IsDead)///Has been rez
				{
					this.initAmounts();
				}
				return;
			}
			///If NPC near a plyer, he says what's he is doing
			if (this.world.GetAlivePlayerTopLeftInside(this.Position - new IntVector2(1, 1) * Tools.CellSizeInCoord, Tools.CellSizeInCoord * 3, Tools.CellSizeInCoord * 3) != null)
			{
				this.speechBalloonDialogStopTick = world.Tick + 60 * 3;
			}
            this.sleepAmount = Tools.InRange(this.sleepAmount - 1, 0, maxAmount);
            this.workAmount = Tools.InRange(this.workAmount - 1, 0, maxAmount);
			this.joyAmount = Tools.InRange(this.joyAmount - 1, 0, maxAmount);
			this.foodReserve = Tools.InRange(this.foodReserve - 1, 0, maxAmount);

			if (this.cpu.IsIdle)
			{
				this.cpu.Go(this.co_main());
			}
			else
			{
				this.cpu.Update();
			}

			if (this.foodReserve > 0)
			{
				this.CharacterAttributes.Regen(this.world);
			}
			else
			{
				if (world.Tick % (60 * 5) == 0)
				{
					this.CharacterAttributes.LoseLifePercentage(0.05, this.world);
					if (this.CharacterAttributes.IsDead)
					{
						this.Position = Tools.CenterCharacterCell(this.Position);
					}
					else
					{
						this.Speech(TextureRect.NewItems32(9, 0));
					}
				}
			}
        }

		private void initAmounts()
		{
			this.sleepAmount = 60 * Tools.Rand.Next(20, 40);
			this.joyAmount = 60 * Tools.Rand.Next(20, 40);
			this.workAmount = 60 * Tools.Rand.Next(20, 40);
			this.foodReserve = 60 * Tools.Rand.Next(20,40); ;
		}

		#region Task

		private static bool addTask(List<NpcTask> availableTasks, NpcTask task, PathfindedMap map, LongVector2 taskPosition)
		{
			int distanceInTick = map.GetDistanceInTick(taskPosition);
			if (distanceInTick < 0)
			{
				return false;
			}
			task.TargetPosition = taskPosition;
			task.DistanceInTick = distanceInTick;///Npc.cellDuration == 90
			availableTasks.Add(task);
			return true;
		}		
		private List<NpcTask> SelectAllAvailableTasks(PathfindedMap map)
		{
			List<NpcTask> availableTasks = new List<NpcTask>();
			{
				var sleepHere = new NpcTask { TaskType = NpcTask.TaskTypeEnum.Sleep, SalaryInSleep = 1, JobDuration = 3 * 60 };
				addTask(availableTasks, sleepHere, map, this.Position);
			}
			
			{
				var sleepHome = new NpcTask
				{
					TaskType = NpcTask.TaskTypeEnum.Sleep,
					SalaryInSleep = 10,
					JobDuration = 3 * 60,
					SpeechIcon = Item.GetTextureRect(ItemType.BED)
				};
				addTask(availableTasks, sleepHome, map, this.Home);
				///Its good to go back home sometime
				if (IntVector2.Delta(this.Home, this.Position).GetDistance() > 8 * Tools.CellSizeInCoord)
				{
					sleepHome.SalaryInJoy = sleepHome.DistanceInTick;
				}
			}

			for (int i = 0; i < 10; i++)
			{
				var enjoyWalking = new NpcTask
				{
					TaskType = NpcTask.TaskTypeEnum.Enjoying,
					JobDuration =  1,
					SpeechIcon = TextureRect.NewItems32(6, 4)///Boots
				};
				LongVector2 p = map.GetARandomPosition();
				bool canGo = addTask(availableTasks, enjoyWalking, map, p);
				if (canGo)
				{
					enjoyWalking.SalaryInJoy = enjoyWalking.DistanceInTick;
				}
			}
			int foodItemInBag;
			List<Item> foodItems = this.Hero.Bag.GetQuantityOfFood(out foodItemInBag);
			foreach (Item food in foodItems)
			{
				var task = new NpcTask
				{
					TaskType = NpcTask.TaskTypeEnum.Eat,
					SalaryInFood = 50,
					JobDuration = 3 * 60,
					TargetItem = food,
					SpeechIcon = food.GetTextureRect()
				};
				addTask(availableTasks, task, map, this.Position);
			}
			foreach (LongVector2 targetPosition in map.GetAllAccessiblePositions())
			{
				Item furniture = world.Ground.GetFurniture(targetPosition);
				if (furniture != null)
				{
					if (furniture.WorkStation != null)
					{
						if (furniture.WorkStation.HasWork())
						{
							var task = new NpcTask
							{
								TaskType = NpcTask.TaskTypeEnum.Work,
								SalaryInWork = 3,
								JobDuration = 60 * 10,
								TargetItem = furniture,
								SpeechIcon = furniture.GetTextureRect()
							};
							addTask(availableTasks, task, map, targetPosition);
						}
					}
					else if (furniture.ItemType == ItemType.APPLE_TREE_WITH_FRUIT)
					{
						var task = new NpcTask
						{
							TaskType = NpcTask.TaskTypeEnum.Work,
							SalaryInWork = Math.Max(5 - foodItemInBag, 0),
							SalaryInFood = Math.Max(5 - foodItemInBag, 0),
							JobDuration = 1 * 60,
							TargetItem = furniture,
							SpeechIcon = furniture.GetTextureRect()
						};
						addTask(availableTasks, task, map, targetPosition);
					}
				}
			}
			foreach (Character character in world.GetDeadFriendlyUnits())
			{
				var task = new NpcTask
				{
					TaskType = NpcTask.TaskTypeEnum.RezFriend,
					TargetCharacter = character,
					SalaryInWork = 20,
					SalaryInJoy = 20,
					JobDuration = 5 * 60,					
					SpeechIcon = new TextureRect(character.GetHeroIfPossible())
				};
				addTask(availableTasks, task, map, character.Position);
			}


			return availableTasks;
		}
		private int getCappedSalary(int salary, int currentState, int lowLevelMultiplier, int onlyIfLowerThanPercent)
		{
			if (currentState * 100 > Npc.maxAmount * onlyIfLowerThanPercent)
			{
				return 0;
			}
			int multiplier = 1;
			if (currentState * 5 < Npc.maxAmount)///Lower than 20%, apply multiplier
			{
				multiplier = lowLevelMultiplier;
			}
			return Math.Min(Npc.maxAmount, currentState + salary) - currentState;
		}
		private int getTaskScore(NpcTask task)
		{
			int bonus =
				getCappedSalary(task.SalaryInFood * task.JobDuration, this.foodReserve, 10, 40) +
				getCappedSalary(task.SalaryInSleep * task.JobDuration, this.sleepAmount, 4, 40) +
				getCappedSalary(task.SalaryInWork * task.JobDuration, this.workAmount, 2, 90) +
				getCappedSalary(task.SalaryInJoy * task.JobDuration, this.joyAmount, 2, 95);		
		
			int foodBonus = Math.Min(Npc.maxAmount, this.foodReserve + task.SalaryInFood * task.JobDuration) - this.foodReserve;
			if(foodBonus > 0)
			{
				bonus += getCappedSalary(foodBonus, this.joyAmount, 2, 100);	///Eating is counted as a pleasure			
			}
			if (task.TaskType == NpcTask.TaskTypeEnum.Eat)
			{
				if (this.foodReserve * 2 > Npc.maxAmount)
				{
					return 0;///Avoid too eat while not hungry
				}
			}
			int duration = task.DistanceInTick + task.JobDuration;
			return bonus * 256 / duration;
		}
		private NpcTask findBestTask(List<NpcTask> alltasks)
		{
			if (this.foodReserve == 0)
			{
				NpcTask best = alltasks.Where(t => t.SalaryInFood > 0).OrderByDescending(t => getTaskScore(t)).FirstOrDefault();
				if (best != null)
				{
					return best;
				}
			}
			if (this.sleepAmount == 0)
			{
				NpcTask best = alltasks.Where(t => t.TaskType == NpcTask.TaskTypeEnum.Sleep).OrderByDescending(t => getTaskScore(t)).FirstOrDefault();
				if (best != null)
				{
					return best;
				}
			}
			if (this.workAmount == 0)
			{
				NpcTask best = alltasks.Where(t => t.SalaryInWork > 0).OrderByDescending(t => getTaskScore(t)).FirstOrDefault();
				if (best != null)
				{
					return best;
				}
			}
			return alltasks.OrderByDescending(t => getTaskScore(t)).FirstOrDefault();				
		}
		private NpcTask FindATask(out Queue<LongVector2> walkingPath)
		{			
			PathfindedMap map = new PathfindedMap(this.world, this.Home, this.Position);
			List<NpcTask> alltasks = this.SelectAllAvailableTasks(map);
			NpcTask task = this.findBestTask(alltasks);
			if (task == null)
			{
				throw new Exception("Null task");
			}
			walkingPath = map.GetPathTo(task.TargetPosition);
			return task;
		}

		private IEnumerable co_main()
		{

			Mob mob;
			while ((mob = getNearMob()) != null)
			{				
				if (this.Hero.Weapon != null)
				{
					this.Hero.Weapon.TryActivate(world, this, mob);
				}
				yield return null;
				if (Tools.RandomWin(100))
				{
					break;///Do not loop over wall
				}
			}
			if (this.Room == null)
			{
				yield return this.cpu.Go(this.co_refreshNpcRoom());
			}

			Queue<LongVector2> walkingPath;
			NpcTask task = this.FindATask(out walkingPath);

			yield return null;///Have thing enought this turn
			if (task.SpeechIcon != null)
			{
				this.Speech(task.SpeechIcon);
			}
			if (walkingPath != null && walkingPath.Count != 0)
			{								
				yield return this.cpu.Go(co_walk(walkingPath));
			}
			if (task.TaskType == NpcTask.TaskTypeEnum.Sleep)
			{
				this.speechBalloonDialog = new SpeechBalloon(this.world, this.Position, Item.GetTextureRect(ItemType.EMOTICON_SLEEPING));
				int duration = task.JobDuration;
				this.speechBalloonDialogStopTick = world.Tick + duration;
				for (int i = 0; i < duration; i++)
				{
					this.sleepAmount += task.SalaryInSleep;
					yield return null;
				}				
			}
			else if (task.TaskType == NpcTask.TaskTypeEnum.Work)
			{
				if (task.TargetItem != null)///TODO: check the item is always here
				{
					if (task.TargetItem.WorkStation != null)
					{
						for (int i = 0; i < task.JobDuration; i++)
						{
							WorkStation workStation = task.TargetItem.WorkStation;
							if (!workStation.HasWork())
							{
								yield break;
							}
							this.workAmount += task.SalaryInWork;
							bool justFinishedCraft = workStation.Work(world, task.TargetPosition);
							if (justFinishedCraft)
							{
								yield break;///??
							}
							yield return null;
						}
					}
					else
					{
						for (int i = 0; i < task.JobDuration; i++)
						{
							this.workAmount += task.SalaryInWork;
							yield return null;
						}
						Item drop = task.TargetItem.GatherByNpc(this.world, task.TargetPosition);
						if (drop != null)
						{
							if (this.Hero.Bag.TryAdd(drop))
							{
								this.world.Add(new SpeechBalloon(this.world, task.TargetPosition, drop.GetTextureRect()));
							}
						}
					}
				}
			}
			else if (task.TaskType == NpcTask.TaskTypeEnum.Enjoying)
			{
				for (int i = 0; i < task.JobDuration; i++)
				{
					this.joyAmount += task.SalaryInJoy;
					yield return null;
				}
			}
			else if (task.TaskType == NpcTask.TaskTypeEnum.Eat)
			{
				this.Hero.Bag.Remove(task.TargetItem);
				for (int i = 0; i < task.JobDuration; i++)
				{
					this.foodReserve += task.SalaryInFood;
					yield return null;
				}
			}
			else if (task.TaskType == NpcTask.TaskTypeEnum.RezFriend)
			{
				Character friendUnit = task.TargetCharacter;
				if (friendUnit.IsDead)
				{
					for (int i = 0; i < task.JobDuration; i++)
					{
						if (!friendUnit.IsDead)
						{
							///Save life for free
							break;
						}						
						yield return null;
					}
				}
			}
			else
			{
				throw new NotImplementedException();
			}

		}

		private IEnumerable co_walk(Queue<LongVector2> walkingPath)
		{
			this.walkTickCount = 0;
			foreach (LongVector2 nextCell in walkingPath)
			{
				if (this.Position == nextCell)
				{
					throw new Exception();
				}
				if (this.world.Ground.HasBlock(nextCell))///new block
				{
					this.walkTickCount = 0;
					this.cpu.Interrrupt();
					yield break;
				}
				FixedTargetMoveAnimation moveAnimation = FixedTargetMoveAnimation.CreateTargetMove(this.Position, nextCell, 90);
				this.LastMovedirection = IntVector2.Delta(this.Position, nextCell);
				while (!moveAnimation.IsFinish())
				{
					this.Position = moveAnimation.GetUpdatedPosition(ref this.walkTickCount);
					yield return null;
				}

			}
		}


		#endregion Task

		private void Speech(TextureRect textureRect)
		{
			this.speechBalloonDialogStopTick = this.world.Tick + 3 * 60;
			this.speechBalloonDialog = new SpeechBalloon(this.world, this.Position, textureRect);
		}


		#region Room

		private bool isFreeRoom(Room room)
		{
			foreach (Npc npc in this.world.GetAllNpcs())
			{
				if (npc == this)
				{
					continue;
				}
				if (room.IsSameRoomThan(npc.Room))
				{
					return false;
				}
			}
			return true;
		}


		private List<LongVector2> getDoorsAround(IntVector2 zone)
		{
			int zoneSizeCell = 16;
			int zoneSizeInCoord = (zoneSizeCell * Tools.CellSizeInCoord);
			LongVector2 topLeft = ((world.PlayerHomePosition / zoneSizeInCoord) + zone) * zoneSizeInCoord;
			List<LongVector2> returned = new List<LongVector2>();
			for (int y = 0; y < zoneSizeCell; y++)
			{
				for (int x = 0; x < zoneSizeCell; x++)
				{
					LongVector2 p = topLeft + new IntVector2(x, y) * Tools.CellSizeInCoord;
					if (!world.Ground.IsPlotOfLandLoaded(p))
					{
						continue;
					}
					if (Room.isDoor(world, p))
					{
						returned.Add(p);						
					}
				}
			}
			return returned;
		}

		private IEnumerable co_refreshNpcRoom()
		{
			Room initialRoom = this.Room;
			LongVector2 initialHome = this.Home;
			///Home?
			if (this.Room == null)
			{
                Room.RoomReturn roomReturn = new Room.RoomReturn();
				yield return this.cpu.Go(Room.TryGetRoomAt(world, this.Home, roomReturn));
                Room r = roomReturn.Room;
                if (r != null && this.isFreeRoom(r))
				{
					this.Room = r;
					Trace.WriteLine("NPC " + this.Hero.DisplayName + " link to his room");
				}
				yield return null;
			}
			///Position?
			if (this.Room == null)
			{
				Room.RoomReturn roomReturn = new Room.RoomReturn();
                yield return this.cpu.Go(Room.TryGetRoomAt(world, this.Position, roomReturn));
                Room newHome = roomReturn.Room;
				if (newHome != null && this.isFreeRoom(newHome))
				{
					this.Room = newHome;
					Trace.WriteLine("NPC " + this.Hero.DisplayName + " take the position room");
				}
				yield return null;
			}
			///Look for a Room
			if (this.Room == null)
			{				
				foreach (IntVector2 zone in Tools.GetSpiralInterator(5))
				{
					if (this.Room != null)
					{
						break;
					}
					foreach (LongVector2 doorPosition in getDoorsAround(zone))
					{
						if (this.Room != null)
						{
							break;
						}
						foreach (IntVector2 around in Tools.CardinalPoints)
						{
							if (this.Room != null)
							{
								break;
							}
							LongVector2 nearDoorPosition  = doorPosition + around * Tools.CellSizeInCoord;
							if (world.Ground.HasBlock(nearDoorPosition))
							{
								continue;
							}
							 Room.RoomReturn roomReturn = new Room.RoomReturn();
                            yield return this.cpu.Go(Room.TryGetRoomAt(world, nearDoorPosition, roomReturn));
                            Room newHome = roomReturn.Room;
							if (newHome != null && this.isFreeRoom(newHome))
							{
								this.Room = newHome;
								Trace.WriteLine("NPC " + this.Hero.DisplayName  + " have found a room");								
							}
							yield return null;
						}
						yield return null;
					}
					yield return null;
				}
			}

			///Ask to create a Room
			if (this.Room == null)
			{
				this.Speech(TextureRect.NewItems32(10, 0));
				yield return this.cpu.GoSleep(90);
			}
			if (this.Room != null)
			{
				if (this.Room != initialRoom)
				{
					this.Home = this.Room.GetNpcHome(world);
					yield return null;
					if (initialHome != this.Home)
					{
						///Go visit home
						yield return this.cpu.Go(co_walkToHome());
					}
				}
			}
		}

		private IEnumerable co_walkToHome()
		{
			this.Speech(TextureRect.NewItems32(11, 0));
			yield return null;
			Queue<LongVector2> path = null;
			if (PathfindedMap.IsInRange(this.Position, this.Home))
			{
				PathfindedMap map = new PathfindedMap(this.world, this.Home, this.Position);
				if (map.GetDistanceInTick(this.Home) >= 0)
				{
					path = map.GetPathTo(this.Home);
				}
			}
			if (path == null)
			{
				yield return this.cpu.GoSleep(60);
				///Go teleport
				this.Position = this.Home;
			}
			else
			{
				yield return this.cpu.Go(this.co_walk(path));
			}
		}

		#endregion Room


		private Mob getNearMob()
		{
			Mob mob = this.world.GetShootableMobAround(this.Position, 3 * Tools.CellSizeInCoord);
            if (mob != null)
            {                
				return mob;                
            }
            return null;
		}

		public override void HitBy(Bullet bullet, World world)
		{
			this.CharacterAttributes.HitBy(bullet, world);
			if (this.CharacterAttributes.IsDead)
			{
				this.Position = Tools.CenterCharacterCell(this.Position);							
			}
			else
			{
				///Stop walking or sleeping
				this.cpu.Interrrupt();
						
			}
		}

		public ComNpc ToCom(List<ComItemSet> allItemSet)
		{
			ComNpc com = new ComNpc();
			com.HeroId = this.Hero.HeroId;
			com.Home = Tools.FloorToCell(this.Home);
			com.BagId = this.Hero.Bag.ToCom(allItemSet, Guid.NewGuid());
			com.NpcWeaponSlotId = this.Hero.NpcWeaponSlot.ToCom(allItemSet, Guid.NewGuid());
			com.GearSlotsId = this.Hero.GearSlots.ToCom(allItemSet, Guid.NewGuid());
			return com;
		}

		public static Npc FromCom(World world, ComNpc com, ComItemSet[] allItemSet)
		{
			Npc npc = new Npc(world, Hero.GetHero(com.HeroId), com.Home);
			npc.Hero.Bag.FillFromCom(allItemSet, com.BagId);
			npc.Hero.NpcWeaponSlot.FillFromCom(allItemSet, com.NpcWeaponSlotId);
			npc.Hero.GearSlots.FillFromCom(allItemSet, com.GearSlotsId);
			return npc;
		}

		public override string ToString()
		{
			return this.Hero.DisplayName;
		}
    }
}
