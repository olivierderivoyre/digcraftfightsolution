﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
	public abstract class Character
	{
		public LongVector2 Position;
		public CharacterAttributes CharacterAttributes = new CharacterAttributes();
		public bool IsDead { get { return this.CharacterAttributes.IsDead; } }
		public IntVector2 LastMovedirection = new IntVector2(0, 1);


		public IntVector2 GetWeaponDirection(Character target)
		{
			if (target != null)
			{
				IntVector2 delta = IntVector2.Delta(this.Position, target.Position);
				int distance = delta.GetDistance();
				if (distance != 0)
				{
					return delta * Tools.PixelSizeInCoord / distance;
				}
			}
			if ((this is Mob) || this.LastMovedirection == IntVector2.Zero)///Mob does not have directin
			{
				return Tools.RandPickOne(Tools.NeighborgPoints) * Tools.PixelSizeInCoord;				
			}
			return this.LastMovedirection * Tools.PixelSizeInCoord;
		}

		public abstract void HitBy(Bullet bullet, World world);


		public Hero GetHeroIfPossible()
		{
			if (this is Player)
			{
				return ((Player)this).Hero;
			}
			if (this is Npc)
			{
				return ((Npc)this).Hero;
			}
			return null;
		}
	}
}
