﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	public class MonsterGenerator
	{
		private readonly MobType mobType;
		private readonly int numberOfMonster;
		private readonly bool hasEliteMonster;

		public MonsterGenerator(MobType mobType, int numberOfMonster, bool hasEliteMonster)
		{
			this.mobType = mobType;
			this.numberOfMonster = numberOfMonster;
			this.hasEliteMonster = hasEliteMonster;
		}

		
		public object[] getComParameter()
		{
			return new object[] { this.mobType, this.numberOfMonster, this.hasEliteMonster };
		}

		public MonsterGenerator(object[] comParameter)
		{
			this.mobType = (MobType)comParameter[0];
			this.numberOfMonster = (int)comParameter[1];
			if (comParameter.Length > 2)
			{
				this.hasEliteMonster = (bool)comParameter[2];
			}
		}
		
		public void GenerateMonster(World world, LongVector2 position)
		{
			for (int i = 0; i < this.numberOfMonster; i++)
			{
                LongVector2 mobPosition = position + new IntVector2(Tools.Rand.Next(-8, 12), Tools.Rand.Next(-8, 12)) * Tools.QuaterCellSizeInCoord;
                if (world.Ground.HasBlock(mobPosition))
                {
                    mobPosition = position;
                }
				world.Add(new Mob(world, mobPosition, this.mobType, isVagabond:false, isElite:false));
			}
			if (this.hasEliteMonster)
			{
				world.Add(new Mob(world, position, this.mobType, isVagabond: false, isElite: true));
			}
		}


		public override int GetHashCode()
		{
			return ("" + this.mobType + this.numberOfMonster + this.hasEliteMonster).GetHashCode();
		}
		public override bool Equals(object obj)
		{
			MonsterGenerator m = obj as MonsterGenerator;
			if (m != null)
			{
				return m.mobType == this.mobType && m.numberOfMonster == this.numberOfMonster && m.hasEliteMonster == this.hasEliteMonster;
			}
			return base.Equals(obj);
		}

	}
}
