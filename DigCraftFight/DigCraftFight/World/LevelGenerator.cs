﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace DigCraftFight
{
	public class LevelGenerator
	{

		private readonly Ground Ground;

		public LevelGenerator(Ground ground)
		{
			this.Ground = ground;
		}


        public const long TestLevels = 1000;

		public void GeneratePlotOfLand(long plotOfLandX, long plotOfLandY)
		{
			int levelAround = this.Ground.getMaxLevelAround(plotOfLandX, plotOfLandY);
			int level = levelAround == -1 ? 0 : levelAround == 0 ? 1 : Tools.Rand.Next(4) == 0 ? levelAround + 1 : levelAround;
			Trace.WriteLine("GeneratePlotOfLand(" + plotOfLandX + ", " + plotOfLandY + "), level=" + level);
			PlotOfLand newLand = this.Ground.newPlotOfLand(plotOfLandX, plotOfLandY, level);
			if (plotOfLandX == TestLevels && MapPattern.MapPatternByCode.ContainsKey("Test" + (plotOfLandY - TestLevels)))
            {
                Trace.WriteLine("Generate test level Test" + (plotOfLandY - TestLevels));
                generateTestLevel(newLand, MapPattern.MapPatternByCode["Test" + (plotOfLandY - TestLevels)]);
			}
            else if (level == 0)
			{
				generateTutorial(newLand);
			}
			else
			{
				if (level >= 2 && Tools.RandomWin(4))
				{
					generateADigLevel(newLand);
				}
				else if (level >= 2 && Tools.RandomWin(6))
				{
					generateAnOceanLevel(newLand);
				}
				else
				{
					generateAGrassAndTreeLevel(newLand);
				}
			}		
			newLand.GeneratorFinish();
		}
		
		public static IntVector2 TutorialPlayerLocalStartingPoint = new IntVector2(32 + 5, PlotOfLand.SizeInCell / 2 + 5);
		
		private void generateTutorial(PlotOfLand newLand)
		{
			///Land around			
            generateAGrassAndTreeLevel(newLand);

			IntVector2 topLeft = new IntVector2(30, PlotOfLand.SizeInCell / 2);
			MapPattern.MapPatternByCode["Tutorial"].CopyTo(newLand, topLeft);			
		}

		
        private void generateTestLevel(PlotOfLand newLand, MapPattern pattern)
        {
            ///Land around
            generateAGrassAndTreeLevel(newLand);


            IntVector2 topLeft = new IntVector2(32, PlotOfLand.SizeInCell / 2);

            pattern.CopyTo(newLand, topLeft);
        }

        private void generateAGrassAndTreeLevel(PlotOfLand newLand)
        {
            newLand.GeneratorFillZone(ItemType.GROUND_GRASS, null, new IntVector2(0, 0), PlotOfLand.SizeInCell, PlotOfLand.SizeInCell);
            for (int j = 0; j < PlotOfLand.SizeInCell; j++)
            {
                for (int i = 0; i < PlotOfLand.SizeInCell; i++)
                {
                    if (Tools.RandomWin(4))
                    {
                        newLand.GeneratorSetFurniture(Item.New(ItemType.TREE), new IntVector2(i, j));
                    }
                    else if (Tools.RandomWin(12))
                    {
                        newLand.GeneratorSetFurniture(Item.New(ItemType.BLOCK_STONE), new IntVector2(i, j));
					}
					else if (Tools.RandomWin(400))
					{
						newLand.GeneratorSetFurniture(Item.New(ItemType.ORE_SPOT), new IntVector2(i, j));
					}					
                }
            }
            List<IntVector2> waterCaveList = new List<IntVector2>();
            for (int i = Tools.Rand.Next(20); i >= 0; i--)
            {
                IntVector2 topLeft = new IntVector2(10 + Tools.Rand.Next(PlotOfLand.SizeInCell - 40), 10 + Tools.Rand.Next(PlotOfLand.SizeInCell - 40));
                int width = Tools.Rand.Next(3, 12);
                int height = Tools.Rand.Next(3, 12);
                newLand.GeneratorFillZone(ItemType.GROUND_WATER, null, topLeft, width, height);
                newLand.GeneratorFillZone(ItemType.GROUND_WATER, null, new IntVector2(topLeft.X + 1, topLeft.Y - 1), width - 2, height + 2);
                waterCaveList.Add(topLeft + new IntVector2(width/2, height/2));
            }
            for(int i = 0; i < waterCaveList.Count; i++)
            {
                generateRoad(newLand, ItemType.GROUND_WATER, waterCaveList[i], waterCaveList[Tools.Rand.Next(waterCaveList.Count)]);
            }
			addMobGenerators(newLand);
        }

		private void generateADigLevel(PlotOfLand newLand)
		{
			newLand.GeneratorFillZone(Item.New(ItemType.GROUND_GRASS), Item.New(ItemType.BLOCK_EARTH), new Rectangle(0, 0, PlotOfLand.SizeInCell, PlotOfLand.SizeInCell));
			newLand.GeneratorFillZone(Item.New(ItemType.GROUND_EARTH), Item.New(ItemType.BLOCK_EARTH), new Rectangle(1, 1, PlotOfLand.SizeInCell-2, PlotOfLand.SizeInCell-2));

			for (int j = 0; j < PlotOfLand.SizeInCell; j++)
			{
				for (int i = 0; i < PlotOfLand.SizeInCell; i++)
				{
					if (Tools.RandomWin(200))
					{
						newLand.GeneratorSetFurniture(Item.New(ItemType.ORE_SPOT), new IntVector2(i, j));
					}
				}
			}

			List<IntVector2> caveList = new List<IntVector2>();
			for (int i = Tools.Rand.Next(10, 20); i > 0; i--)
			{
				caveList.Add(new IntVector2(Tools.Rand.Next(10, PlotOfLand.SizeInCell - 10), Tools.Rand.Next(10, PlotOfLand.SizeInCell - 10)));
			}
			List<IntVector2> pathCenter = createPointsOnBorder().Concat(caveList).ToList();

			generateRoadsBetween(newLand, pathCenter, ItemType.GROUND_EARTH);
			foreach (IntVector2 cave in caveList)
			{
				generateCave(newLand, cave);
			}

			addMobGenerators(newLand);
		}

		private void generateAnOceanLevel(PlotOfLand newLand)
		{
			newLand.GeneratorFillZone(Item.New(ItemType.GROUND_WATER), null, new Rectangle(0, 0, PlotOfLand.SizeInCell, PlotOfLand.SizeInCell));

			List<IntVector2> caveList = new List<IntVector2>();
			for (int i = Tools.Rand.Next(10, 20); i > 0; i--)
			{
				caveList.Add(new IntVector2(Tools.Rand.Next(10, PlotOfLand.SizeInCell - 10), Tools.Rand.Next(10, PlotOfLand.SizeInCell - 10)));
			}
			List<IntVector2> pathCenter = createPointsOnBorder().Concat(caveList).ToList();
			generateRoadsBetween(newLand, pathCenter, ItemType.GROUND_GRASS);
			foreach (IntVector2 cave in caveList)
			{
				generateCave(newLand, cave);
			}

			addMobGenerators(newLand);

		}


		private static void addMobGenerators(PlotOfLand newLand)
		{
			for (int j = 0; j < PlotOfLand.SizeInCell; j++)
			{
				for (int i = 0; i < PlotOfLand.SizeInCell; i++)
				{
					if (newLand.GetFurniture(new IntVector2(i, j)) == null)
					{
						Item item = newLand.GetCarpet(new IntVector2(i, j));
						if (item != null && item.ItemType != ItemType.GROUND_WATER)
						{
							if (Tools.RandomWin(120))
							{
								newLand.GeneratorSetCapet(Item.CreateFrom(new MonsterGenerator(MobType.Bat, Tools.Rand.Next(1, 5),  Tools.RandomWin(10))), new IntVector2(i, j));
							}
						}
					}
				}
			}
		}

		private static void generateCave(PlotOfLand newLand, IntVector2 caveCenter)
		{

			int width = Tools.Rand.Next(4, 15);
			int height = Tools.Rand.Next(4, 15);
			IntVector2 topLeft = caveCenter + new IntVector2(-width / 2, -height / 2);

			int caveType = Tools.Rand.Next(8);///0 water, 1 stone, 2-3 tree, 4-5 sulfer, 6-7-bats			
			ItemType itemType;
			switch (caveType)
			{
				case 0: itemType = ItemType.GROUND_WATER; break;
				case 2:
				case 3: itemType = ItemType.GROUND_GRASS; break;
				case 6:
				case 7: itemType = ItemType.GROUND_BAT; break;
				default: itemType = ItemType.GROUND_EARTH; break;
			}
			Item fill = null;
			if (caveType == 1)
			{
				fill = Item.New(ItemType.BLOCK_STONE);
			}

			newLand.GeneratorFillZone(Item.New(itemType), fill, new Rectangle(topLeft.X, topLeft.Y, width, height));
			newLand.GeneratorFillZone(Item.New(itemType), fill, new Rectangle(topLeft.X + 1, topLeft.Y - 1, width - 2, height + 2));

			if (caveType == 2 || caveType == 3)
			{
				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						if (Tools.Rand.Next(4) == 0)
						{
							newLand.GeneratorSetFurniture(Item.New(ItemType.TREE), new IntVector2(topLeft.X + i, topLeft.Y + j));
						}
					}
				}
			}
			if (caveType == 4 || caveType == 5)
			{
				IntVector2 oreTopLeft = topLeft + new IntVector2(Tools.Rand.Next(width), Tools.Rand.Next(height));
				newLand.GeneratorFillZone(Item.New(ItemType.GROUND_EARTH), Item.New(ItemType.ORE_SPOT), new Rectangle(oreTopLeft.X, oreTopLeft.Y, 3, 3));
			}
		}



		private static List<IntVector2> createPointsOnBorder()
		{
			List<IntVector2> pathCenter = new List<IntVector2>();
			for (int i = Tools.Rand.Next(2, 5); i > 0; i--)
			{
				pathCenter.Add(new IntVector2(Tools.Rand.Next(1, PlotOfLand.SizeInCell - 1), 0));
			}
			for (int i = Tools.Rand.Next(2, 5); i > 0; i--)
			{
				pathCenter.Add(new IntVector2(Tools.Rand.Next(1, PlotOfLand.SizeInCell - 1), PlotOfLand.SizeInCell - 1));
			}
			for (int i = Tools.Rand.Next(2, 5); i > 0; i--)
			{
				pathCenter.Add(new IntVector2(0, Tools.Rand.Next(1, PlotOfLand.SizeInCell - 1)));
			}
			for (int i = Tools.Rand.Next(2, 5); i > 0; i--)
			{
				pathCenter.Add(new IntVector2(PlotOfLand.SizeInCell - 1, Tools.Rand.Next(1, PlotOfLand.SizeInCell - 1)));
			}
			return pathCenter;
		}


		
		private static void generateRoadsBetween(PlotOfLand newLand, List<IntVector2> caveList, ItemType ground)
		{
			for (int i = 0; i < caveList.Count; i++)
			{
				generateRoad(newLand, ground, caveList[i], caveList[Tools.Rand.Next(caveList.Count)]);
			}
		}

		private static void generateRoad(PlotOfLand newLand, ItemType ground, IntVector2 from, IntVector2 to)
		{			
			foreach(IntVector2 current in GetPath(newLand, from, to))
			{				
                newLand.GeneratorSetCapet(Item.New(ground), current);
                newLand.GeneratorSetFurniture(null, current);///Remove Tree if any
			}
		}

		private static IEnumerable<IntVector2> GetPath(PlotOfLand newLand, IntVector2 from, IntVector2 to)
		{
			IntVector2 current = from;
			while (current != to)
			{
				if (Tools.RandomWin(2))
				{
					if (current.X < to.X || current.X == 0)
					{
						current.X++;
					}
					else
					{
						current.X--;
					}
				}
				else
				{
					if (current.Y < to.Y || current.Y == 0)
					{
						current.Y++;
					}
					else
					{
						current.Y--;
					}
				}
				yield return current;				
			}
		}
	}
}
