﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class Pick
    {
        public readonly Cooldown Cooldown;
        public readonly int level;

        private Pick(int delay, int level)
        {
            this.Cooldown = new Cooldown(1, delay);
            this.level = level;
        }

        public static Pick TryGetPick(ItemType itemType)
        {
            if (itemType == ItemType.TOOL_PICK1)
            {
                return new Pick(50, 1);
            }
			if (itemType == ItemType.TOOL_PICK2)
			{
				return new Pick(45, 2);
			}
            return null;
        }
    }
}
