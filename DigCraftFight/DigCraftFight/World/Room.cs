﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace DigCraftFight
{
	/// <summary>
	/// Somewhere NPC can live
	/// </summary>
	public class Room
	{
		private readonly List<LongVector2> Doors = new List<LongVector2>();
		private readonly List<LongVector2> Cells = new List<LongVector2>();
		private readonly HashSet<LongVector2> WalkCells = new HashSet<LongVector2>();
		private readonly Dictionary<ItemType, LongVector2> Furnitures = new Dictionary<ItemType, LongVector2>();

		private Room() 
		{
		}



		private static bool isFurnitureWall(World world, LongVector2 p)
		{
			Item furniture = world.Ground.GetFurniture(p);
			if (furniture != null)
			{
				if (furniture.ItemType > ItemType.DELIMITER_HOME_WALL && furniture.ItemType < ItemType.DELIMITER_HOME_WALL_END)
				{
					return true;
				}
			}
			return false;
		}

		public static bool isDoor(World world, LongVector2 p)
		{
			Item carpet = world.Ground.GetCarpet(p);
			if (carpet != null)
			{
				if (carpet.ItemType == ItemType.DOOR)
				{
					return true;
				}
			}
			return false;
		}

		private static bool hasWall(World world, LongVector2 p)
		{
			if (isFurnitureWall(world, p))
			{
				return true;
			}
			if (isDoor(world, p))
			{
				return true;
			}			
			return false;
		}

        public class RoomReturn
        {
            public Room Room;
        }
        public static IEnumerable TryGetRoomAt(World world, LongVector2 targetPosition, RoomReturn roomReturn)
		{
            yield return null;
            roomReturn.Room = null;
			MapReferencial mapRef = new MapReferencial(targetPosition);
			bool[] blockedCells = mapRef.NewBool2DArray();
			foreach (IntVector2 localCoord in mapRef.GetAllMapCoord())
			{
				blockedCells[mapRef.GetIndex(localCoord)] = hasWall(world, mapRef.ToWorldPosition(localCoord));
			}
            yield return null;
            AlgoRoomFinder.RoomFinderReturn roomFinderReturn = new AlgoRoomFinder.RoomFinderReturn();

            foreach (object obj in AlgoRoomFinder.GetRoomFromAnyCell(blockedCells, mapRef.GetLocalMapSize(), mapRef.GetLocalCoord(targetPosition), roomFinderReturn))
            {
                yield return null;
            }
            yield return null;
            HashSet<IntVector2> roomCells = roomFinderReturn.RoomCells;
            if (roomCells == null)
			{
				yield break;
			}
			Room room = new Room();
			foreach (IntVector2 cell in roomCells)
			{
				LongVector2 position = mapRef.ToWorldPosition(cell);
				room.Cells.Add(position);
				if (isDoor(world, position))
				{
					room.Doors.Add(position);
				}
				else if (!world.Ground.HasBlock(position))
				{
					room.WalkCells.Add(position);
				}
				Item furniture = world.Ground.GetFurniture(position);
				if (furniture != null)
				{
					room.Furnitures[furniture.ItemType] = position;
				}
			}
            yield return null;
			if (room.Doors.Count == 0)
			{
                yield break; ;///Need one door
			}
			if (room.WalkCells.Count == 0)
			{
                yield break;///Do no fill with furniture
			}
            roomReturn.Room = room;
		}


		

		public bool IsSameRoomThan(Room otherRoom)
		{
			if (otherRoom == null)
			{
				return false;
			}
			return this.WalkCells.Overlaps(otherRoom.WalkCells);
		}


		public LongVector2 GetNpcHome(World world)
		{
			if (this.Furnitures.ContainsKey(ItemType.BED))
			{
				LongVector2 bedPosition = this.Furnitures[ItemType.BED];
				return bedPosition;///NPC never fo to their goal, they stop one cell before.
			}
			return this.WalkCells.First();
		}
	}

}
