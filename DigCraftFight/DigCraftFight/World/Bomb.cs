﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Microsoft.Xna.Framework;

//namespace DigCraftFight
//{	
//    public class Bomb : IAlive
//    {
//        private World world;
//        private Item item;
//        private LongVector2 position;
//        private int lastTick;

//        public Bomb(World world, LongVector2 position, Item item)
//        {
//            this.world = world;
//            this.item = item;
//            this.position = position;
//            this.lastTick = this.world.Tick + 180;
//        }

//        public bool Update()
//        {
//            if (this.world.Tick > this.lastTick)
//            {				
//                Action.StartAction(ActionType.CardinalExplosion, position + new IntVector2(1, 1) * Tools.CellSizeInCoord / 2, world,
//                        TargetType.Furniture | TargetType.Player | TargetType.Monster, this.item);
//                return false;
				
//            }
//            return true;
//        }

//        public void Draw(Screen screen)
//        {			
//            screen.Draw64(this.item.GetTileset(screen.Ressource), this.position, this.item.GetIconRectangleInTileset(), Color.White);			
//        }
//    }
//}
