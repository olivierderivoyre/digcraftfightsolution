﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using System.IO;
using System.Xml.Serialization;

namespace DigCraftFight
{
	public class MapPattern
	{

		private static XmlSerializer serializer = new XmlSerializer(typeof(ComItemMap));
		public static Dictionary<string, MapPattern> MapPatternByCode;
		
		public static void Init(string directory)
		{
			Dictionary<string, MapPattern> mapPatternList = new Dictionary<string, MapPattern>();
			foreach (string file in Directory.GetFiles(directory, "*.xml"))
			{
				ComItemMap comItemMap;
				using (var stream = File.OpenRead(file))
				{
					comItemMap = (ComItemMap)serializer.Deserialize(stream);
				}
				string code = Path.GetFileNameWithoutExtension(file);
				ItemMap itemMap = new ItemMap(comItemMap);
				mapPatternList.Add(code, new MapPattern(code, itemMap));
			}
			MapPattern.MapPatternByCode = mapPatternList;
		}


		private string code;
		private ItemMap itemMap;
		
		public MapPattern(string code, ItemMap itemMap)
		{
			this.code = code;
			this.itemMap = itemMap; 
		}


		private Item convertItem(PlotOfLand plotOfLand, Item patternItem, IntVector2 cell, IntVector2 topLeft)
		{
			if (patternItem == null)
			{
				return null;
			}
			if (patternItem.BossZone != null)
			{
				var r = patternItem.BossZone.LockCells;
				return new Item(new BossZone(new Microsoft.Xna.Framework.Rectangle(r.X + topLeft.X, r.Y + topLeft.Y, r.Width, r.Height)));				
			}
			return patternItem;
		}

		public void CopyTo(PlotOfLand plotOfLand, IntVector2 topLeft)
		{
			foreach(IntVector2 cell in this.itemMap.GetAllCells())
			{
				plotOfLand.GeneratorSet(
					convertItem(plotOfLand, this.itemMap.GetCarpet(cell), cell, topLeft),
					convertItem(plotOfLand, this.itemMap.GetFurniture(cell), cell, topLeft),
					topLeft + cell);
			}
		}


	}
}
