﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
	public class SpeechBalloon : IAlive 
	{
		private World world;
		private LongVector2 ballonPosition;
		public LongVector2 Position { get { return this.ballonPosition; } }
		private int lastTick;
        private TextureRect textureRect;
		public LongVector2 OriginPosition { set { this.ballonPosition = characterToBallonPosition(value); } }

		public SpeechBalloon(World world, LongVector2 originPosition, TextureRect textureRect, string message = "")
		{
			this.world = world;
			this.OriginPosition = originPosition;
			this.lastTick = this.world.Tick + 90;
			this.textureRect = textureRect;
		}

		public bool Update()
		{
			if (this.world.Tick > this.lastTick)
			{
				return false;
			}
			return true;
		}

		private static LongVector2 characterToBallonPosition(LongVector2 characterPosition)
		{
			return characterPosition + new IntVector2(-5, -60) * Tools.PixelSizeInCoord; 
		}

		public void Draw(Screen screen)
		{
			this.drawAtPosition(screen, this.ballonPosition);
		}
		public void DrawOver(Screen screen, LongVector2 characterPosition)
		{
			this.drawAtPosition(screen, characterToBallonPosition(characterPosition));
		}

		private void drawAtPosition(Screen screen, LongVector2 ballonPosition)
		{
			screen.Draw(screen.Ressource.TilesetInterface40, ballonPosition, new Rectangle(205, 0, 40, 80), Color.White);
			screen.Draw32(this.textureRect, ballonPosition + new IntVector2(4, 4) * Tools.PixelSizeInCoord, Color.White);			
		}
	}
}
