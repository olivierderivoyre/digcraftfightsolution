﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
     public enum TargetType{Monster=1, Player=2, Furniture=4, Carpet=8}

    public class Bullet
    {
        private LinearMovingPoint trajectory;
        private bool isFinish = false;
		public readonly BulletAttributes attributes;
		private int explosionTick = -1;
		
		public class BulletAttributes
		{
			public TargetType TargetType;
			public Item Tool;
			public TextureRect TextureRect;
			public double Damage;
			public int Range;
			public int SpeedCoord;
			public int ExplosionRange;
		}

		public Bullet(BulletAttributes bulletAttributes, LinearMovingPoint trajectory)
        {
			this.attributes = bulletAttributes;
			this.trajectory = trajectory;				            
        }

        public bool IsFisnish()
        {
            return this.isFinish;
        }

        public void Update(World world)
        {
			if (explosionTick >= 0)
			{
				explosionTick++;
				if (explosionTick == nbTickPerExplotionFrame * 2)
				{
					this.doDamage(world);
				}
				if (explosionTick >= nbTickPerExplotionFrame * 7)
				{
					this.isFinish = true;
				}
				return;
			}

            this.trajectory.UpdateInitIterate();
			bool trajectoryContinueIterate = true;

			bool bulletStopped = false;
			while (!bulletStopped && trajectoryContinueIterate)
            {
                trajectoryContinueIterate = this.trajectory.UpdateIterate();
				if (this.trajectory.IsFisnish())
				{
					bulletStopped = true;
					break;
				}
                Item furniture = world.Ground.GetFurniture(this.trajectory.Position);
                if (furniture != null)
                {
					if ((this.attributes.TargetType & TargetType.Furniture) != 0)
					{
						world.PickHitFurniture(furniture, this.trajectory.Position, this.attributes.Tool);
						this.isFinish = true;
						return;
					}
					bulletStopped = true;					                                       
                }
				if ((this.attributes.TargetType & TargetType.Monster) != 0)
                {
					if(world.GetVisibleMobsHitBy(this.trajectory.Position, 0).Any())					
                    {
						bulletStopped = true;
						break;						
                    }
                }
				if ((this.attributes.TargetType & TargetType.Player) != 0)
                {
					if (world.GetFriendlyUnitsHitBy(this.trajectory.Position, 0).Any())
					{
						bulletStopped = true;
						break;
					}
                }
            }
			if (bulletStopped)
			{
				explose(world);
			}
        }


		private void explose(World world)
		{
			if (this.attributes.ExplosionRange <= 0)
			{
				this.doDamage(world);
				this.isFinish = true;
				return;
			}
			else
			{
				explosionTick = 0;
			}
		}

		private void doDamage(World world)
		{
			if ((this.attributes.TargetType & TargetType.Monster) != 0)
			{
				foreach(Mob mob in world.GetVisibleMobsHitBy(this.trajectory.Position, this.attributes.ExplosionRange))
				{
					mob.HitBy(this, world);
				}
			}
			if ((this.attributes.TargetType & TargetType.Player) != 0)
			{
				foreach (Character character in world.GetFriendlyUnitsHitBy(this.trajectory.Position,  this.attributes.ExplosionRange))
				{
					character.HitBy(this, world);
				}
			}
		}

		private const int nbTickPerExplotionFrame = 3;
        public void Draw(Screen screen)
        {
			if (explosionTick >= 0)
			{
				int range = this.attributes.ExplosionRange;
				LongVector2 topLeft = this.trajectory.Position - new IntVector2(range, range);
				int spriteImg = this.explosionTick / nbTickPerExplotionFrame;
				Rectangle source = new Rectangle(82 + spriteImg * 41, 124, 40, 40);
				screen.DrawScale(screen.Ressource.TilesetInterface40, topLeft, range * 2 / Tools.PixelSizeInCoord, source, Color.White);

			}
			else
			{
				LongVector2 topLeft = this.trajectory.Position - new IntVector2(16, 16) * Tools.PixelSizeInCoord;
				screen.Draw32(this.attributes.TextureRect, topLeft, Color.White);
			}
        }
      
    }
}
