﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	[Serializable]
	public class ComItem
	{
		public ItemType ItemType;
		public Object[] Parameters = null;

	}

	[Serializable]
	public class ComPlotOfLand
	{
		public int Level;
		public ComItemMap ItemMap;

	}

	[Serializable]
	public class ComItemMap
	{
		public int Width;
		public int Height;
		public ComItem[] Items;
		public ComItemSet[] ItemSetList;

		public string CarpetCsv;
		public string FurnitureCsv;

		public static string ConvertToCsv(ushort[] array, int width)
		{
			StringBuilder r = new StringBuilder();
			for (int i = 0; i < array.Length; i++)
			{
				if (i % width == 0)
				{
					r.AppendLine();
				}
				else 
				{
					r.Append(",");
				}
				r.Append(array[i].ToString().PadLeft(2));				
			}
			r.AppendLine();
			return r.ToString();
		}
		public static ushort[] ConvertToArray(string csv, int width, int height)
		{
			ushort[] r = new ushort[width * height];
			string[] lines = csv.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
			for (int j = 0; j < lines.Length; j++)
			{
				string[] values = lines[j].Split(',');
				for (int i = 0; i < values.Length; i++)
				{
					ushort v = ushort.Parse(values[i]);
					r[j * width + i] = v;
				}
			}
			return r;
		}
		
	}

	[Serializable]
	public class ComItemSet
	{
		public Guid Id;
		public ComItemStack[] ItemContainers;

	}

	[Serializable]
	public class ComItemStack
	{
		public ComItem Item;
		public int Quantity;
	}
	[Serializable]
	public class ComNpc
	{
		public int HeroId;
		public LongVector2 Home;
		public Guid BagId;
		public Guid GearSlotsId;
		public Guid NpcWeaponSlotId;
	}

	[Serializable]
	public class ComWorld
	{
		public LongVector2 PlayerHomePosition;
		public LongVector2 PlayerResurectPosition;
		public ComNpc[] NpcList;
		public ComItemSet[] ItemSetList;
		public Guid PlayerBagId;
		public Guid Player1ActionBarId;
		public Guid Player2ActionBarId;
		public Guid Player1GearBagId;
		public Guid Player2GearBagId;
	}

}
