﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace DigCraftFight
{
	public class PlotOfLand
	{
		public const int SizeInCell = 128;
		public const int SizeInCoord = SizeInCell * Tools.CellSizeInCoord;

		public int Level;

		private ItemMap itemMap;



		private bool[] hasBlock;
		private bool[] lockedBlock;
		private Tuple<int, int>[] furnitureHeal;



		public PlotOfLand(int level)
		{
			this.Level = level;
			this.itemMap = new ItemMap(SizeInCell, SizeInCell);
			this.hasBlock = new bool[SizeInCell * SizeInCell];
			this.lockedBlock = new bool[SizeInCell * SizeInCell];
			this.furnitureHeal = new Tuple<int, int>[SizeInCell * SizeInCell];
		}

		private void refreshIsSolid()
		{
			Trace.WriteLine("refreshIsSolid");
			for (int j = 0; j < this.itemMap.Height; j++)
			{
				for (int i = 0; i < this.itemMap.Width; i++)
				{
					IntVector2 cell = new IntVector2(i, j);
					Item carpet = this.itemMap.GetCarpet(cell);
					if (carpet != null && carpet.ItemType == ItemType.GROUND_WATER)
					{
						this.hasBlock[j * SizeInCell + i] = this.isWaterBloking(cell);
					}
					else
					{
						Item furnitureItem = this.itemMap.GetFurniture(cell);
						if (furnitureItem == null &&
							(carpet.ItemType >= ItemType.DELIMITER_GROUND && carpet.ItemType <= ItemType.DELIMITER_GROUND_BLOCKING))
						{
							this.hasBlock[j * SizeInCell + i] = false;
						}
						else
						{
							this.hasBlock[j * SizeInCell + i] = true;
						}
					}
				}
			}
		}

		

		public void RefreshLockedBlock()
		{
			this.lockedBlock = new bool[SizeInCell * SizeInCell];
			foreach (Item item in this.itemMap.GetDistinctItems())
			{
				if (item != null && item.BossZone != null)
				{
					if (item.BossZone.BossDefeated)
					{
						continue;
					}
					for (int j = 0; j < item.BossZone.LockCells.Height; j++)
					{
						for (int i = 0; i < item.BossZone.LockCells.Width; i++)
						{
							int x = item.BossZone.LockCells.X + i;
							int y = item.BossZone.LockCells.Y + j;
							if (Tools.IsInRange(x, 0, PlotOfLand.SizeInCell)
								&& Tools.IsInRange(y, 0, PlotOfLand.SizeInCell))
							{
								this.lockedBlock[y * SizeInCell + x] = true;
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Can wal into water only if there is a free space near.
		/// </summary>
		private bool isWaterBloking(IntVector2 cell)
		{
			return !this.getCardinal(cell).Any(
						c => c.Item1.ItemType != ItemType.GROUND_WATER
							&& c.Item1.ItemType != ItemType.GROUND_EARTH
							&& c.Item2 == null);
		}

		private List<Tuple<Item, Item>> getCardinal(IntVector2 cell)
		{
			var returned = new List<Tuple<Item, Item>>();
			foreach (var d in Tools.CardinalPoints)
			{
				int neighborX = cell.X + d.X;
				int neighborY = cell.Y + d.Y;
				if (Tools.IsInRange(neighborX, 0, PlotOfLand.SizeInCell)
					&& Tools.IsInRange(neighborY, 0, PlotOfLand.SizeInCell))
				{
					var neighbor = new IntVector2(neighborX, neighborY);
					returned.Add(new Tuple<Item, Item>(this.GetCarpet(neighbor), this.GetFurniture(neighbor)));
				}
				else
				{
					///TODO: look on other PlotOfLand if they are loaded.
				}
			}

			return returned;
		}



		/// <summary>
		/// Ground or water
		/// </summary>
		public bool HasBlock(IntVector2 localCell)
		{
			return this.hasBlock[localCell.Y * SizeInCell + localCell.X];
		}

		public Item GetCarpet(IntVector2 localCell)
		{
			return this.itemMap.GetCarpet(localCell);
		}

		public Item GetFurniture(IntVector2 localCell)
		{
			return this.itemMap.GetFurniture(localCell);
		}


		public bool IsLocked(IntVector2 localCell)
		{
			return this.lockedBlock[localCell.Y * SizeInCell + localCell.X];
		}
		public void Draw(int worldTick, Screen screen, IntVector2 localCell, LongVector2 target)
		{
			Item carpetItem = this.GetCarpet(localCell);
			Item furnitureItem = this.GetFurniture(localCell);

			Color carpetColor = Color.White;
			Color furnitureColor = Color.White;
			if (carpetItem.ItemType == ItemType.GROUND_WATER)
			{
				if (this.HasBlock(localCell))
				{
					carpetColor = Color.LightBlue;
				}
			}                
			else if (this.IsLocked(localCell))
			{
				furnitureColor = Color.LightGreen;
			}
          
			screen.Draw64(carpetItem.GetTextureRect(), target, carpetColor);
			if (furnitureItem != null)
			{
				int index = localCell.Y * SizeInCell + localCell.X;
				if (this.furnitureHeal[index] != null && worldTick - this.furnitureHeal[index].Item1 < 5)
				{
					furnitureColor = Color.Red;
				}
				screen.Draw64(furnitureItem.GetTextureRect(), target, furnitureColor);
			}
		}

		public void DrawPreviousForLoadMenu(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch, Ressource ressource, IntVector2 localHome)
		{			
			int imgWidthCell = 95;
			int imgHeightCell = 35;
			int yStart = Tools.InRange(localHome.Y - imgHeightCell / 2, 1, SizeInCell - imgHeightCell - 5);
			int xStart = Tools.InRange(localHome.X - imgWidthCell / 2, 1, SizeInCell - imgHeightCell - 5);
			
			int yEnd = yStart + imgHeightCell;
			int xEnd = xStart + imgWidthCell;
			IntVector2 topLeft = new IntVector2(20, 20);
			for (int y = yStart; y < yEnd; y++)
			{
				for (int x = xStart; x < xEnd; x++)
				{
					Rectangle target = new Rectangle(topLeft.X + (x - xStart) * 8, topLeft.Y + (y - yStart) * 8, 8, 8);
					IntVector2 localCell = new IntVector2(x, y);
					Item carpetItem = this.GetCarpet(localCell);
					Color carpetColor = Color.White;
					Color furnitureColor = Color.White;
					if (carpetItem.ItemType == ItemType.GROUND_WATER)
					{
						if (this.HasBlock(localCell))
						{
							carpetColor = Color.LightBlue;
						}
					}
					else if (this.IsLocked(localCell))
					{
						furnitureColor = Color.LightGreen;
					}
					if(carpetItem != null)
					{
						var texture = carpetItem.GetTextureRect();
						spriteBatch.Draw(texture.GetTileset(ressource), target, texture.Rectangle, carpetColor);
					}
					Item furnitureItem = this.GetFurniture(localCell);
					if (furnitureItem != null)
					{
						var texture = furnitureItem.GetTextureRect();
						spriteBatch.Draw(texture.GetTileset(ressource), target, texture.Rectangle, furnitureColor);
					}
				}
			}
		}



		public void RemoveFurniture(IntVector2 localCell)
		{
			this.itemMap.RemoveFurniture(localCell);
			this.furnitureHeal[localCell.Y * SizeInCell + localCell.X] = null;
			this.refreshIsSolid();
		}

		public bool SetFurniture(Item item, IntVector2 localCell)
		{
			this.itemMap.SetFurniture(item, localCell);
			this.furnitureHeal[localCell.Y * SizeInCell + localCell.X] = null;
			this.refreshIsSolid();
			return true;
		}
		public bool SetCarpet(Item item, IntVector2 localCell)
		{
			this.itemMap.SetCarpet(item, localCell);
			this.refreshIsSolid();///Since water flood
			return true;
		}

		public int HitFurniture(int tick, IntVector2 localCell, int damage)
		{

			int index = localCell.Y * SizeInCell + localCell.X;
			if (this.furnitureHeal[index] == null || tick - this.furnitureHeal[index].Item1 > 60 * 60)
			{
				this.furnitureHeal[index] = new Tuple<int, int>(tick, damage);
			}
			else
			{
				this.furnitureHeal[index] = new Tuple<int, int>(tick, this.furnitureHeal[index].Item2 + damage);
			}
			return this.furnitureHeal[index].Item2;
		}




		
		public void GeneratorSetCapet(Item item, IntVector2 localCell)
		{
			this.itemMap.SetCarpet(item, localCell);
		}
		public void GeneratorSetFurniture(Item item, IntVector2 localCell)
		{
			this.itemMap.SetFurniture(item, localCell);
		}
		public void GeneratorSet(Item carpet, Item furniture, IntVector2 localCell)
		{
			this.GeneratorSetCapet(carpet, localCell);
			this.GeneratorSetFurniture(furniture, localCell);
		}
		public void GeneratorFillZone(Item carpet, Item furniture, Rectangle zone)
		{
			for (int j = 0; j < zone.Height; j++)
			{
				for (int i = 0; i < zone.Width; i++)
				{
					int x = Tools.InRange(zone.Left + i, 0, PlotOfLand.SizeInCell - 1);
					int y = Tools.InRange(zone.Top + j, 0, PlotOfLand.SizeInCell - 1);
					IntVector2 localCell = new IntVector2(x, y);
					this.itemMap.SetCarpet(carpet, localCell);
					this.itemMap.SetFurniture(furniture, localCell);
				}
			}
		}
		public void GeneratorFillZone(ItemType carpet, ItemType? furniture, IntVector2 localCell, int width, int height)
		{
			this.GeneratorFillZone(
				 Item.New(carpet),
				furniture == null ? null : Item.New(furniture.Value),
				new Rectangle(localCell.X, localCell.Y, width, height));
		}
		public void GeneratorFinish()
		{
			this.refreshIsSolid();
			this.RefreshLockedBlock();
		}


		


		public ComPlotOfLand toCom()
		{
			ComPlotOfLand com = new ComPlotOfLand();
			com.Level = this.Level;
			com.ItemMap = this.itemMap.toCom();
			return com;
		}

		public PlotOfLand(ComPlotOfLand com)
		{
			this.Level = com.Level;
			this.itemMap = new ItemMap(com.ItemMap);

			this.hasBlock = new bool[SizeInCell * SizeInCell];
			this.lockedBlock = new bool[SizeInCell * SizeInCell];
			this.furnitureHeal = new Tuple<int, int>[SizeInCell * SizeInCell];
			this.refreshIsSolid();
			this.RefreshLockedBlock();
		}

		
	}



}
