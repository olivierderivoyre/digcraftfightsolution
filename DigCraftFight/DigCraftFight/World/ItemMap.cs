﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace DigCraftFight
{
	public class ItemMap
	{

		private struct CellItems
		{
			public ushort Carpet;
			public ushort Furniture;
		}

		public readonly int Width;
		public readonly int Height;
		private List<Item> items = new List<Item>() { null };///Index=0
		private CellItems[] cells;


		public ItemMap(int width, int height)
		{
			this.Width = width;
			this.Height = height;
			if(this.Width < 1) throw new ArgumentException();
			if(this.Height < 1) throw new ArgumentException();

			this.cells = new CellItems[this.Width * this.Height];
		}


		public ReadOnlyCollection<Item> GetDistinctItems()
		{
			return items.AsReadOnly();
		}

		private ushort add(Item item)
		{
			if (item == null)
			{
				return 0;
			}

			for (int i = 1; i < this.items.Count; i++)
			{
				if (this.items[i] == item)
				{
					return (ushort)i;
				}
			}
			
			///Not found
			this.items.Add(item);
			return (ushort)(this.items.Count - 1);
		}

		private int getIndex(IntVector2 localCell)
		{
			if (localCell.X < 0) throw new ArgumentException("x:" + localCell.X);
			if (localCell.Y < 0) throw new ArgumentException("y:" + localCell.Y);
			if (localCell.X >= this.Width) throw new ArgumentException("x:" + localCell.X);
			if (localCell.Y >= this.Height) throw new ArgumentException("y:" + localCell.Y);
			return localCell.Y * this.Width + localCell.X;
		}

		private CellItems getCellItems(IntVector2 localCell)
		{
			int cellIndex = getIndex(localCell);
			CellItems cellItems = this.cells[cellIndex];
			return cellItems;
		}
		public Item GetFurniture(IntVector2 localCell)
		{
			CellItems cellItems = getCellItems(localCell);
			Item furnitureItem = this.items[cellItems.Furniture];
			return furnitureItem;
		}

		public Item GetCarpet(IntVector2 localCell)
		{
			CellItems cellItems = getCellItems(localCell);
			Item item = this.items[cellItems.Carpet];
			return item;
		}

		public void RemoveFurniture(IntVector2 localCell)
		{
			int cellIndex = getIndex(localCell);
			this.cells[cellIndex].Furniture = 0;			
		}

		public void SetCarpet(Item item, IntVector2 localCell)
		{
			int cellIndex = getIndex(localCell);
			ushort itemIndex = this.add(item);
			this.cells[cellIndex].Carpet = itemIndex;
		}
		
		public void SetFurniture(Item item, IntVector2 localCell)
		{
			int cellIndex = getIndex(localCell);
			ushort itemIndex = this.add(item);
			this.cells[cellIndex].Furniture = itemIndex;						
		}
		
		public ComItemMap toCom()
		{
			ComItemMap com = new ComItemMap();
			com.Width = this.Width;
			com.Height = this.Height;
			com.Items = this.items.Select(i => Item.ToComItem(i)).ToArray();
			List<ComItemSet> allItemSet = new List<ComItemSet>();
			foreach (Item item in this.items)
			{
				if (item != null)
				{
					item.fillComItemSet(allItemSet);
				}
			}			
			com.ItemSetList = allItemSet.ToArray();
			com.CarpetCsv = ComItemMap.ConvertToCsv( this.cells.Select(c => c.Carpet).ToArray(), com.Width);
			com.FurnitureCsv = ComItemMap.ConvertToCsv(this.cells.Select(c => c.Furniture).ToArray(), com.Width); ;
			return com;
		}

		public ItemMap(ComItemMap com) : this(com.Width, com.Height)
		{
			this.items = com.Items.Select(c => Item.FromComItem(c)).ToList();
			if (com.ItemSetList != null)
			{
				foreach (Item item in this.items)
				{
					if (item != null)
					{
						item.initFromComItemSet(com.ItemSetList);
					}
				}
			}
			ushort[] carpetArray = ComItemMap.ConvertToArray(com.CarpetCsv, com.Width, com.Height);
			ushort[] furnitureArray = ComItemMap.ConvertToArray(com.FurnitureCsv, com.Width, com.Height);

			for (int i = 0; i < this.cells.Length; i++)
			{
				this.cells[i] = new CellItems() { Carpet = carpetArray[i], Furniture = furnitureArray[i] };
			}			
		}

		public IEnumerable<IntVector2> GetAllCells()
		{
			for (int j = 0; j < this.Height; j++)
			{
				for (int i = 0; i < this.Width; i++)
				{
					yield return new IntVector2(i, j);
				}
			}
		}
	}
}
