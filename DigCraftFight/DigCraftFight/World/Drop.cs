﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
    public class Drop
    {
        public readonly LongVector2 Position;
        private Item item;
		private bool lootWhenPlayerIsNear;

		private Drop(Item item, LongVector2 position, bool lootWhenPlayerIsNear)
        {
            this.item = item;
            this.Position = position;
			this.lootWhenPlayerIsNear = lootWhenPlayerIsNear;
        }


		public static void AddDropAround(World world, Item item, LongVector2 position)
		{
			world.Add(new Drop(item, getDropPosition(position), true));
		}

		public static Drop CreateDecorativeDrop(World world, Item item, LongVector2 position)
		{
			return new Drop(item, position, false);			
		}

		private static LongVector2 getDropPosition(LongVector2 position)
		{
			return Tools.CenterCharacterCell(position) + new IntVector2(Tools.Rand.Next(3), Tools.Rand.Next(3)) * Tools.QuaterCellSizeInCoord;
		}
        public void Draw(Screen screen)
        {
            screen.Draw32(this.item.GetTextureRect(), this.Position, Color.White);            
        }

        public void Update(World world)
        {
			if (!this.lootWhenPlayerIsNear)
			{
				return;
			}
			Player player = world.GetAlivePlayerAtPosition(this.Position);
			if (player != null)
            {
				if (world.MainBagTryAddByPlayerLoot(this.item, player))
                {
                    world.Remove(this);
                }
            }
        }
    }
}
