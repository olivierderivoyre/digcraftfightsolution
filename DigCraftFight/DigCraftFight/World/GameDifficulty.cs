﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	public class GameDifficulty
	{
		public int RecommendedGroundLevel = 0;
		public int PowerLevel = 0;
		public int CraftLevel = 0;


		/// <summary>
		/// Return groundLevel / 5
		/// </summary>
		public int GenerateOreLevel(int groundLevel)
		{
			int targetOreLevel;
			if (groundLevel <= 5)
			{
				targetOreLevel = 0;
			}
			else if (groundLevel <= 9)
			{
				targetOreLevel = 1;
			}
			else if (Math.Abs(groundLevel - this.RecommendedGroundLevel) <= 3)
			{
				targetOreLevel = this.CraftLevel;
				if (groundLevel > this.RecommendedGroundLevel)
				{
					if (Tools.RandomWin(10))
					{
						targetOreLevel++;
					}
				}
				if (groundLevel < this.RecommendedGroundLevel)
				{
					if (Tools.RandomWin(10))
					{
						targetOreLevel--;
					}
				}
			}
			else
			{
				targetOreLevel = this.CraftLevel + (groundLevel - this.RecommendedGroundLevel) / 5;
			}
			
			if (Tools.RandomWin(10))
			{
				return Math.Max(0, targetOreLevel + 1);
			}
			if (Tools.RandomWin(10))
			{
				return Math.Max(0, targetOreLevel - 1);
			}
			return Math.Max(0, targetOreLevel);
		}


		public static double GetMobDamage(int level)
		{
			if (level > 10)
			{
				return 20 * Math.Pow(1.2, (level - 10));
			}
			switch (level)
			{
				case 0: return 5;
				case 1: return 10;
				case 2: return 12;
				case 3: return 13;
				case 4: return 14;
				case 5: return 15;
				case 6: return 16;
				case 7: return 17;
				case 8: return 18;
				case 9: return 19;
				case 10: return 20;
			}
			throw new ArgumentException();
		}

		

		public static double GetWeaponDamage(int level)
		{
			if (level > 10)
			{
				return 20 * Math.Pow(1.2, (level - 10));
			}
			switch (level)
			{
				case 0: return 1;
				case 1: return 2;
				case 2: return 3;
				case 3: return 5;
				case 4: return 7;
				case 5: return 9;
				case 6: return 11;
				case 7: return 13;
				case 8: return 15;
				case 9: return 17;
				case 10: return 20;
			}
			throw new ArgumentException();
		}
		public static double GetMobHealth(int level)
		{
			if (level > 10)
			{
				return 100 * Math.Pow(1.2, (level - 10));
			}
			switch (level)
			{
				case 0: return 2;
				case 1: return 6;
				case 2: return 9;
				case 3: return 15;
				case 4: return 21;
				case 5: return 30;
				case 6: return 40;
				case 7: return 50;
				case 8: return 65;
				case 9: return 80;
				case 10: return 100;
			}
			throw new ArgumentException();
		}
		public static double GetArmorHealth(int level)
		{
			if (level > 10)
			{
				return 60 * Math.Pow(1.2, (level - 10));
			}
			switch (level)
			{
				case 0: return 30;
				case 1: return 30;
				case 2: return 30;
				case 3: return 30;
				case 4: return 30;
				case 5: return 30;
				case 6: return 30;
				case 7: return 30;
				case 8: return 60;
				case 9: return 60;
				case 10: return 60;
			}
			throw new ArgumentException();
		}
	}
}
