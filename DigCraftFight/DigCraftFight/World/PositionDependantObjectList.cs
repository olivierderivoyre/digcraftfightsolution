﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	public class PositionDependantObjectList
	{
		public readonly  HashSet<Drop> drops = new HashSet<Drop>();
		public readonly HashSet<Mob> mobs = new HashSet<Mob>();
		public readonly List<Tuple<Item, LongVector2, int>> cleverBlocks = new List<Tuple<Item, LongVector2, int>>();
		public readonly List<IAlive> aliveObjectList = new List<IAlive>();


		public PositionDependantObjectList GetSubset(LongVector2 topLeft, int width, int height)
		{
			PositionDependantObjectList r = new PositionDependantObjectList();
			r.drops.UnionWith(this.drops.Where(i => i.Position.IsInside(topLeft, width, height)));
			r.mobs.UnionWith(this.mobs.Where(i => i.Position.IsInside(topLeft, width, height)));
			r.cleverBlocks.AddRange(this.cleverBlocks.Where(i => i.Item2.IsInside(topLeft, width, height)));
			r.aliveObjectList.AddRange(this.aliveObjectList.Where(i => i.Position.IsInside(topLeft, width, height)));
			return r;
		}

        public bool cleanUp()
        {
            int nbObjectDeleted = 0;
            nbObjectDeleted += this.mobs.RemoveWhere(m => m.IsDead);
            return nbObjectDeleted > 0;
        }
	}
}
