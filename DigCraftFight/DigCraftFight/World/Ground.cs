﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;

namespace DigCraftFight
{
    public class Ground
    {

        private Dictionary<string, PlotOfLand> plotOfLands = new Dictionary<string, PlotOfLand>();
		private LongVector2 lastKeyInPlot;
		private PlotOfLand lastPlotOfLand;
		private readonly string saveDirectory;
		private readonly LevelGenerator levelGenerator;

		public Ground(string worldCode)
		{
			this.saveDirectory = Path.Combine(Tools.SaveDirectory, worldCode, "Ground");
			Directory.CreateDirectory(this.saveDirectory);			
			this.levelGenerator = new LevelGenerator(this);
		}

		public static LongVector2 PlotOfLandToTopLeftCoord(long plotOfLandX, long plotOfLandY)
		{
			return new LongVector2(plotOfLandX, plotOfLandY) * PlotOfLand.SizeInCoord;
		}

        private PlotOfLand getPlotOfLand(long x, long y)
        {            
			long plotOfLandX = x / PlotOfLand.SizeInCoord;
            long plotOfLandY = y / PlotOfLand.SizeInCoord;
            PlotOfLand plotOfLand = tryGetPlotOfLand(plotOfLandX, plotOfLandY);
            if (plotOfLand != null)
            {
                return plotOfLand;
            }
			this.levelGenerator.GeneratePlotOfLand(plotOfLandX, plotOfLandY);
			save(plotOfLandX + "x" + plotOfLandY);
            return tryGetPlotOfLand(plotOfLandX, plotOfLandY);             
        }
		
        private PlotOfLand tryGetPlotOfLand(long plotOfLandX, long plotOfLandY)
        {
            PlotOfLand outValue;
			LongVector2 keyInPlot = new LongVector2(plotOfLandX , plotOfLandY);
			if (keyInPlot == this.lastKeyInPlot)
            {
                return lastPlotOfLand;
            }
			string key = plotOfLandX + "x" + plotOfLandY;
            if (this.plotOfLands.TryGetValue(key, out outValue))
            {
				this.lastKeyInPlot = keyInPlot;
                this.lastPlotOfLand = outValue;
                return outValue;
            }
            ///try load from disk here
            string filename = Path.Combine(this.saveDirectory, key + ".xml");
            if (File.Exists(filename))
            {

                Trace.WriteLine("Load " + filename);
                using (var stream = File.OpenRead(filename))
                {
                    ComPlotOfLand com = (ComPlotOfLand)Ground.serializer.Deserialize(stream);
                    PlotOfLand plotOfLand = new PlotOfLand(com);
                    this.plotOfLands.Add(key, plotOfLand);
                    return plotOfLand;
                }

            }
            ///Never generated
            return null;
        }

		public bool IsPlotOfLandLoaded(LongVector2 position)
		{
			long plotOfLandX = position.X / PlotOfLand.SizeInCoord;
			long plotOfLandY = position.Y / PlotOfLand.SizeInCoord;
			LongVector2 keyInPlot = new LongVector2(plotOfLandX, plotOfLandY);
			if (keyInPlot == this.lastKeyInPlot)
			{
				return true;
			}
			string key = plotOfLandX + "x" + plotOfLandY;
			return this.plotOfLands.ContainsKey(key);			
		}

		private static XmlSerializer serializer = new XmlSerializer(typeof(ComPlotOfLand));
		
		public void SaveAll()
		{
			foreach (string key in this.plotOfLands.Keys)
			{
				save(key);
			}
		}
		public void SaveLocal()
		{
			save(this.lastKeyInPlot.X + "x" + this.lastKeyInPlot.Y);
		}

		private void save(string key)
		{
			ComPlotOfLand com = this.plotOfLands[key].toCom();
			string filename = Path.Combine(this.saveDirectory, key + ".xml");
			Trace.WriteLine("Save " + filename);
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
			using (var stream = File.OpenWrite(filename))
			{
				Ground.serializer.Serialize(stream, com);
			}
			Trace.WriteLine("Saved " + filename);
		}

        internal int getMaxLevelAround(long plotOfLandX, long plotOfLandY)
        {
            int max = -1;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    PlotOfLand land = tryGetPlotOfLand(plotOfLandX + i, plotOfLandY + j);
                    if (land != null && land.Level > max)
                    {
                        max = land.Level;
                    }
                }
            }
            return max;
        }
		internal PlotOfLand newPlotOfLand(long plotOfLandX, long plotOfLandY, int level)
		{
			PlotOfLand newLand = new PlotOfLand(level);
			string key = plotOfLandX + "x" + plotOfLandY;
			this.plotOfLands[key] = newLand;
			return newLand;
		}

        

        public IntVector2 getLocalCell(long x, long y)
        {
            int cellX = ((int)(x % PlotOfLand.SizeInCoord)) / Tools.CellSizeInCoord;
            int cellY = ((int)(y % PlotOfLand.SizeInCoord)) / Tools.CellSizeInCoord;
            return new IntVector2(cellX, cellY);
        }
        private PlotOfLand getPlotOfLand(LongVector2 position)
        {
            return getPlotOfLand(position.X, position.Y);
        }
        public IntVector2 getLocalCell(LongVector2 position)
        {
            return getLocalCell(position.X, position.Y);
        }

        public bool HasBlock(long x, long y)
        {
            return this.getPlotOfLand(x, y).HasBlock(getLocalCell(x, y));
        }
		public bool HasBlock(LongVector2 position)
		{
			return HasBlock(position.X, position.Y);
		}
        public Item GetFurniture(LongVector2 position)
        {
            return this.getPlotOfLand(position).GetFurniture(getLocalCell(position));
        }
        public Item GetCarpet(LongVector2 position)
        {
            return this.getPlotOfLand(position).GetCarpet(getLocalCell(position));
        }
		public int GetGroundLevel(LongVector2 position)
		{
			return this.getPlotOfLand(position).Level;
		}

		public bool IsLocked(LongVector2 position)
		{
			return this.getPlotOfLand(position).IsLocked(getLocalCell(position));
		}
		public void RefreshLockedBlock(LongVector2 position)
		{
			this.getPlotOfLand(position).RefreshLockedBlock();
		}

		public void Draw(int worldTick, Screen screen)
        {
            LongVector2 topLeftCell = (screen.TopLeft / Tools.CellSizeInCoord) * Tools.CellSizeInCoord;

            long xMinCeilled = (topLeftCell.X / Tools.CellSizeInCoord) * Tools.CellSizeInCoord;
            long xMaxCeilled = (((topLeftCell.X + screen.WidthCoord) / Tools.CellSizeInCoord) + 2) * Tools.CellSizeInCoord;
            long yMinCeilled = (topLeftCell.Y / Tools.CellSizeInCoord) * Tools.CellSizeInCoord;
            long yMaxCeilled = (((topLeftCell.Y + screen.HeightCoord) / Tools.CellSizeInCoord) + 2) * Tools.CellSizeInCoord;


            for (long x = xMinCeilled; x < xMaxCeilled; x += Tools.CellSizeInCoord)
            {
                for (long y = yMinCeilled; y < yMaxCeilled; y += Tools.CellSizeInCoord)
                {
					this.getPlotOfLand(x, y).Draw(worldTick, screen, getLocalCell(x, y), new LongVector2(x, y));                    
                }
            }
        }


        public IntVector2 LimitCharacterMove(LongVector2 currentPosition, IntVector2 direction)
        {
            IntVector2 constrainedDirection = direction;
            LongVector2 quaterCellPosition = currentPosition / Tools.QuaterCellSizeInCoord;
            if (quaterCellPosition.X % 4 != 0 && quaterCellPosition.Y % 4 != 0)
            {
                return constrainedDirection;///part of the player is already in the cell 
            }
            if (quaterCellPosition.Y % 4 == 0 && direction.Y != 0)///Player want to go up or down
            {
                if (HasBlock(currentPosition.X, currentPosition.Y + direction.Y * Tools.CellSizeInCoord))
                {
                    constrainedDirection.Y = 0;              
                }
                if (quaterCellPosition.X % 4 != 0)
                {
                    if (HasBlock(currentPosition.X + Tools.CellSizeInCoord, currentPosition.Y + direction.Y * Tools.CellSizeInCoord))
                    {
                        constrainedDirection.Y = 0;                     
                    }
                }
            }
            if (quaterCellPosition.X % 4 == 0 && direction.X != 0)///Player want to go left or right
            {
                if (HasBlock(currentPosition.X + direction.X * Tools.CellSizeInCoord, currentPosition.Y))
                {
                    constrainedDirection.X = 0;
                }
                if (quaterCellPosition.Y % 4 != 0)
                {
                    if (HasBlock(currentPosition.X + direction.X * Tools.CellSizeInCoord, currentPosition.Y + Tools.CellSizeInCoord))
                    {
                        constrainedDirection.X = 0;                        
                    }
                }

            }
            ///Diagonal pariculiar case: can we go in X+1, Y+1?
            if (quaterCellPosition.X % 4 == 0 && quaterCellPosition.Y % 4 == 0)
            {
                if (direction.X != 0 && direction.Y != 0)
                {
                    if (HasBlock(
                        currentPosition.X + direction.X * Tools.CellSizeInCoord,
                        currentPosition.Y + direction.Y * Tools.CellSizeInCoord))
                    {
                        constrainedDirection = IntVector2.Zero;                        
                    }
                }
            }
            return constrainedDirection;
        }

        /// <summary>
        /// Round corner
        /// </summary>
        public IntVector2 HelpPlayerMove(LongVector2 currentPosition, IntVector2 inputDirection)
        {
            LongVector2 quaterCellPosition = currentPosition / Tools.QuaterCellSizeInCoord;           
            if (inputDirection.X != 0 && inputDirection.Y == 0)///Player want to go right, but there is a block
            {
                if (quaterCellPosition.Y % 4 != 0)
                {
                    ///Try to go up
                    if (!HasBlock(currentPosition.X, currentPosition.Y)
                        && !HasBlock(currentPosition.X + Tools.CellSizeInCoord * inputDirection.X, currentPosition.Y))
                    {
                        return new IntVector2(0, -1);
                    }
                    ///Try go down
                    if (!HasBlock(currentPosition.X, currentPosition.Y + Tools.CellSizeInCoord)
                        && !HasBlock(currentPosition.X + Tools.CellSizeInCoord * inputDirection.X, currentPosition.Y + Tools.CellSizeInCoord))
                    {
                        return new IntVector2(0, +1);
                    }
                }
            }
            if (inputDirection.X == 0 && inputDirection.Y != 0)///Player want to go up, but there is a block
            {
                if (quaterCellPosition.X % 4 != 0)
                {
                    ///Try to go left
                    if (!HasBlock(currentPosition.X, currentPosition.Y)
                        && !HasBlock(currentPosition.X, currentPosition.Y + Tools.CellSizeInCoord * inputDirection.Y))
                    {
                        return new IntVector2(-1, 0);
                    }
                    ///Try go right
                    if (!HasBlock(currentPosition.X + Tools.CellSizeInCoord, currentPosition.Y)
                        && !HasBlock(currentPosition.X + Tools.CellSizeInCoord, currentPosition.Y + Tools.CellSizeInCoord * inputDirection.Y))
                    {
                        return new IntVector2(+1, 0);
                    }
                }
            }
            return IntVector2.Zero;
        }



        public void RemoveFurniture(LongVector2 position)
        {
            this.getPlotOfLand(position).RemoveFurniture(getLocalCell(position));
        }      
        public void SetFurniture(Item item, LongVector2 position)
        {
             this.getPlotOfLand(position).SetFurniture(item, getLocalCell(position));
        }
        public void SetCarpet(Item item, LongVector2 position)
        {
            this.getPlotOfLand(position).SetCarpet(item, getLocalCell(position));
        }
        public int HitFurniture(int tick,LongVector2 position, int damage)
        {
            return this.getPlotOfLand(position).HitFurniture(tick, getLocalCell(position), damage);
        }


		public void DrawPreviousForLoadMenu(SpriteBatch spriteBatch, Ressource ressource, LongVector2 homePosition)
		{
			this.getPlotOfLand(homePosition).DrawPreviousForLoadMenu(spriteBatch, ressource, getLocalCell(homePosition));
		}
	}
}
