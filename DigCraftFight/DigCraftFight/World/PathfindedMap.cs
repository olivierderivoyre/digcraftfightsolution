﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	/// <summary>
	/// Relative map over AlgoMapPaths.
	/// Used for NPC to know where they can go.
	/// </summary>
	public class PathfindedMap
	{
		
		private readonly MapReferencial mapRef;
		private readonly AlgoMapPaths mapPaths;

		public PathfindedMap(World world, LongVector2 home, LongVector2 npcPosition)
		{
			if (home != Tools.FloorToCell(home))
			{
				throw new ArgumentException();
			}
			this.mapRef = new MapReferencial(home);
			bool[] blockedCells = this.mapRef.NewBool2DArray();
			foreach (IntVector2 local in this.mapRef.GetAllMapCoord())
			{
				blockedCells[this.mapRef.GetIndex(local)] = world.Ground.HasBlock(this.mapRef.ToWorldPosition(local));	
			}
			IntVector2 localStart = this.mapRef.GetLocalCoord(npcPosition);
			this.mapPaths = new AlgoMapPaths(blockedCells, mapRef.GetLocalMapSize(), localStart);			
		}
						
		public int GetDistanceInTick(LongVector2 tartgetPosition)
		{
			int distance = this.mapPaths.GetDistance(this.mapRef.GetLocalCoord(tartgetPosition));
			if (distance < 0)
			{
				return -1;
			}
			return distance * 90 / 10;
		}

		public Queue<LongVector2> GetPathTo(LongVector2 targetPosition)
		{

			IntVector2 localTarget = this.mapRef.GetLocalCoord(targetPosition);			
			List<IntVector2> path = this.mapPaths.GetPath(localTarget);
			if (path == null)
			{
				return null;
			}
			if (path.Count == 0)
			{
				return new Queue<LongVector2>();
			}
			path.RemoveAt(path.Count - 1);///To be near the furniture is ok.
			return new Queue<LongVector2>(path.Select(l => this.mapRef.ToWorldPosition(l)));
		}

		public LongVector2 GetARandomPosition()
		{
			return this.mapRef.ToWorldPosition(this.mapRef.GetARandomCoord());
		}

	
		public IEnumerable<LongVector2> GetAllAccessiblePositions()
		{
			foreach (IntVector2 local in this.mapRef.GetAllMapCoord())
			{
				if (this.mapPaths.GetDistance(local) < 0)
				{
					continue;
				}
				yield return this.mapRef.ToWorldPosition(local);
			}
		}

		public static bool IsInRange(LongVector2 center, LongVector2 to)
		{
			return new MapReferencial(center).IsInRange(to);
		}
	}
}
