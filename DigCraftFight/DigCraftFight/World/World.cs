﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace DigCraftFight
{
    public class World
    {
		public readonly DigCraftFightGame Game;
		public readonly string WorldCode;
        private Screen screen;
        public Player player1;
        public Player player2;
        public Ground Ground;
        public ItemSet Bag;
		public ItemSet BinBag = new ItemSet(1, 1);
        private HashSet<Bullet> bullets = new HashSet<Bullet>();
        private List<Npc> npcList = new List<Npc>();

		private HashSet<LongVector2> loadedZones = new HashSet<LongVector2>();
		private PositionDependantObjectList allObjects = new PositionDependantObjectList();
		private PositionDependantObjectList visibleObjects = new PositionDependantObjectList();
		/// <summary>
		/// AliveZone is a square of 96x96 cells, with screen centered inside.
		/// </summary>
		private LongVector2 aliveZoneTopLeft;
        public int Tick = 0;
        public Mob Boss;

		public readonly GameDifficulty GameDifficulty;
		public LongVector2 PlayerHomePosition;
		public LongVector2 PlayerResurectPosition;


        public World(DigCraftFightGame game, string worldCode, Screen screen, LongVector2 playerHomePosition, Player player1, Player player2, ItemSet bag, Ground ground)
        {
			this.Game = game;
			this.WorldCode = worldCode;
            this.screen = screen;
			this.PlayerHomePosition = playerHomePosition;
			this.PlayerResurectPosition = playerHomePosition;
            this.player1 = player1;
            this.player2 = player2;
            this.Bag = bag;
            this.Ground = ground;
			this.GameDifficulty = new GameDifficulty();
        }
        public void Add(Bullet bullet)
        {
            this.bullets.Add(bullet);
        }

        public void Add(Mob mob)
        {
			this.allObjects.mobs.Add(mob);
			this.visibleObjects.mobs.Add(mob);
        }
        public void Remove(Mob mob)
        {
			//this.allObjects.mobs.Remove(mob);
			//this.visibleObjects.mobs.Remove(mob);
        }
        public void Add(Drop item)
        {
			this.allObjects.drops.Add(item);
			this.visibleObjects.drops.Add(item);
		}
        public void Remove(Drop item)
        {
			this.allObjects.drops.Remove(item);
			this.visibleObjects.drops.Remove(item);
		}
        public void Add(Npc item)
        {
            this.npcList.Add(item);
        }
        public void Remove(Npc item)
        {
            this.npcList.Remove(item);
        }
		public void Add(IAlive item)
		{
			this.allObjects.aliveObjectList.Add(item);
			this.visibleObjects.aliveObjectList.Add(item);
		}		
        public void AddCleverBlock(Item item, LongVector2 position)
        {
			this.allObjects.cleverBlocks.Add(new Tuple<Item, LongVector2, int>(item, position, this.Tick));
			this.visibleObjects.cleverBlocks.Add(new Tuple<Item, LongVector2, int>(item, position, this.Tick));
		}
      
        public void Draw(Screen screen, GameTime gameTime)
        {
            this.Ground.Draw(this.Tick, screen);
            foreach (Bullet item in this.bullets)
            {
                item.Draw(screen);
            }
			foreach (Mob item in this.visibleObjects.mobs)
            {
                item.Draw(screen);
            }
			foreach (Drop item in this.visibleObjects.drops)
            {
                item.Draw(screen);
            }
			foreach (Npc item in this.npcList)
            {
				item.Draw(this, screen);
            }
			foreach (var item in this.visibleObjects.aliveObjectList)
			{
				item.Draw(screen);
			}
            if (player1.Position.Y < player2.Position.Y)
            {
                this.player1.Draw(this, this.screen);
                this.player2.Draw(this, this.screen);
            }
            else
            {
                this.player2.Draw(this, this.screen);
                this.player1.Draw(this, this.screen);
            }
			Mob mob = GetNearestEliteMob(this.screen.GetCenter());
			if (mob != null)
			{
				screen.DrawIndicator(mob.textureRect, mob.Position, Color.Gray);
			}
			screen.DrawIndicator(Item.ItemBed.GetTextureRect(), this.PlayerHomePosition, Color.LightGray);
			
        }
		
        public IntVector2 LimitPlayerMove(LongVector2 currentPosition, IntVector2 direction)
        {
            IntVector2 groundConstrainedDirection = this.Ground.LimitCharacterMove(currentPosition, direction);
            if (groundConstrainedDirection == IntVector2.Zero)
            {
                groundConstrainedDirection = this.Ground.HelpPlayerMove(currentPosition, direction);
            }
            if (groundConstrainedDirection == IntVector2.Zero)
            {
                return IntVector2.Zero;
            }
            LongVector2 newPosition = currentPosition + Tools.QuaterCellSizeInCoord * groundConstrainedDirection;
            IntVector2 screenConstrainedDirection = this.screen.LimitPlayerMove(newPosition, groundConstrainedDirection);            
            return screenConstrainedDirection;
        }
       

		private const int ZoneNbCell = 32;
		private void refreshAliveZone()
		{
			/// A zone is 32x32 cell square.
			///AliveZone is a square of 3 x 32 = 96 cells.
			///with the screen inside the [16x16, 80x80] rectangle (it's a [16x16, 64, 64] rect)

			bool isNeededToSwitchZone = !this.screen.TopLeft.IsInside(this.aliveZoneTopLeft + new IntVector2(1, 1) * ZoneNbCell /2 * Tools.CellSizeInCoord,
				2 * ZoneNbCell * Tools.CellSizeInCoord, 2 * ZoneNbCell * Tools.CellSizeInCoord);
			if (!isNeededToSwitchZone)
			{
				return;
			}
			///Do some computation
			this.aliveZoneTopLeft = FloorToZone(this.screen.TopLeft) - new IntVector2(ZoneNbCell, ZoneNbCell) * Tools.CellSizeInCoord;
			Trace.WriteLine("refreshAliveZone to [" + this.aliveZoneTopLeft + ", " + (ZoneNbCell * 3) + ", " + (ZoneNbCell * 3) + "]");
			this.visibleObjects = this.allObjects.GetSubset(this.aliveZoneTopLeft, ZoneNbCell * 3 * Tools.CellSizeInCoord, ZoneNbCell * 3 * Tools.CellSizeInCoord);

			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 3; i++)
				{
					tryLoadZone(this.aliveZoneTopLeft + new IntVector2(i, j) * ZoneNbCell * Tools.CellSizeInCoord);
				}
			}
		}

		private static LongVector2 FloorToZone(LongVector2 p)
		{
			return (p / (32 * Tools.CellSizeInCoord)) * 32 * Tools.CellSizeInCoord;
		}

		private void tryLoadZone(LongVector2 topLeftZone)
		{
			if (this.loadedZones.Contains(topLeftZone))
			{
				return;
			}
			this.loadedZones.Add(topLeftZone);
			Trace.WriteLine("Load zone " + topLeftZone);
			for (int j = 0; j < ZoneNbCell; j++)
			{
				for (int i = 0; i < ZoneNbCell; i++)
				{
					LongVector2 p = topLeftZone + new IntVector2(i, j) * Tools.CellSizeInCoord;
					
					Item carpet = this.Ground.GetCarpet(p);
					carpet.OnGroundLoad(this, p);

					Item furniture = this.Ground.GetFurniture(p);
					if (furniture != null)
					{
						furniture.OnGroundLoad(this, p);
					}					
				}
			}
		}

		public void Update()
        {
            this.Tick++;
			this.refreshAliveZone();

            foreach (Bullet bullet in this.bullets)
            {
                bullet.Update(this);               
            }
			foreach (Drop item in this.visibleObjects.drops.ToList())
            {
                item.Update(this);
            }                      
            foreach (Mob mob in this.visibleObjects.mobs.ToList())
            {
                mob.Update(this, screen);
            }
			for (int i = 0; i < this.visibleObjects.cleverBlocks.Count; i++)
            {
				var block = this.visibleObjects.cleverBlocks[i];
				bool alive = block.Item1.Update(this, block.Item2, this.Tick - block.Item3);
                if (!alive)
                {
					
					this.visibleObjects.cleverBlocks.Remove(block);
					this.allObjects.cleverBlocks.Remove(block);
				}
            }
			foreach (Npc item in this.npcList)
			{
				item.Update();
			}
			for (int i = 0; i < this.visibleObjects.aliveObjectList.Count; i++)
			{
				var item = this.visibleObjects.aliveObjectList[i];
				bool alive = item.Update();
				if (!alive)
				{
					this.visibleObjects.aliveObjectList.Remove(item);
					this.allObjects.aliveObjectList.Remove(item);
					i--;
				}
			}            
            this.lookForUpdateItems();
			if (this.Tick % 1201 == 0)///20 sec, prime number to avoid cycle effects
            {
				if (this.Ground.GetGroundLevel(this.screen.TopLeft) > 0)///No repop during tutorial
				{
					this.TryRepopMob();
				}
            }

            this.bullets.RemoveWhere(b => b.IsFisnish());
            if (this.visibleObjects.cleanUp())
            {
                this.allObjects.cleanUp();
            }


			if (this.Tick % (60 * 60 * 5) == 0)
			{
				this.Ground.SaveLocal();
			}
			if (!this.player1.IsActive && !this.player2.IsActive)
			{
				if (this.Game.youAreDeadMenu == null)
				{
					this.Game.youAreDeadMenu = new YouAreDeadMenu(this);
				}
			}
			else
			{
				if (this.Game.youAreDeadMenu != null)
				{
					this.Game.youAreDeadMenu = null;///Probably rez by a NPC
				}
			}
			
        }

		public void ResurectPlayers(LongVector2 position)
		{
			this.player1.Resurect(position);
			this.player2.Resurect(position);			
			this.screen.CenterOn(position);
		}

		public IEnumerable<Mob> GetVisibleMobsHitBy(LongVector2 targetPosition, int hitRange)
		{
			foreach (Mob unit in this.visibleObjects.mobs)
            {
				if (targetPosition.HitRectangle(unit.Position, Tools.CellSizeInCoord, Tools.CellSizeInCoord, hitRange))
                {
					yield return unit;
                }
            }			
        }

		public IEnumerable<Character> GetFriendlyUnitsHitBy(LongVector2 targetPosition, int hitRange)
		{
			foreach (Character unit in this.GetFriendlyUnits())
			{
				if (targetPosition.HitRectangle(unit.Position, Tools.CellSizeInCoord, Tools.CellSizeInCoord, hitRange))
				{
					yield return unit;
				}
			}
		}


		public Mob GetShootableMobAround(LongVector2 position, int range)
		{
			Mob mob = GetNearestMobInsideScreen(position);
			if (mob == null)
			{
				return null;
			}
			if (HasClearShoot(position, mob.Position, range))
			{
				return mob;
			}
			return null; 
		}
		
		public Mob GetVisibleMobInside(LongVector2 topLeft, int squareSize)
        {
            Mob bestMob = null;
			foreach (Mob mob in this.visibleObjects.mobs)
            {
				if (mob.Position.IsInside(topLeft, squareSize, squareSize))
                {
                    if (!mob.Immortal)
                    {
                        return mob;
                    }
                    else
                    {
                        bestMob = mob;
                    }
                }
            }
            return bestMob;
        }
       
		public Mob GetNearestMobInsideScreen(LongVector2 position)
        {
            int nearestDistance = int.MaxValue;
            int nearestMortalDistance = int.MaxValue;
            Mob nearestMob = null;
            Mob nearestMortalMob = null;
             LongVector2 topLeft = this.screen.TopLeft - new IntVector2(1, 1) * Tools.CellSizeInCoord;
            int width = this.screen.WidthCoord * Tools.PixelSizeInCoord + 2 * Tools.CellSizeInCoord;
            int height = this.screen.HeightCoord * Tools.PixelSizeInCoord + 2 * Tools.CellSizeInCoord;
			foreach (Mob mob in this.visibleObjects.mobs)
            {
                if (!mob.Position.IsInside(topLeft, width, height))
                {
                    continue;
                }
				int distante = IntVector2.Delta(position, mob.Position).GetDistance();                
                if (distante < nearestDistance)
                {
                    nearestDistance = distante;
                    nearestMob = mob;
                }
                if (!mob.Immortal)
                {
                    if (distante < nearestMortalDistance)
                    {
                        nearestMortalDistance = distante;
                        nearestMortalMob = mob;
                    }
                }
            }
            if (nearestMortalMob != null)
            {
                return nearestMortalMob;
            }
            return nearestMob;
        }

		public Mob GetNearestEliteMob(LongVector2 position)
		{
			Mob nearestMob = null;
			int nearestDistance = int.MaxValue;
			foreach (Mob mob in this.visibleObjects.mobs)
			{
				if (mob.IsElite)
				{
					int distante = IntVector2.Delta(position, mob.Position).GetDistance();
					if (distante < nearestDistance)
					{
						nearestDistance = distante;
						nearestMob = mob;
					}
				}
			}
			return nearestMob;
		}

		private IEnumerable<Character> GetFriendlyUnits()
		{
			return this.npcList.Where(p => !p.IsDead).Cast<Character>().Concat(new[] { this.player1, this.player2 }.Where(p => p.IsActive));
		}

		public IEnumerable<Character> GetDeadFriendlyUnits()
		{
			return this.npcList.Where(p => p.IsDead).Cast<Character>().Concat(new[] { this.player1, this.player2 }.Where(p => !p.NeedToPressStartToPlay && p.IsDead));
		}

		public Character GetNearestPlayer(LongVector2 position, int rangeInCoord)
		{
			int nearestDistance = int.MaxValue;
			Character nearestPlayer = null;
			LongVector2 topLeft = position - new IntVector2(rangeInCoord, rangeInCoord);
			foreach (Character character in this.GetFriendlyUnits())
			{
				int distance = IntVector2.Delta(position, character.Position).GetDistance();
				if (distance > rangeInCoord)
				{
					continue;
				}
				if (distance < nearestDistance)
				{
					nearestDistance = distance;
					nearestPlayer = character;
				}
			}
			return nearestPlayer;
		}

		public Player GetAlivePlayerTopLeftInside(LongVector2 topLeft, int width, int height)
		{
			if (this.player1.IsActive)
			{
				if (this.player1.Position.IsInside(topLeft, width, height))
				{
					return this.player1;
				}
			}
			if (this.player2.IsActive)
			{
				if (this.player2.Position.IsInside(topLeft, width, height))
				{
					return this.player2;
				}
			}
			return null;
		}

		public Player GetAlivePlayerAtPosition(LongVector2 targetPosition)
        {
			if (this.player1.IsActive)
			{
				if (targetPosition.IsInside(this.player1.Position, Tools.CellSizeInCoord, Tools.CellSizeInCoord))
				{
					return this.player1;
				}
			}
			if (this.player2.IsActive)
			{
				if (targetPosition.IsInside(this.player2.Position, Tools.CellSizeInCoord, Tools.CellSizeInCoord))
				{
					return this.player2;
				}
			}
            return null;
        }
		public IEnumerable<Npc> GetAllNpcs()
		{
			return this.npcList.AsReadOnly();
		}
		public Npc GetAliveNpcAtPosition(LongVector2 targetPosition)
		{
			foreach (Npc npc in this.npcList)
			{
				if (npc.IsDead)
				{
					continue;
				}
				if (npc.Position.IsInside(targetPosition - new IntVector2(1, 1) * 4 * Tools.QuaterCellSizeInCoord, 
					Tools.CellSizeInCoord + 9 * Tools.QuaterCellSizeInCoord,
					Tools.CellSizeInCoord + 9 * Tools.QuaterCellSizeInCoord))///Take large because NPC go only near target, not over it
				{
					return npc;
				}
			}			
			return null;
		}
		public bool HasPlayerInside(LongVector2 topLeft, int width, int height)
		{
			if (this.player1.Position.IsInside(topLeft, width, height))
			{
				return true;
			}
			if (this.player2.Position.IsInside(topLeft, width, height))
			{
				return true;
			}
			return false;
		}

		public bool HasAliveFriendlyUnitOver(LongVector2 topLeft)
		{
			return GetAlivePlayerAtPosition(topLeft) != null || GetAliveNpcAtPosition(topLeft) != null;
		}

        public bool HasClearShoot(LongVector2 topLeftFrom, LongVector2 topLeftTo, int range)
        {
            IntVector2 delta =  IntVector2.Delta(topLeftFrom, topLeftTo);
            int distance = delta.GetDistance();
            if (distance > range)
            {
                return false;
            }
            int cellDistance = distance / Tools.CellSizeInCoord;
            for (int i = 0; i < cellDistance; i++)
            {
                LongVector2 bulletPosition = topLeftFrom + delta * i / cellDistance;
                if (this.Ground.HasBlock(bulletPosition))
                {
                    return false;
                }
            }
            return true;
        }

        public IEnumerable<Player> GetAlivePlayers()
        {
            if (!this.player1.IsDead)
            {
                yield return this.player1;
            }
            if (!this.player2.IsDead)
            {
                yield return this.player2;
            }
        }

        public Npc GetSpeakingNpc()
        {
            foreach (Npc npc in this.npcList)
            {
				if (this.player1.IsActive)
				{
					if (npc.Position.IsInside(this.player1.Position - new IntVector2(Tools.CellSizeInCoord, Tools.CellSizeInCoord),
						3 * Tools.CellSizeInCoord, 3 * Tools.CellSizeInCoord))
					{
						return npc;
					}
				}
				if (this.player2.IsActive)
				{
					if (npc.Position.IsInside(this.player2.Position - new IntVector2(Tools.CellSizeInCoord, Tools.CellSizeInCoord),
						3 * Tools.CellSizeInCoord, 3 * Tools.CellSizeInCoord))
					{
						return npc;
					}
				}
            }
            return null;
        }
        
		public Item GetSpeakingWorkStation()
        {
            foreach (Player player in new[] { player1, player2 })
            {
				if (!player.IsActive)
				{
					continue;
				}
				foreach (IntVector2 nearCell in Tools.CardinalPoints)
				{
					Item item = this.Ground.GetFurniture(Tools.CenterCharacterCell(player.Position) + nearCell * Tools.CellSizeInCoord);
					if (item != null && item.WorkStation != null)
					{
						return item;
					}
				}
            }
            return null;
        }

		public LongVector2? GetWorkStationPosition(LongVector2 npcPosition, WorkStation target)
		{
			for (int y = -1; y <= 1; y++)
			{
				for (int x = -1; x <= 1; x++)
				{
					LongVector2 position = Tools.FloorToCell(npcPosition + new IntVector2(x, y) * Tools.CellSizeInCoord);
					Item item = this.Ground.GetFurniture(position);
					if (item != null && item.WorkStation == target)
					{
						return position;
					}
				}
			}
			return null;
		}
		/// <summary>
		/// Since the NPC only go near the item
		/// </summary>
		public LongVector2? GetItemPosition(LongVector2 npcPosition, Item target)
		{
			for (int y = -1; y <= 1; y++)
			{
				for (int x = -1; x <= 1; x++)
				{
					LongVector2 position = Tools.FloorToCell(npcPosition + new IntVector2(x, y) * Tools.CellSizeInCoord);
					Item furniture = this.Ground.GetFurniture(position);
					if (furniture != null && furniture == target)
					{
						return position;
					}
					Item carpet = this.Ground.GetCarpet(position);
					if (carpet != null && carpet == target)
					{
						return position;
					}
				}
			}
			return null;
		}


        private IEnumerable<LongVector2> getPopablePositions()
        {
            for (int x = -Tools.CellSizeInCoord * 3 / 2; x < screen.WidthCoord + Tools.CellSizeInCoord * 3 / 2; x+= Tools.CellSizeInCoord)
            {
                yield return screen.TopLeft + new IntVector2(x, -Tools.CellSizeInCoord * 3 / 2);
                yield return screen.TopLeft + new IntVector2(x, screen.HeightCoord + Tools.CellSizeInCoord * 3 / 2);
            }
            for (int y = -Tools.CellSizeInCoord * 3 / 2; y < screen.HeightCoord + Tools.CellSizeInCoord * 3 / 2; y += Tools.CellSizeInCoord)
            {
                yield return screen.TopLeft + new IntVector2( -Tools.CellSizeInCoord * 3 / 2, y);
                yield return screen.TopLeft + new IntVector2(screen.WidthCoord + Tools.CellSizeInCoord * 3 / 2, y);
            }
        }

        public void TryRepopMob()
        {
            int random = Tools.Rand.Next(4);

            List<LongVector2> availablePositions = new List<LongVector2>();
            foreach(LongVector2 p in this.getPopablePositions())
            {
                LongVector2 roundPosition = p / Tools.CellSizeInCoord * Tools.CellSizeInCoord;
                if (!this.Ground.HasBlock(roundPosition.X, roundPosition.Y))
                {
                    availablePositions.Add(roundPosition);
                }
            }
            if (availablePositions.Count == 0)
            {
                return;
            }
            LongVector2 mobPopPositionCoord = availablePositions[Tools.Rand.Next(availablePositions.Count)];
            
            Item groundItem = this.Ground.GetCarpet(mobPopPositionCoord);
            if(groundItem == null)
            {
              return;
            }
			MobType mobType;
            if (groundItem.ItemType == ItemType.GROUND_BAT)
            {
                mobType = MobType.Bat;
            }
            else
            {
				mobType = Tools.RandPickOne(MobType.Rat, MobType.Bat);
            }
			this.Add(new Mob(this, mobPopPositionCoord, mobType, isVagabond: true));
        }



        //private Dictionary<LongVector2, Tuple<int, int>> pickHits = new Dictionary<LongVector2, Tuple<int, int>>();

        public void PickHitFurniture(Item furniture, LongVector2 pickPosition, Item tool)
        {

			LongVector2 cellPosition = Tools.FloorToCell(pickPosition);

			if (this.Ground.IsLocked(cellPosition))
			{
				this.Add(new SpeechBalloon(this, cellPosition, Item.GetTextureRect(ItemType.EMOTICON_FORBID)));
				return;
			}


            ///We hit at 10 +/-3
            int nbHitToBreak = -1;
			int pickLevel = tool != null && tool.Pick != null ? tool.Pick.level : 0;
            if (furniture.ItemType < ItemType.DELIMITER_BLOCK_DECORATION_END)
            {
                nbHitToBreak = 100;
            }
            else if (furniture.ItemType == ItemType.TREE)
            {
                nbHitToBreak = 50;
            }
            else if (furniture.ItemType == ItemType.BLOCK_EARTH)
            {
                nbHitToBreak = 30;
            }			
            else if (furniture.ItemType < ItemType.DELIMITER_BLOCK_PEEK1)
            {
				if (pickLevel >= 1)
				{
					nbHitToBreak = 60;
				}
				else
				{
					///can not break
				}
            }
			else if (furniture.ItemType < ItemType.DELIMITER_BLOCK_PEEK2)
			{
				if (pickLevel >= 2)
				{
					nbHitToBreak = 70;
				}
				else
				{
					///can not break
				}
			}
			else if (furniture.ItemType < ItemType.DELIMITER_BLOCK_PEEK3)
			{
				if (pickLevel >= 3)
				{
					nbHitToBreak = 90;
				}
				else
				{
					///can not break
				}
			}
            else
            {
                if (tool == null || tool.ItemType == ItemType.TOOL_PICK1)
                {
                    ///can not break
                }
            }
            if (nbHitToBreak < 0)
            {
                this.Add(new SpeechBalloon(this, cellPosition, Item.GetTextureRect(ItemType.EMOTICON_FORBID)));
                return;///can not break
            }
            int totalDamage = this.Ground.HitFurniture(this.Tick, cellPosition, Tools.Rand.Next(7, 14));
            if (totalDamage >= nbHitToBreak)
            {
                this.Ground.RemoveFurniture(cellPosition);
				furniture.GenerateDropWhenPlayerPicked(this, cellPosition);                
            }            
        }

        /// <summary>
        /// Look for something to refresh on the map.
        /// </summary>
		private void lookForUpdateItems()
		{
			Character character = Tools.RandPickOne(this.GetFriendlyUnits().ToArray());
			if (character == null)
			{
				return;
			}
			int border = 50 * Tools.CellSizeInCoord;///The larger the border, the slower the items refresh
			LongVector2 cell = Tools.FloorToCell(character.Position + new IntVector2(
				Tools.Rand.Next(-border, border),
				Tools.Rand.Next(-border, border)));

			Item furniture = this.Ground.GetFurniture(cell);
			if (furniture != null)
			{
				furniture.UpdateItemOnGround(this, cell);
			}
			Item carpet = this.Ground.GetCarpet(cell);
			if (carpet != null)
			{
				carpet.UpdateItemOnGround(this, cell);
			}						
		}

		public bool TrySetItemOnGround(Item item, LongVector2 characterPosition)
		{
			if (this.Ground.IsLocked(characterPosition))
			{
				return false;
			}
			var itemPosition = Tools.CenterCharacterCell(characterPosition);
			bool success = false;
			Item carpet = this.Ground.GetCarpet(itemPosition);				
			if (item.ItemType >= ItemType.DELIMITER_GROUND_BLOCKING && item.ItemType < ItemType.DELIMITER_TOOL)
			{
				if (carpet.ItemType != ItemType.GROUND_WATER
					&& carpet.ItemType != ItemType.GROUND_WATER_BRIDGE
					&& carpet.ItemType != ItemType.DOOR)
				{
					Item furniture = this.Ground.GetFurniture(itemPosition);
					if (furniture == null)
					{
						this.Ground.SetFurniture(item, itemPosition);
						success = true;
					}
				}
			}
			else if (item.ItemType >= ItemType.DELIMITER_GROUND && item.ItemType < ItemType.DELIMITER_GROUND_END)
			{
				if (item.ItemType == ItemType.GROUND_WATER_BRIDGE)
				{
					if (carpet.ItemType == ItemType.GROUND_WATER)
					{
						this.Ground.SetCarpet(item, itemPosition);
						success = true;
					}
				}
				else if(carpet.ItemType == ItemType.GROUND_EARTH)
				{
					this.Ground.SetCarpet(item, itemPosition);
					success = true;
				} 
			}
			
			if (success)
			{
				if (item.IsCleverBlock())
				{
					this.AddCleverBlock(item, itemPosition);
				}							
			}
			return success;
		}


		public void Save()
		{
			this.saveWorldFile();
			this.Ground.SaveAll();
		}

		private static XmlSerializer serializer = new XmlSerializer(typeof(ComWorld));


		public const string SaveWorldFileName = "World.xml";
		private void saveWorldFile()
		{
			string filename = Path.Combine(Tools.SaveDirectory, this.WorldCode, World.SaveWorldFileName);
			if (File.Exists(filename))
			{
				File.Delete(filename);
			}
			ComWorld com = this.ToCom();
			using (var stream = File.OpenWrite(filename))
			{
				World.serializer.Serialize(stream, com);
			}
			Trace.WriteLine("Saved " + filename);
		}


		private ComWorld ToCom()
		{
			var com = new ComWorld();
			List<ComItemSet> allComItemSet = new List<ComItemSet>();
			com.NpcList = this.npcList.Select(n => n.ToCom(allComItemSet)).ToArray();
			com.PlayerBagId = this.Bag.ToCom(allComItemSet, Guid.NewGuid());
			com.Player1ActionBarId = this.player1.ActionItems.ToCom(allComItemSet, Guid.NewGuid());
			com.Player2ActionBarId = this.player2.ActionItems.ToCom(allComItemSet, Guid.NewGuid());
			com.Player1GearBagId = this.player1.Hero.GearSlots.ToCom(allComItemSet, Guid.NewGuid());
			com.Player2GearBagId = this.player2.Hero.GearSlots.ToCom(allComItemSet, Guid.NewGuid());
			com.ItemSetList = allComItemSet.ToArray();
			com.PlayerHomePosition = this.PlayerHomePosition;
			com.PlayerResurectPosition = this.PlayerResurectPosition;
			return com;
		}

        public void Load()
        {
			string filename = Path.Combine(Tools.SaveDirectory, this.WorldCode, World.SaveWorldFileName);
            if (!File.Exists(filename))
            {
				throw new Exception("File not found: " + filename);
            }

            ComWorld com;
            using (var stream = File.OpenRead(filename))
            {
                com = (ComWorld)World.serializer.Deserialize(stream);
            }
			//this.PlayerHomePosition = com.
            this.npcList.AddRange(com.NpcList.Select(c => Npc.FromCom(this, c, com.ItemSetList)));
            this.Bag.FillFromCom(com.ItemSetList, com.PlayerBagId);
			this.player1.ActionItems.FillFromCom(com.ItemSetList, com.Player1ActionBarId);
			this.player2.ActionItems.FillFromCom(com.ItemSetList, com.Player2ActionBarId);
			this.player1.Hero.GearSlots.FillFromCom(com.ItemSetList, com.Player1GearBagId);
			this.player2.Hero.GearSlots.FillFromCom(com.ItemSetList, com.Player2GearBagId);
			this.PlayerHomePosition = com.PlayerHomePosition;
			this.PlayerResurectPosition = com.PlayerResurectPosition;
			this.player1.Position = this.PlayerHomePosition;
			this.player2.Position = this.PlayerHomePosition;
			this.screen.CenterOn(this.PlayerHomePosition);
        }

		/// <summary>
		/// Return the quantity effectively moved.
		/// </summary>
		public int MainBagAddByPlayerLoot(Item item, Player player, int quantityToMove)
		{
			for (int i = 0; i < quantityToMove; i++)///Stupide method
			{
				if (!MainBagTryAddByPlayerLoot(item, player))
				{
					return i;
				}
			}
			return quantityToMove;
		}
		public bool MainBagTryAddByPlayerLoot(Item item, Player player)
		{
			if (player.ActionItems.TryAdd(item))
			{
				return true;
			}
			return this.Bag.MainBagTryAddByPlayerLoot(item, player.IsPlayer1);
		}
	}
}
