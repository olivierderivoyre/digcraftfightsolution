﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
    public class InterfaceLayer
    {
        private class BagCursor
        {
            public ItemSet Bag;			
            public IntVector2 Position;
			public int MenuIndex = -1;
        }


        private World world;
        public bool IsShowingBag = false;
        private BagCursor player1Cursor = new BagCursor();
        private BagCursor player2Cursor = new BagCursor();
        private BagCursor player1CursorSelection;
        private BagCursor player2CursorSelection;
        private TimeSpan lastActionTick = TimeSpan.Zero;
		private WorkStation workStation;
		private Item workStationItem;
		private Dictionary<Item, int> bagContentCacheForWorkStation = new Dictionary<Item, int>();
		private BagCursor workStationCursor;
        private Npc npc;
        private ItemSet npcAsBag;


        public InterfaceLayer(World world)
        {
            this.world = world;
        }
        
		public void Draw(SpriteBatch spriteBatch, IntVector2 screenSize, Ressource ressource)
        {
            if (!this.IsShowingBag)
            {
				drawInGameUserInterface(spriteBatch, screenSize, ressource);
            }
            else if (this.workStation != null)
            {
               drawWorkStationScreen(spriteBatch, screenSize, ressource);                
            } 
            else
            {				
                showBag_drawScreen(spriteBatch, ressource);
            }
        }



		public void Update(GameTime gameTime, InputState inputState, InputState previousInputState)
		{
			if ((world.player1.InputController.HasPausePressed(inputState) && !world.player1.InputController.HasPausePressed(previousInputState))
				|| (world.player2.InputController.HasPausePressed(inputState) && !world.player2.InputController.HasPausePressed(previousInputState)))
			{
				this.IsShowingBag = !this.IsShowingBag;
				if (this.IsShowingBag)
				{
					this.player1Cursor = new BagCursor { Bag = world.Bag, Position = new IntVector2(0, 0) };
					this.player2Cursor = new BagCursor { Bag = world.Bag, Position = new IntVector2(world.Bag.Width - 1, 0) };
					this.workStationItem = this.world.GetSpeakingWorkStation();
					if (this.workStationItem != null)
					{
						this.workStation = this.workStationItem.WorkStation;
						this.npc = null;
						this.workStationCursor = new BagCursor() { Position = new IntVector2(0, 0) };
						this.bagContentCacheForWorkStation.Clear();
					}
					else
					{
						this.workStation = null;
						this.npc = this.world.GetSpeakingNpc();
					}
					if (this.npc != null)
					{
						this.npcAsBag = new ItemSet(1, 1);
						this.npcAsBag.TryAdd(new Item(this.npc.Hero));
					}
					else
					{
						this.npcAsBag = null;
					}
				}
				else///Close menu
				{
					showbag_close();
				}
			}
			if (this.IsShowingBag)
			{
				if (this.workStation != null)
				{
					moveCursorInWorkingStation(world.player1, inputState, previousInputState);
					moveCursorInWorkingStation(world.player2, inputState, previousInputState);
					clickInWorkingStation(world.player1, inputState, previousInputState);
					clickInWorkingStation(world.player2, inputState, previousInputState);
				}
				else
				{
					showBag_moveCursor(this.player1Cursor, world.player1, inputState, previousInputState, gameTime);
					showBag_moveCursor(this.player2Cursor, world.player2, inputState, previousInputState, gameTime);
					showBag_click(world.player1, this.player1Cursor, ref this.player1CursorSelection, inputState, previousInputState);
					showBag_click(world.player2, this.player2Cursor, ref this.player2CursorSelection, inputState, previousInputState);
				}
			}
		}

		


		#region Tools

		private void drawMenu(SpriteBatch spriteBatch, Ressource ressource, int menuIndex, string text, IntVector2 topLeft)
		{
			Color? focusColor = null;
			if (this.player1Cursor.MenuIndex == menuIndex)
			{
				focusColor = Color.Green;
			}
			if (this.player2Cursor.MenuIndex == menuIndex)
			{
				focusColor = Color.Blue;
			}
			DrawMenu(spriteBatch, ressource, text, topLeft, focusColor);
		}

		public static void DrawMenu(SpriteBatch spriteBatch, Ressource ressource, string text, IntVector2 topLeft, Color? focusColor)
		{
			spriteBatch.DrawString(ressource.Font, text, new Vector2(topLeft.X, topLeft.Y), Color.White);
			if (focusColor != null)
			{
				int textPx = text.Length * 9;
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X - 15, topLeft.Y - 2, 15, 40), new Rectangle(123, 0, 15, 40), focusColor.Value);
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y - 2, textPx, 40), new Rectangle(123 + 15, 0, 10, 40), focusColor.Value);
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X + textPx, topLeft.Y - 2, 15, 40), new Rectangle(123 + 25, 0, 15, 40), focusColor.Value);
			}			
		}

		private void drawItemTooltip(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, BagCursor bagCursor, Player playerCursor)
		{
			Item item = null;
			if (bagCursor.Bag != null)
			{


				ItemStack itemStack = bagCursor.Bag.GetBagItem(bagCursor.Position);
				if (!itemStack.IsEmpty())
				{
					item = itemStack.Item;
				}
			}
			drawItemTooltip(spriteBatch, ressource, topLeft, item, playerCursor);
		}
		
		private void drawItemTooltip(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, Item item, Player playerCursor)
		{
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, 220, 72), new Rectangle(41 * 2, 41 * 2, 40, 40), Color.Gray);
			if (item == null)
			{
				return;
			}
			TextureRect textureRect = item.GetTextureRect();
			spriteBatch.Draw(textureRect.GetTileset(ressource), new Rectangle(topLeft.X + 4, topLeft.Y + 4, 64, 64), textureRect.Rectangle, Color.White);
			///Hide the pixel line that appears when resiziing the screen
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X + 2, topLeft.Y + 2, 64 + 4, 64 + 4), new Rectangle(0, 123, 64 + 4, 64 + 4), Color.DarkGray);
			string itemHeader = "";
			string itemSubText = "";
			int improment = 0;
			Player p1 = world.player1;
			Player p2 = world.player2;
			if (playerCursor == world.player1)
			{
				p2 = null;
			}
			if (playerCursor == world.player2)
			{
				p1 = null;
			}
			item.GetTooltip(ref itemHeader, ref itemSubText, ref improment, p1, p2);
			Color headerColor = Color.White;
			if (improment > 0)
			{
				headerColor = Color.Yellow;
			}
			if (improment < 0)
			{
				headerColor = Color.DarkGray;
			}
			spriteBatch.DrawString(ressource.Font, itemHeader, new Vector2(topLeft.X + 4 + 64 + 4, topLeft.Y + 4), headerColor);
			if (!string.IsNullOrWhiteSpace(itemSubText))
			{
				spriteBatch.DrawString(ressource.Font, itemSubText, new Vector2(topLeft.X + 4 + 64 + 4, topLeft.Y + 4 + 24), Color.White);
			}
		}


		private Tuple<IntVector2, TimeSpan>[] lastPressedStateArray = new Tuple<IntVector2, TimeSpan>[2];
		
		private IntVector2 getMoveDirection(Player player, InputState inputState, GameTime gameTime)
		{
			int playerIndex = world.player1 == player ? 0 : 1;

			IntVector2 currentMove = player.InputController.GetMoveDirection(inputState);
			if (currentMove == IntVector2.Zero)
			{
				this.lastPressedStateArray[playerIndex] = null;
				return currentMove;
			}
			var lastPressedState = this.lastPressedStateArray[playerIndex];
			if (lastPressedState == null)
			{
				this.lastPressedStateArray[playerIndex] = new Tuple<IntVector2, TimeSpan>(currentMove, gameTime.TotalGameTime);
				return currentMove;
			}
			if (lastPressedState.Item1 != currentMove)
			{
				if (gameTime.TotalGameTime.Subtract(lastPressedState.Item2).TotalMilliseconds > 150)
				{
					this.lastPressedStateArray[playerIndex] = new Tuple<IntVector2, TimeSpan>(currentMove, gameTime.TotalGameTime);
					return currentMove;
				}
				else
				{
					return IntVector2.Zero;
				}
			}
			if (gameTime.TotalGameTime.Subtract(lastPressedState.Item2).TotalMilliseconds < 200)
			{
				return IntVector2.Zero;
			}
			this.lastPressedStateArray[playerIndex] = new Tuple<IntVector2, TimeSpan>(currentMove, gameTime.TotalGameTime.Subtract(TimeSpan.FromMilliseconds(50)));
			return currentMove;
		}

		#endregion Tools

		#region Toolbar while playing


		private IntVector2 drawInGameUserInterface(SpriteBatch spriteBatch, IntVector2 screenSize, Ressource ressource)
		{
			if (!world.player1.NeedToPressStartToPlay)
			{
				drawActionBar(spriteBatch, ressource, world.player1, 10);
				drawLifeBars(spriteBatch, ressource, world.player1, 10 + 130);
				drawHeroIcon(spriteBatch, ressource, world.player1, 10 + 130 + 130);
			}
			else
			{
				drawPressStart(spriteBatch, ressource, 10);
			}
			if (!world.player2.NeedToPressStartToPlay)
			{
				drawHeroIcon(spriteBatch, ressource, world.player2, screenSize.X - 310);
				drawLifeBars(spriteBatch, ressource, world.player2, screenSize.X - 310 + 50);
				drawActionBar(spriteBatch, ressource, world.player2, screenSize.X - 310 + 50 + 130);
			}
			else
			{
				drawPressStart(spriteBatch, ressource, screenSize.X - 210);
			}
			if (this.world.Boss != null)
			{
				drawBossLifeBars(spriteBatch, ressource, screenSize);
			}
			return screenSize;
		}

		private void drawHeroIcon(SpriteBatch spriteBatch, Ressource ressource, Player player, int left)
		{

			Color color = player.CharacterAttributes.GetLifeRedAlertIndicator(this.world);
			spriteBatch.Draw(ressource.TilesetInterface40,
						 new Rectangle(left, 10 + 40, 40, 40),
						 new Rectangle(0, 0, 40, 40), color);
			spriteBatch.Draw(player.Hero.GetSprite(ressource),
				new Rectangle(left + 4, 10 + 40 + 4, 32, 32),
				new Rectangle(0, 0, 64, 64), color);
			
		}



		private void drawLifeBars(SpriteBatch spriteBatch, Ressource ressource, Player player, int left)
		{
			///Life and mana
			CharacterAttributes attr = player.CharacterAttributes;
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(left, 10 + 40, 120, 40), new Rectangle(0, 41, 120, 40), Color.White);
			int maxLifePx = 112;
			if (!attr.IsDead)
			{
				int lifeLogPx = maxLifePx * attr.LifeBasisPointDisplay / 10000;
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(left + 4, 10 + 40, lifeLogPx, 40), new Rectangle(123, 40, 40, 40), Color.White);
			}
			if (attr.CurrentMana > 0)
			{
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(left + 4, 10 + 40, attr.CurrentMana * maxLifePx / attr.MaxMana, 40), new Rectangle(164, 40, 40, 40), Color.White);
			}


		}

		private void drawActionBar(SpriteBatch spriteBatch, Ressource ressource, Player player, int left)
		{
			//drawItemSet(spriteBatch, ressource, new IntVector2(left, 10), player.ActionItems);
			drawItemStack(spriteBatch, ressource, new IntVector2(left, 10 + 40), player.ActionItems.GetBagItem(2), Color.Blue);
			drawItemStack(spriteBatch, ressource, new IntVector2(left + 40, 10), player.ActionItems.GetBagItem(3), Color.Yellow);
			drawItemStack(spriteBatch, ressource, new IntVector2(left + 40, 10 + 40 + 40), player.ActionItems.GetBagItem(0), Color.Green);
			drawItemStack(spriteBatch, ressource, new IntVector2(left + 80, 10 + 40), player.ActionItems.GetBagItem(1), Color.Red);
		}

		private void drawBossLifeBars(SpriteBatch spriteBatch, Ressource ressource, IntVector2 screenSize)
		{
			spriteBatch.Draw(ressource.TilesetInterface40,
				new Rectangle(40, screenSize.Y - 40, (screenSize.X - 80) * world.Boss.CharacterAttributes.LifeBasisPoint / 10000, 40),
				new Rectangle(134, 40, 20, 40),
				world.Boss.Immortal ? Color.Yellow : Color.White);
		}

		private void drawPressStart(SpriteBatch spriteBatch, Ressource ressource, int left)
		{
			spriteBatch.DrawString(ressource.Font, "Press any key to start", new Vector2(left, 10), Color.White);			
		}

		#endregion Toolbar while playing

		#region Bag
		private static ItemSet emptyItemSet1x1 = new ItemSet(1, 1);
		private void showBag_drawScreen(SpriteBatch spriteBatch, Ressource ressource)
		{
			int bagRightPx = 110 + world.Bag.Width * 40;
			int gearTopPx = 40;
			this.drawItemSet(spriteBatch, ressource, new IntVector2(60, gearTopPx), world.player1.ActionItems, new[] { Color.Green, Color.Red, Color.Blue, Color.Yellow });
			spriteBatch.Draw(ressource.TilesetItems32, new Rectangle(24, gearTopPx + 4, 32, 32), new Rectangle(32 * 7, 32 * 3, 32, 32), Color.White);
			spriteBatch.Draw(ressource.TilesetItems32, new Rectangle(24, gearTopPx + 44, 32, 32), new Rectangle(32 * 2, 32 * 2, 32, 32), Color.White);
			spriteBatch.Draw(ressource.TilesetItems32, new Rectangle(24, gearTopPx + 84, 32, 32), new Rectangle(32 * 1, 32 * 4, 32, 32), Color.White);
			this.drawItemSet(spriteBatch, ressource, new IntVector2(20, gearTopPx), world.player1.Hero.GearSlots);

			this.drawItemSet(spriteBatch, ressource, new IntVector2(40, gearTopPx + 4 * 40 + 10), world.player1.HeroAsBag);

			this.drawItemSet(spriteBatch, ressource, new IntVector2(bagRightPx + 20, gearTopPx), world.player2.ActionItems, new[] { Color.Green, Color.Red, Color.Blue, Color.Yellow });
			spriteBatch.Draw(ressource.TilesetItems32, new Rectangle(bagRightPx + 64, gearTopPx + 4, 32, 32), new Rectangle(32 * 7, 32 * 3, 32, 32), Color.White);
			spriteBatch.Draw(ressource.TilesetItems32, new Rectangle(bagRightPx + 64, gearTopPx + 44, 32, 32), new Rectangle(32 * 2, 32 * 2, 32, 32), Color.White);
			spriteBatch.Draw(ressource.TilesetItems32, new Rectangle(bagRightPx + 64, gearTopPx + 84, 32, 32), new Rectangle(32 * 1, 32 * 4, 32, 32), Color.White);
			this.drawItemSet(spriteBatch, ressource, new IntVector2(bagRightPx + 60, gearTopPx), world.player2.Hero.GearSlots);
			this.drawItemSet(spriteBatch, ressource, new IntVector2(bagRightPx + 40, gearTopPx + 4 * 40 + 10), world.player2.HeroAsBag);
			///Bag
			this.drawItemSet(spriteBatch, ressource, new IntVector2(120, 40), world.Bag);
			///Bin trash
			int bagDownPx = 40 + 7 * 40;
			this.drawItemSet(spriteBatch, ressource, new IntVector2(120, bagDownPx + 10), world.BinBag, new[] { Color.DarkOliveGreen });
			///hide a little the deleted item by drawing over in semi-transparent.
			this.drawItemSet(spriteBatch, ressource, new IntVector2(120, bagDownPx + 10), emptyItemSet1x1, new[] { Color.DarkOliveGreen });
			if (world.BinBag.GetBagItem(0).IsEmpty())
			{
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(120, bagDownPx + 10, 40, 40), new Rectangle(41 * 6, 41 * 0, 40, 40), Color.Gray);
			}			
			///Tooltip
			this.drawItemTooltip(spriteBatch, ressource, new IntVector2(170, bagDownPx + 10), this.player1Cursor, world.player1);
			this.drawItemTooltip(spriteBatch, ressource, new IntVector2(400, bagDownPx + 10), this.player2Cursor, world.player2);
			///Menus
			drawMenu(spriteBatch, ressource, 0, "Back to save", new IntVector2(640, bagDownPx + 10));
			drawMenu(spriteBatch, ressource, 1, "Back to home", new IntVector2(640, bagDownPx + 10 + 24));
			drawMenu(spriteBatch, ressource, 2, "Stop playing", new IntVector2(640, bagDownPx + 10 + 48));
			drawMenu(spriteBatch, ressource, 3, "Exit to windows", new IntVector2(640, bagDownPx + 10 + 72));
			///Temp
			if (this.npcAsBag != null && this.npc != null)
			{
				this.drawItemSet(spriteBatch, ressource, new IntVector2(40, 100 + world.Bag.Height * 40 + 20), this.npcAsBag);
				this.drawItemSet(spriteBatch, ressource, new IntVector2(100, 100 + world.Bag.Height * 40 + 20), this.npc.Hero.Bag);
			}
		}

		private void drawItemSet(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, ItemSet itemSet, Color[] colorList = null)
		{
			for (int j = 0; j < itemSet.Height; j++)
			{
				for (int i = 0; i < itemSet.Width; i++)
				{
					IntVector2 topLeftCell = new IntVector2(topLeft.X + i * 40, topLeft.Y + j * 40);
					ItemStack bagItem = itemSet.GetBagItem(i, j);
					Color? c = null;
					if (colorList != null && i + j < colorList.Length)
					{
						c = colorList[i + j];
					}
					drawItemStack(spriteBatch, ressource, topLeftCell, bagItem, c);
				}
			}
			if (this.IsShowingBag)///Hide cursor on ActionBar when playing
			{
				if (this.player1Cursor.Bag == itemSet)
				{
					drawCursor(spriteBatch, ressource, topLeft, this.player1Cursor, Color.Green);
				}
				if (this.player2Cursor.Bag == itemSet)
				{
					drawCursor(spriteBatch, ressource, topLeft, this.player2Cursor, Color.Blue);
				}
				if (this.player1CursorSelection != null && this.player1CursorSelection.Bag == itemSet)
				{
					drawCursorSelection(spriteBatch, ressource, topLeft, this.player1CursorSelection, Color.Green);
				}
				if (this.player2CursorSelection != null && this.player2CursorSelection.Bag == itemSet)
				{
					drawCursorSelection(spriteBatch, ressource, topLeft, this.player2CursorSelection, Color.Blue);
				}
			}
		}
		private static void drawCursor(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, BagCursor cursor, Color color)
		{
			spriteBatch.Draw(ressource.TilesetInterface40,
				new Rectangle(topLeft.X + cursor.Position.X * 40, topLeft.Y + cursor.Position.Y * 40, 40, 40),
				new Rectangle(41, 0, 40, 40), color);
		}
		private static void drawCursorSelection(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, BagCursor cursor, Color color)
		{
			spriteBatch.Draw(ressource.TilesetInterface40,
				new Rectangle(topLeft.X + cursor.Position.X * 40, topLeft.Y + cursor.Position.Y * 40, 40, 40),
				new Rectangle(82, 0, 40, 40), color);
		}
		private static void drawItemStack(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, ItemStack bagItem, Color? backColor)
		{
			if (backColor == null)
			{
				spriteBatch.Draw(ressource.TilesetInterface40,
					new Rectangle(topLeft.X, topLeft.Y, 40, 40),
					new Rectangle(0, 0, 40, 40), Color.White);
			}
			else
			{
				spriteBatch.Draw(ressource.TilesetInterface40,
					new Rectangle(topLeft.X, topLeft.Y, 40, 40),
					new Rectangle(0, 82, 40, 40), backColor.Value);
			}
			if (bagItem != null)
			{
				//spriteBatch.Draw(ressource.TilesetInterface40,
				//    new Rectangle(topLeft.X + 4, topLeft.Y + 4, 32, 32),
				//   new Rectangle(120,0, 40,40), Color.White);
				TextureRect textureRect = bagItem.Item.GetTextureRect();
				spriteBatch.Draw(textureRect.GetTileset(ressource),
					 new Rectangle(topLeft.X + 4, topLeft.Y + 4, 32, 32),
				   textureRect.Rectangle, Color.White);
				if (bagItem.Quantity != 1)
				{
					drawItemQuantity(spriteBatch, ressource, topLeft, bagItem.Quantity);
				}
			}
		}
		
		private static void drawItemQuantity(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, int quantity)
		{
			spriteBatch.DrawString(ressource.Font, quantity.ToString().PadLeft(3, ' '),
				new Vector2(topLeft.X + 4, topLeft.Y + 18), Color.White);
		}

		private void showBag_moveCursor(BagCursor current, Player player, InputState inputState, InputState previousInputState, GameTime gameTime)
		{

			IntVector2 move = getMoveDirection(player, inputState, gameTime);
			if (move == IntVector2.Zero)
			{
				return;
			}
			if (current.Bag == null)
			{
				current.MenuIndex += move.Y;
				if (current.MenuIndex < 0)
				{
					current.Bag = this.world.Bag;
					current.Position.X = this.world.Bag.Width - 1;
					current.Position.Y = this.world.Bag.Height - 1;
				}
				if (current.MenuIndex >= 3)
				{
					current.MenuIndex = 3;
				}
				return;
			}
			IntVector2 newPosition = current.Position + move;

			#region switch bags
			if (player == this.world.player1)
			{
				if (newPosition.X < 0)
				{
					if (current.Bag == this.world.Bag)
					{
						current.Bag = this.world.player1.ActionItems;
					}
					else if (current.Bag == this.world.player1.ActionItems)
					{
						current.Bag = this.world.player1.Hero.GearSlots;
						this.autoplaceCursorInGearBag(current, this.player1CursorSelection, ref newPosition);
					}
					else if (current.Bag == this.world.player1.Hero.GearSlots)
					{
						current.Bag = this.world.Bag;
						newPosition.X = this.world.Bag.Width - 1;
					}
					else if (current.Bag == this.world.player1.HeroAsBag)
					{
						current.Bag = this.world.Bag;
						newPosition.X = this.world.Bag.Width - 1;
						newPosition.Y = 4;
					}
					else if (this.npc != null && current.Bag == this.npc.Hero.Bag)
					{
						current.Bag = this.npcAsBag;
					}
				}
				else if (newPosition.X >= current.Bag.Width)
				{
					if (current.Bag == this.world.player1.Hero.GearSlots)
					{
						current.Bag = this.world.player1.ActionItems;
						newPosition.X = 0;
					}
					else if (current.Bag == this.world.player1.ActionItems)
					{
						current.Bag = this.world.Bag;
						newPosition.X = 0;
					}
					else if (current.Bag == this.world.player1.HeroAsBag)
					{
						current.Bag = this.world.Bag;
						newPosition.X = 0;
						newPosition.Y = 4;
					}
					else if (current.Bag == this.world.Bag)
					{
						current.Bag = this.world.player1.Hero.GearSlots;
						this.autoplaceCursorInGearBag(current, this.player1CursorSelection, ref newPosition);
					}
					else if (this.npc != null && current.Bag == this.npcAsBag)
					{
						current.Bag = this.npc.Hero.Bag;
						newPosition.X = 0;
					}
				}
				if (newPosition.Y < 0)
				{
					if (current.Bag == this.world.player1.HeroAsBag)
					{
						current.Bag = this.world.player1.ActionItems;
						newPosition.Y = this.world.player1.ActionItems.Height - 1;
					}
					else if (current.Bag == this.world.BinBag)
					{
						current.Bag = this.world.Bag;
						newPosition.Y = this.world.Bag.Height - 1;
					}
					else if (this.npc != null && current.Bag == this.npc.Hero.Bag)
					{
						current.Bag = this.world.Bag;
						newPosition.Y = this.world.Bag.Height - 1;
					}
				}
				else if (newPosition.Y >= current.Bag.Height)
				{
					if (current.Bag == this.world.player1.ActionItems || current.Bag == this.world.player1.Hero.GearSlots)
					{
						current.Bag = this.world.player1.HeroAsBag;
						newPosition.X = 0;
						newPosition.Y = 0;
					}
					else if (current.Bag == this.world.Bag)
					{
						if (newPosition.X < 5)
						{
							current.Bag = this.world.BinBag;
						}
						else if (newPosition.X > 10)
						{
							current.Bag = null;
							current.MenuIndex = 0;
							return;
						}
					}
					else if (current.Bag == this.world.Bag)
					{
						if (this.npc != null)
						{
							current.Bag = this.npc.Hero.Bag;
							newPosition.Y = 0;
						}
					}
				}
			}
			if (player == this.world.player2)
			{
				if (newPosition.X < 0)
				{
					if (current.Bag == this.world.player2.Hero.GearSlots)
					{
						current.Bag = this.world.player2.ActionItems;
						newPosition.X = 0;
					}
					else if (current.Bag == this.world.player2.ActionItems)
					{
						current.Bag = this.world.Bag;
						newPosition.X = this.world.Bag.Width - 1;
					}
					else if (current.Bag == this.world.player2.HeroAsBag)
					{
						current.Bag = this.world.Bag;
						newPosition.X = this.world.Bag.Width - 1;
						newPosition.Y = 4;
					}
					else if (current.Bag == this.world.Bag)
					{
						current.Bag = this.world.player2.Hero.GearSlots;
						this.autoplaceCursorInGearBag(current, this.player2CursorSelection, ref newPosition);
					}
					else if (this.npc != null && current.Bag == this.npc.Hero.Bag)
					{
						current.Bag = this.npcAsBag;
					}
				}
				else if (newPosition.X >= current.Bag.Width)
				{
					if (current.Bag == this.world.Bag)
					{
						current.Bag = this.world.player2.ActionItems;
					}
					else if (current.Bag == this.world.player2.ActionItems)
					{
						current.Bag = this.world.player2.Hero.GearSlots;
						this.autoplaceCursorInGearBag(current, this.player2CursorSelection, ref newPosition);
					}
					else if (current.Bag == this.world.player2.Hero.GearSlots)
					{
						current.Bag = this.world.Bag;
						newPosition.X = 0;
					}
					else if (current.Bag == this.world.player2.HeroAsBag)
					{
						current.Bag = this.world.Bag;
						newPosition.X = 0;
						newPosition.Y = 4;
					}
					else if (this.npc != null && current.Bag == this.npcAsBag)
					{
						current.Bag = this.npc.Hero.Bag;
						newPosition.X = 0;
					}
				}
				if (newPosition.Y < 0)
				{
					if (current.Bag == this.world.player2.HeroAsBag)
					{
						current.Bag = this.world.player2.ActionItems;
						newPosition.Y = this.world.player2.ActionItems.Height - 1;
					}
					else if (current.Bag == this.world.BinBag)
					{
						current.Bag = this.world.Bag;
						newPosition.Y = this.world.Bag.Height - 1;
					}
					else if (this.npc != null && current.Bag == this.npc.Hero.Bag)
					{
						current.Bag = this.world.Bag;
						newPosition.Y = this.world.Bag.Height - 1;
					}
				}
				else if (newPosition.Y >= current.Bag.Height)
				{
					if (current.Bag == this.world.player2.ActionItems || current.Bag == this.world.player2.Hero.GearSlots)
					{
						current.Bag = this.world.player2.HeroAsBag;
					}
					else if (current.Bag == this.world.Bag)
					{
						if (newPosition.X < 5)
						{
							current.Bag = this.world.BinBag;
						}
						else if (newPosition.X > 10)
						{
							current.Bag = null;
							current.MenuIndex = 0;
							return;
						}
					}
					else if (current.Bag == this.world.Bag)
					{
						if (this.npc != null)
						{
							current.Bag = this.npc.Hero.Bag;
							newPosition.Y = 0;
						}
					}
				}
			}
			#endregion switch bags

			newPosition.X = Math.Max(0, Math.Min(current.Bag.Width - 1, newPosition.X));
			newPosition.Y = Math.Max(0, Math.Min(current.Bag.Height - 1, newPosition.Y));
			current.Position = newPosition;
		}

		private void autoplaceCursorInGearBag(BagCursor current, BagCursor selection, ref IntVector2 newPosition)
		{
			if (selection == null || selection.Bag == null)
			{
				return;
			}
			ItemStack selectedItemStak = selection.Bag.GetBagItem(selection.Position);
			if (selectedItemStak.IsEmpty() || selectedItemStak.Item.Gear == null)
			{
				return;
			}
			newPosition.Y = selectedItemStak.Item.Gear.GetPositionInPlayerGearBag();
		}

		private void showBag_click(Player player, BagCursor current, ref BagCursor selection, InputState inputState, InputState previousInputState)
		{
			if (player.InputController.HasAnyActionButtonPressed(inputState) && !player.InputController.HasAnyActionButtonPressed(previousInputState))
			{
				if (current.Bag == null)
				{
					showBag_clickInMenu(player, current.MenuIndex);
					return;
				}
				if (selection == null)
				{
					selection = new BagCursor { Bag = current.Bag, Position = current.Position };
				}
				else if (selection.Bag == current.Bag && selection.Position == current.Position)
				{
					selection = null;
				}
				else
				{
					///Forbid to fill the npcAsBag
					if (this.npcAsBag != null)
					{
						if (selection.Bag == this.npcAsBag)
						{
							if (!this.npc.Hero.Bag.IsEmpty())
							{
								selection = null;
								return;
							}
							if (current.Bag == this.npc.Hero.Bag)
							{
								selection = null;
								return;
							}
							if (current.Bag.GetBagItem(current.Position) != null)
							{
								return;
							}
						}
						if (current.Bag == this.npcAsBag)
						{
							if (!this.npc.Hero.Bag.IsEmpty())
							{
								selection = null;
								return;
							}
							if (selection.Bag == this.npc.Hero.Bag)
							{
								selection = null;
								return;
							}
							if (selection.Bag.GetBagItem(selection.Position) != null)
							{
								return;
							}
						}
						if (selection.Bag == this.npc.Hero.Bag || current.Bag == this.npc.Hero.Bag)
						{
							if (this.npcAsBag.GetBagItem(0) == null)
							{
								selection = null;
								return;
							}
						}
					}
					
					ItemStack selectedItemStak = selection.Bag.GetBagItem(selection.Position);
					ItemStack currentItemStak = current.Bag.GetBagItem(current.Position);

					if (!showBag_canPutItemInBag(selection.Bag, currentItemStak.IsEmpty() ? null : currentItemStak.Item, selection.Position) ||
						!showBag_canPutItemInBag(current.Bag, selectedItemStak.IsEmpty() ? null : selectedItemStak.Item, current.Position))
					{
						selection = null; 
						return;
					}

					///Empty bin
					if (selection.Bag == this.world.BinBag || current.Bag == this.world.BinBag)
					{
						if (!selection.Bag.GetBagItem(selection.Position).IsEmpty() && !current.Bag.GetBagItem(current.Position).IsEmpty())
						{
							this.world.BinBag.RemoveAt(new IntVector2(0, 0));
						}
					}

					current.Bag.Move(selection.Bag, selection.Position, current.Position);
					selection = null;
					player.RefreshState();
				}
			}
		}

		private void showBag_clickInMenu(Player player, int menuIndex) 
		{
			if (menuIndex == 0)///Back to save
			{
				this.world.ResurectPlayers(this.world.PlayerResurectPosition);
				this.showbag_close();
			}
			if (menuIndex == 1)///Back to home
			{
				this.world.ResurectPlayers(this.world.PlayerHomePosition);
				this.showbag_close();
			}
			if (menuIndex == 2)///Stop playing
			{
				Player counterpartPlayer = player.IsPlayer1 ? world.player2 : world.player1;
				player.NeedToPressStartToPlay = true;
				player.NeedToPressStartToPlayTickDelay = world.Tick + 60;
				counterpartPlayer.NeedToPressStartToPlay = false;
				this.showbag_close();
				
			}
			if (menuIndex == 3)///Exit to windows
			{
				this.world.Game.Exit();
			}
		}
		private void showbag_close()
		{
			this.IsShowingBag = false;
			if (this.npc != null && this.npcAsBag.GetBagItem(0) == null)
			{
				world.Remove(this.npc);
			}
			world.player1.RefreshState();
			world.player2.RefreshState();
			this.bagContentCacheForWorkStation.Clear();
		}
		private bool showBag_canPutItemInBag(ItemSet targetBag, Item item, IntVector2 targetPosition)
		{
			foreach (Player player in new[] { this.world.player1, this.world.player2 })
			{
				if (targetBag == player.HeroAsBag)
				{
					if (item == null || item.Hero == null)
					{
						return false;
					}
				}
				if (targetBag == player.Hero.GearSlots)
				{
					if (item == null)
					{
						return true;
					}
					if (item.Gear == null)
					{
						return false;
					}
					if (item.Gear.GetPositionInPlayerGearBag() != targetPosition.Y)
					{
						return false;
					}					
				}
			}		
			return true;
		}

		#endregion Bag

		#region WorkStation

		private void drawWorkStationScreen(SpriteBatch spriteBatch, IntVector2 screenSize, Ressource ressource)
		{
			Item selectedItem = null;
			IntVector2 topLeft = new IntVector2(40, 40);
			for (int i = 0; i < this.workStation.Recipes.Count; i++)
			{
				Recipe r = this.workStation.Recipes[i];
				bool isSelected = this.workStationCursor.Position.Y == i && this.workStationCursor.Position.X == 0;
				drawRecipeDetail(spriteBatch, ressource, new IntVector2(topLeft.X, topLeft.Y + 44 * i), r, isSelected, true);
				if (isSelected)
				{
					selectedItem = r.Result;
				}
			}
			topLeft = new IntVector2(380, 0);

			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, 410, 124), new Rectangle(0, 205, 410, 124), Color.White);
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y + 124, 410, 280), new Rectangle(0, 328, 410, 10), Color.White);
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y + 124 + 280, 410, 40), new Rectangle(0, 328, 410, 40), Color.White);
			///tools
			spriteBatch.Draw(ressource.TilesetItems32, new Rectangle(topLeft.X + 330, topLeft.Y + 50, 32, 32), new Rectangle(128, 0, 32, 32), Color.White);
			TextureRect workStationRect = this.workStationItem.GetTextureRect();
			spriteBatch.Draw(workStationRect.GetTileset(ressource), new Rectangle(topLeft.X + 40, topLeft.Y + 40, 64, 64), workStationRect.Rectangle, Color.White);


			topLeft = new IntVector2(380 + 36, 120);
			for (int i = 0; i < this.workStation.CraftList.Count; i++)
			{
				Recipe r = this.workStation.CraftList[i].Item1;
				int quantity = this.workStation.CraftList[i].Item2;
				bool isSelected = this.workStationCursor.Position.Y == i && this.workStationCursor.Position.X == 1;

				int y = topLeft.Y + 44 * i;
				if (quantity >= 2)
				{
					spriteBatch.DrawString(ressource.Font, quantity.ToString().PadLeft(2, ' ') + " X", new Vector2(topLeft.X, y + 8), Color.Black);
				}
				drawRecipeDetail(spriteBatch, ressource, new IntVector2(topLeft.X + 64, y), r, isSelected, false);
				if (isSelected)
				{
					selectedItem = r.Result;
				}
			}
			drawItemTooltip(spriteBatch, ressource, new IntVector2(40, 340), selectedItem, null);
		}
		
		private void drawRecipeDetail(SpriteBatch spriteBatch, Ressource ressource, IntVector2 topLeft, Recipe r, bool isSelected, bool checkQuantity)
		{
			int resultQuantity = r.ResultQuantity;
			int recipeWidthPx = 40 * r.Ingredients.Count * 2 + 100 + (resultQuantity != 1 ? 20 : 0);
			Color backColor = Color.DarkOliveGreen;
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X - 15, topLeft.Y, 15, 40), new Rectangle(123, 82, 15, 40), backColor);
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, recipeWidthPx, 40), new Rectangle(123 + 15, 82, 10, 40), backColor);
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X + recipeWidthPx, topLeft.Y, 15, 40), new Rectangle(123 + 25, 82, 15, 40), backColor);
			int cursorX = topLeft.X;
			bool recipeEnabled = true;
			for (int i = 0; i < r.Ingredients.Count; i++)
			{
				int quantity = r.Ingredients[i].Item2;
				Item item = r.Ingredients[i].Item1;
				Color color = Color.White;
				if (checkQuantity)
				{
					if (!this.bagContentCacheForWorkStation.ContainsKey(item))
					{
						this.bagContentCacheForWorkStation[item] = this.world.Bag.GetQuantityOf(item);
					}
					if (this.bagContentCacheForWorkStation[item] < quantity)
					{
						color = Color.Red;
						recipeEnabled = false;
					}
				}
				spriteBatch.DrawString(ressource.Font, (i == 0 ? "  " : "+ ") + quantity.ToString().PadLeft(2, ' ') + " X", new Vector2(cursorX, topLeft.Y + 8), color);
				cursorX += 48;
				TextureRect textureRect = item.GetTextureRect();
				spriteBatch.Draw(textureRect.GetTileset(ressource), new Rectangle(cursorX, topLeft.Y + 4, 32, 32), textureRect.Rectangle, Color.White);
				cursorX += 36;
			}
			{
				Color color = Color.White;
				if (!recipeEnabled)
				{
					color = Color.Red;
				}
				Item item = r.Result;
				cursorX += 8;
				if (resultQuantity != 1)
				{
					spriteBatch.DrawString(ressource.Font, " => " + resultQuantity.ToString().PadLeft(2, ' ') + " X",
								new Vector2(cursorX, topLeft.Y + 8), color);
					cursorX += 60;
				}
				else
				{
					spriteBatch.DrawString(ressource.Font, " => ",
								new Vector2(cursorX, topLeft.Y + 8), color);
					cursorX += 40;
				}
				TextureRect textureRect = item.GetTextureRect();
				spriteBatch.Draw(textureRect.GetTileset(ressource), new Rectangle(cursorX, topLeft.Y + 4, 32, 32), textureRect.Rectangle, Color.White);
			}
			if (isSelected)
			{
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X - 15, topLeft.Y, 15, 40), new Rectangle(164, 82, 15, 40), Color.Green);
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, recipeWidthPx, 40), new Rectangle(164 + 15, 82, 10, 40), Color.Green);
				spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X + recipeWidthPx, topLeft.Y, 15, 40), new Rectangle(164 + 25, 82, 15, 40), Color.Green);
			}
		}


		private void moveCursorInWorkingStation(Player player, InputState inputState, InputState previousInputState)
		{
			if (player.InputController.HasDirection(previousInputState))
			{
				return;
			}
			IntVector2 move = player.InputController.GetMoveDirection(inputState);
			if (move == IntVector2.Zero)
			{
				return;
			}
			IntVector2 newPosition = this.workStationCursor.Position + move;

			if (this.workStation.CraftList.Count > 0)
			{
				newPosition.X = Tools.InRange(newPosition.X, 0, 1);
			}
			else
			{
				newPosition.X = 0;
			}
			if (newPosition.X == 0)
			{
				newPosition.Y = Tools.InRange(newPosition.Y, 0, this.workStation.Recipes.Count - 1);
			}
			else
			{
				newPosition.Y = Tools.InRange(newPosition.Y, 0, this.workStation.CraftList.Count - 1);
			}
			this.workStationCursor.Position = newPosition;
		}
		
		private void clickInWorkingStation(Player player, InputState inputState, InputState previousInputState)
		{
			if (player.InputController.HasAnyActionButtonPressed(inputState) && !player.InputController.HasAnyActionButtonPressed(previousInputState))
			{
				if (this.workStationCursor.Position.X == 0)
				{
					this.workStation.TryEnqueueJob(this.world, this.workStationCursor.Position.Y);
				}
				else
				{
					this.workStation.CancelJob(this.world, this.workStationCursor.Position.Y);
				}
				this.bagContentCacheForWorkStation.Clear();
			}
		}

		#endregion WorkStation
            
        
    }
}
