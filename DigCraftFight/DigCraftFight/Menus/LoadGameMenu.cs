﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
	public class LoadGameMenu
	{
		private DigCraftFightGame game;
		private string[] availableGameDirectories;
		private int gameDirectoryIndex;
		private World selectedWorld;
		private int selectedMenu = 1;


		public static string[] GetSaveDirectories()
		{
			if (!Directory.Exists(Tools.SaveDirectory))
			{
				Directory.CreateDirectory(Tools.SaveDirectory);
			}
			Dictionary<string, DateTime> directories = new Dictionary<string, DateTime>();
			foreach(string directory in Directory.GetDirectories(Tools.SaveDirectory))
			{
				FileInfo fileInfo = new FileInfo(Path.Combine(directory, World.SaveWorldFileName));
				if (fileInfo.Exists)
				{
					directories[directory] = fileInfo.LastWriteTimeUtc;
				}
			}

			return directories.Keys.OrderByDescending(d => directories[d]).ToArray();

		
		}

		public LoadGameMenu(DigCraftFightGame game, string[] availableGameDirectories)
		{
			this.game = game;
			this.availableGameDirectories = availableGameDirectories; 
			this.selectSave(0);
		}

		private void selectSave(int index)
		{
			this.gameDirectoryIndex = Tools.InRange(index, 0, this.availableGameDirectories.Length - 1);
			string dir = this.availableGameDirectories[this.gameDirectoryIndex];
			string worldCode = Path.GetFileName(dir);
			this.selectedWorld = this.game.LoadWorld(worldCode);
			this.selectedMenu = 1;///Ok
		}


		private Color? getSelectedColor(int menuIndex)
		{
			if (this.selectedMenu == menuIndex)
			{
				return Color.Green;
			}
			return null;
		}
		public void Draw(SpriteBatch spriteBatch, Ressource ressource)
		{
			if (this.selectedWorld != null)
			{
				this.selectedWorld.Ground.DrawPreviousForLoadMenu(spriteBatch, ressource, this.selectedWorld.PlayerHomePosition);				
			}
			IntVector2 topLeft = new IntVector2(340, 340);
			if (this.gameDirectoryIndex - 1 >= 0)
			{
				InterfaceLayer.DrawMenu(spriteBatch, ressource, "Previous", new IntVector2(topLeft.X - 96, topLeft.Y), getSelectedColor(0));
			}
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "Ok", new IntVector2(topLeft.X + 16, topLeft.Y), getSelectedColor(1));
			if (this.gameDirectoryIndex + 1 < this.availableGameDirectories.Length)
			{
				InterfaceLayer.DrawMenu(spriteBatch, ressource, "Next", new IntVector2(topLeft.X + 96, topLeft.Y), getSelectedColor(2));
			}
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "Cancel", new IntVector2(topLeft.X+ 4, topLeft.Y + 48), getSelectedColor(3));
		}

		public void Update(InputState inputState, InputState previousInputState)
		{
			moveCursor(game.Player1InputController, inputState, previousInputState);
			moveCursor(game.Player2InputController, inputState, previousInputState);
			click(game.Player1InputController, inputState, previousInputState);
			click(game.Player2InputController, inputState, previousInputState);
		}

		private void moveCursor(InputController inputController, InputState inputState, InputState previousInputState)
		{
			if (inputController.HasDirection(previousInputState))
			{
				return;
			}
			IntVector2 move = inputController.GetMoveDirection(inputState);
			if (move == IntVector2.Zero)
			{
				return;
			}
			if (move.Y > 0)
			{
				this.selectedMenu = 3;
			}
			else if (this.selectedMenu == 3 && move.Y < 0)
			{
				this.selectedMenu = 1;
			}
			else if (this.selectedMenu != 3)
			{
				this.selectedMenu = Tools.InRange(this.selectedMenu + move.X, 0, 2);
				if (this.gameDirectoryIndex - 1 < 0)
				{
					if (this.selectedMenu == 0)
					{
						this.selectedMenu = 1;
					}
				}
				if (this.gameDirectoryIndex + 1 >= this.availableGameDirectories.Length)
				{
					if (this.selectedMenu == 2)
					{
						this.selectedMenu = 1;
					}
				}
			}
		}

		private void click(InputController inputController, InputState inputState, InputState previousInputState)
		{
			if (inputController.HasAnyActionButtonPressed(inputState) && !inputController.HasAnyActionButtonPressed(previousInputState))
			{
				if (this.selectedMenu == 1)///Ok
				{
					game.loadGameMenu = null;
				}
				else if (this.selectedMenu == 0)///Previous
				{
					this.selectSave(this.gameDirectoryIndex - 1);				
				}
				else if (this.selectedMenu == 2)///Next
				{
					this.selectSave(this.gameDirectoryIndex + 1);
				}
				else if (this.selectedMenu == 3)///Cancel
				{
					game.mainMenu = new MainMenu(this.game);
					game.loadGameMenu = null;
				}
			}
		}
	}
}
