﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DigCraftFight
{
	public class YouAreDeadMenu
	{
		private World world;
		private int tickStart;
		private int selectedMenu = -1;

		public YouAreDeadMenu(World world)
		{
			this.world = world;
			this.tickStart = world.Tick + 60 * 1;
		}


		private Color? getSelectedColor(int menuIndex)
		{
			if (this.selectedMenu == menuIndex)
			{
				return Color.Green;
			}
			return null;
		}


		public void Draw(SpriteBatch spriteBatch, Ressource ressource)
		{
			IntVector2 topLeft = new IntVector2(260, 80);
			spriteBatch.Draw(ressource.TilesetInterface40, new Rectangle(topLeft.X, topLeft.Y, 300, 300), new Rectangle(164, 0, 40, 40), Color.White);
			topLeft = new IntVector2(360, 100);
			spriteBatch.DrawString(ressource.Font, "You are dead", new Vector2(topLeft.X, topLeft.Y), Color.Red);

			topLeft = new IntVector2(340, 200);
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "Back to save point", topLeft, getSelectedColor(0));
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "Back to home", new IntVector2(topLeft.X, topLeft.Y + 64), getSelectedColor(1));
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "Rage quit", new IntVector2(topLeft.X, topLeft.Y + 128), getSelectedColor(2));
		}

		public void Update(InputState inputState, InputState previousInputState)
		{
			if (world.Tick < this.tickStart)
			{
				return;
			}
			moveCursor(world.player1.InputController, inputState, previousInputState);
			moveCursor(world.player2.InputController, inputState, previousInputState);
			click(world.player1.InputController, inputState, previousInputState);
			click(world.player2.InputController, inputState, previousInputState);
		}

		private void moveCursor(InputController inputController, InputState inputState, InputState previousInputState)
		{
			if (inputController.HasDirection(previousInputState))
			{
				return;
			}
			IntVector2 move = inputController.GetMoveDirection(inputState);
			if (move == IntVector2.Zero)
			{
				return;
			}
			this.selectedMenu = Tools.InRange(this.selectedMenu + move.Y, 0, 2);
		}

		private void click(InputController inputController, InputState inputState, InputState previousInputState)
		{
			if (inputController.HasAnyActionButtonPressed(inputState) && !inputController.HasAnyActionButtonPressed(previousInputState))
			{
				if (this.selectedMenu == 0)///Back to save point
				{
					this.world.ResurectPlayers(world.PlayerResurectPosition);
				}
				else if (this.selectedMenu == 1)///Back to home
				{
					this.world.ResurectPlayers(world.PlayerHomePosition);
				}
				else if (this.selectedMenu == 2)///Exit
				{
					this.world.Game.Exit();
				}
			}
		}

	}
}
