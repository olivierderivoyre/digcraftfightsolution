﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
	public class MainMenu
	{
		private DigCraftFightGame game;
		private int selectedMenu = 0;


		public MainMenu(DigCraftFightGame game)
		{
			this.game = game;
		}

		private Color? getSelectedColor(int menuIndex)
		{
			if (this.selectedMenu == menuIndex)
			{
				return Color.Green;
			}
			return null;
		}


		public void Draw(SpriteBatch spriteBatch, Ressource ressource)
		{
			IntVector2 topLeft = new IntVector2(340, 200);
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "Play", topLeft, getSelectedColor(0));
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "New game", new IntVector2(topLeft.X, topLeft.Y + 64), getSelectedColor(1));
			InterfaceLayer.DrawMenu(spriteBatch, ressource, "Exit", new IntVector2(topLeft.X, topLeft.Y + 128), getSelectedColor(2));
		}

		public void Update(InputState inputState, InputState previousInputState)
		{
			moveCursor(game.Player1InputController, inputState, previousInputState);
			moveCursor(game.Player2InputController, inputState, previousInputState);
			click(game.Player1InputController, inputState, previousInputState);
			click(game.Player2InputController, inputState, previousInputState);			
		}

		private void moveCursor(InputController inputController, InputState inputState, InputState previousInputState)
		{
			if (inputController.HasDirection(previousInputState))
			{
				return;
			}
			IntVector2 move = inputController.GetMoveDirection(inputState);
			if (move == IntVector2.Zero)
			{
				return;
			}
			this.selectedMenu = Tools.InRange(this.selectedMenu + move.Y, 0, 2);
		}

		private void click(InputController inputController, InputState inputState, InputState previousInputState)
		{
			if (inputController.HasAnyActionButtonPressed(inputState) && !inputController.HasAnyActionButtonPressed(previousInputState))
			{
				if (this.selectedMenu == 0)///Play
				{
					string[] saveDirectories = LoadGameMenu.GetSaveDirectories();
					if (saveDirectories.Length == 0)///Same as new game
					{
						this.game.CreateNewWorld();
					}
					else
					{
						this.game.loadGameMenu = new LoadGameMenu(this.game, saveDirectories);
					}
					this.game.mainMenu = null;
				}
				else if (this.selectedMenu == 1)///New game
				{
					this.game.CreateNewWorld();
					this.game.mainMenu = null;
				}
				else if (this.selectedMenu == 2)///Exit
				{
					this.game.Exit();
				}
			}
		}
	}
}
