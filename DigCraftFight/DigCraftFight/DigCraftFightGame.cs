using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;
using System.IO;

namespace DigCraftFight
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class DigCraftFightGame : Microsoft.Xna.Framework.Game
	{
		private GraphicsDeviceManager graphics;
		private SpriteBatch spriteBatch;
		private SpriteBatch spriteBatchInterfaceLayer;
		public MainMenu mainMenu;
		public LoadGameMenu loadGameMenu;
		public YouAreDeadMenu youAreDeadMenu;

		private Screen screen;
		private Ressource ressource;
		public InputController Player1InputController;
		public InputController Player2InputController;
		private Player player1;
		private Player player2;
		private World world;
		private int fpsCounter = 0;
		private int fpsLastShowTickCount = Environment.TickCount;
		private InterfaceLayer interfaceLayer;
		private InputState previousInputState;
		private int fps = 60;


		public static int DebugTestMap = -1;

		public DigCraftFightGame()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
			this.Window.AllowUserResizing = true;
			this.Exiting += DigCraftFightGame_Exiting;
			this.mainMenu = new MainMenu(this);

		}



		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			base.Initialize();
		}



		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			this.spriteBatch = new SpriteBatch(GraphicsDevice);
			this.spriteBatchInterfaceLayer = new SpriteBatch(GraphicsDevice);
			this.ressource = new Ressource(this.Content);

			this.Player1InputController = new InputController(PlayerIndex.One);
			this.Player2InputController = new InputController(PlayerIndex.Two);

			MapPattern.Init(Path.Combine(this.Content.RootDirectory, "MapPattern"));

			this.previousInputState = InputState.GetState();
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{

		}



		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(new Color(0.13f, 0.13f, 0.13f));

			if (this.mainMenu != null || this.loadGameMenu != null || this.interfaceLayer.IsShowingBag)///Show menu
			{
				this.spriteBatchInterfaceLayer.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Tools.GetTransformMatrixForMyBound(800, 450, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height));
				this.spriteBatchInterfaceLayer.Draw(this.ressource.TilesetInterface40, new Rectangle(0, 0, 800, 450), new Rectangle(1 * 41, 2 * 41, 40, 40), Color.White);
				if (this.mainMenu != null)
				{
					this.mainMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
				}
				else if (this.loadGameMenu != null)
				{
					this.loadGameMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
				}
				else if (this.interfaceLayer.IsShowingBag)
				{
					this.interfaceLayer.Draw(this.spriteBatchInterfaceLayer, new IntVector2(800, 450), this.ressource);
				}
				this.spriteBatchInterfaceLayer.End();
			}
			else ///Show game
			{
				this.spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Matrix.Identity);
				if (!this.interfaceLayer.IsShowingBag)
				{
					this.world.Draw(this.screen, gameTime);
					if (this.youAreDeadMenu == null)
					{
						this.interfaceLayer.Draw(this.spriteBatch, new IntVector2(this.Window.ClientBounds.Width, this.Window.ClientBounds.Height), screen.Ressource);
					}
					LongVector2 screenCenter = this.screen.GetCenter();
					int level = this.world.Ground.GetGroundLevel(screenCenter);
					IntVector2 relativeCoord = IntVector2.Delta(this.world.PlayerHomePosition, screenCenter) / (8 * Tools.CellSizeInCoord);
					this.spriteBatch.DrawString(this.ressource.Font, "Coord: " + relativeCoord.X +", " + (-1 * relativeCoord.Y) + ", level: " + level + ", FPS: " + this.fps, new Vector2(this.Window.ClientBounds.Width - 240, this.Window.ClientBounds.Height - 32), Color.White);
				}
				this.spriteBatch.End();
				if (this.youAreDeadMenu != null)
				{
					this.spriteBatchInterfaceLayer.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Tools.GetTransformMatrixForMyBound(800, 450, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height));
					this.youAreDeadMenu.Draw(this.spriteBatchInterfaceLayer, this.ressource);
					this.spriteBatchInterfaceLayer.End();
				}
			}
			base.Draw(gameTime);
		}



		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			this.fpsCounter++;
			if (Environment.TickCount - this.fpsLastShowTickCount > 1500)
			{
				this.fps = this.fpsCounter * 1000 / (Environment.TickCount - this.fpsLastShowTickCount);
				
				this.fpsCounter = 0;
				this.fpsLastShowTickCount = Environment.TickCount;
			}

			InputState inputState = InputState.GetState();
			if (inputState.Keyboard.IsKeyDown(Keys.Escape) && inputState.Keyboard.IsKeyDown(Keys.LeftShift))
			{
				this.world.Save();
				this.world = null;
				this.Exit();
				return;
			}
			if (inputState.Keyboard.IsKeyDown(Keys.F12) && inputState.Keyboard.IsKeyDown(Keys.LeftShift))
			{
				
				string dir = Path.Combine(Tools.SaveDirectory);
				System.IO.Directory.Delete(dir, true);
				Trace.WriteLine("Delete SaveDirectory: " + dir);
				
				this.world = null;
				this.Exit();
				return;
			}
			if (inputState.Keyboard.IsKeyDown(Keys.LeftAlt) && inputState.Keyboard.IsKeyDown(Keys.Enter))
			{
				this.toggleFullScreen();
			}

			if (this.mainMenu != null)
			{
				this.mainMenu.Update(inputState, this.previousInputState);
			}
			else if (this.loadGameMenu != null)
			{
				this.loadGameMenu.Update(inputState, this.previousInputState);
			}

			else
			{
				this.interfaceLayer.Update(gameTime, inputState, this.previousInputState);
				if (!this.interfaceLayer.IsShowingBag)
				{
					this.world.Update();						
					this.screen.Update(this.Window.ClientBounds.Width, this.Window.ClientBounds.Height, this.player1, this.player2);
					this.player1.Update(inputState, this.previousInputState, this.world);
					this.player2.Update(inputState, this.previousInputState, this.world);

					///Debug
					if (inputState.Keyboard.IsKeyDown(Keys.F3))
					{
						this.world.TryRepopMob();
					}
					if (this.youAreDeadMenu != null)					
					{
						this.youAreDeadMenu.Update(inputState, this.previousInputState);
					}
				}
			}
			base.Update(gameTime);
			this.previousInputState = inputState;
		}

		private void toggleFullScreen()
		{
			this.graphics.IsFullScreen = !this.graphics.IsFullScreen;
			if (this.graphics.IsFullScreen)
			{
				Trace.WriteLine("Switch to FullScreen mode");
				DisplayMode screen = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;
				this.graphics.PreferredBackBufferWidth = screen.Width;
				this.graphics.PreferredBackBufferHeight = screen.Height;
			}
			else
			{
				Trace.WriteLine("Switch to windows mode");
				this.graphics.PreferredBackBufferWidth = 800;
				this.graphics.PreferredBackBufferHeight = 600;
			}
			this.graphics.ApplyChanges();
		}


		private void DigCraftFightGame_Exiting(object sender, EventArgs e)
		{
			if (this.world != null)
			{
				this.world.Save();
			}
		}

		public void CreateNewWorld()
		{
			LongVector2 initialPlayerPosition = Ground.PlotOfLandToTopLeftCoord(100000000L, 200000000L)
				+ LevelGenerator.TutorialPlayerLocalStartingPoint * Tools.CellSizeInCoord;
			if (DebugTestMap != -1)
			{
				initialPlayerPosition = Ground.PlotOfLandToTopLeftCoord(LevelGenerator.TestLevels, LevelGenerator.TestLevels + DebugTestMap)
				+ LevelGenerator.TutorialPlayerLocalStartingPoint * Tools.CellSizeInCoord;
			}

			this.screen = new Screen(this.spriteBatch, this.ressource, initialPlayerPosition, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height);
			//			this.screen.TopLeft = initialPlayerPosition -  new IntVector2(6, 4) * Tools.CellSizeInCoord;
			// TODO: use this.Content to load your game content here


			this.player1 = new Player(this.Player1InputController, Hero.Harajuku4, true);
			this.player2 = new Player(this.Player2InputController, Hero.Moderngirl02, false);

			this.player1.Position = initialPlayerPosition;
			this.player2.Position = this.player1.Position + new IntVector2(2, 0) * Tools.CellSizeInCoord;

			string worldCode = DateTime.Now.ToString("yyyyMMdd_HHmmss");
			this.world = new World(this, worldCode, this.screen, initialPlayerPosition, this.player1, this.player2, new ItemSet(14, 7), new Ground(worldCode));

			this.player1.ActionItems.TryAdd(Weapon.NewNoobSword());
			this.player2.ActionItems.TryAdd(Weapon.NewNoobSword());
			this.player1.ActionItems.TryAdd(Item.New(ItemType.TOOL_PICK1));
			this.player2.ActionItems.TryAdd(Item.New(ItemType.TOOL_PICK1));
			if (DebugTestMap != -1)
			{
				this.world.Bag.TryAdd(new Item(Hero.Tokinoiori03));
				this.world.Bag.TryAdd(new Item(Hero.Tokinoiori05));
				this.world.Bag.TryAdd(Item.New(ItemType.GUANA));
				this.world.Bag.TryAdd(Item.New(ItemType.TEETH));
				this.world.Bag.TryAdd(Item.New(ItemType.BONE), 5);
				this.world.Bag.TryAdd(Item.New(ItemType.GROUND_EARTH_YOUNG_TREE), 15);
				this.world.Bag.TryAdd(Item.New(ItemType.WOOD_LOG), 50);
				this.world.Bag.TryAdd(Item.New(ItemType.BLOCK_EARTH), 50);
				this.world.Bag.TryAdd(Item.New(ItemType.WORKSTATION_TABLE), 1);
				this.world.Bag.TryAdd(Item.New(ItemType.BLOCK_STONE), 50);
				this.world.Bag.TryAdd(Item.New(ItemType.GROUND_WATER), 10);
				this.world.Bag.TryAdd(Item.New(ItemType.TOOL_SPADE), 1);
				this.world.Bag.TryAdd(Item.New(ItemType.ORE, 0), 150);
				this.world.Bag.TryAdd(Item.New(ItemType.ORE, 1), 150);
				this.world.Bag.TryAdd(Item.New(ItemType.ORE, 2), 150);
				this.world.Bag.TryAdd(Item.New(ItemType.ORE, 3), 150);
				this.world.Bag.TryAdd(Item.New(ItemType.ORE, 4), 150);
				this.world.Bag.TryAdd(Item.New(ItemType.WORKSTATION_ORE, 0), 1);
				this.world.Bag.TryAdd(Item.New(ItemType.WORKSTATION_ORE, 2), 1);
			}



			this.interfaceLayer = new InterfaceLayer(this.world);

			this.player1.RefreshState();
			this.player2.RefreshState();
		}


		public World LoadWorld(string worldCode)
		{
			LongVector2 initialPlayerPosition = Ground.PlotOfLandToTopLeftCoord(100000000L, 200000000L)
				+ LevelGenerator.TutorialPlayerLocalStartingPoint * Tools.CellSizeInCoord;

			this.screen = new Screen(this.spriteBatch, ressource, initialPlayerPosition, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height);


			this.player1 = new Player(this.Player1InputController, Hero.Harajuku4, true);
			this.player2 = new Player(this.Player2InputController, Hero.Moderngirl02, false);

			player1.Position = initialPlayerPosition;
			player2.Position = this.player1.Position + new IntVector2(2, 0) * Tools.CellSizeInCoord;

			this.world = new World(this, worldCode, this.screen, initialPlayerPosition, this.player1, this.player2, new ItemSet(14, 7), new Ground(worldCode));
			world.Load();
			this.interfaceLayer = new InterfaceLayer(this.world);
			this.player1.RefreshState();
			this.player2.RefreshState();
			return world;
		}
	}
}