﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class ItemStack
    {
        public readonly Item Item;
         public int Quantity;

        public ItemStack(Item item, int quantity)
        {
            this.Item = item;
            this.Quantity = quantity;
        }

		
    }
	public static class ItemStackExtention
	{
		public static bool IsEmpty(this ItemStack stack)
		{
			return stack == null || stack.Item == null || stack.Quantity <= 0;
		}
	}
	

}
