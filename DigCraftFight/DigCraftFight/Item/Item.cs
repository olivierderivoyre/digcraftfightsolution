﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace DigCraftFight
{
    public class Item
    {

		
        /// <summary>
        /// Create only one instance for non-unique items
        /// </summary>
        private static readonly Dictionary<ItemType, Item> distinctItems = Item.getDistinctItems();
		private static readonly Dictionary<int, Item> oreItems = new Dictionary<int, Item>();
		private static readonly Dictionary<MonsterGenerator, Item> distinctMonsterGeneratorItems = new Dictionary<MonsterGenerator, Item>();


		public static Item ItemBed = Item.New(ItemType.BED);


		public readonly ItemType ItemType;
		private readonly object[] parameters = new object[0];
		public readonly Hero Hero;
        public readonly WorkStation WorkStation;
        public readonly Weapon Weapon;
        public readonly Pick Pick;
		public readonly MonsterGenerator MonsterGenerator;
		public readonly BossZone BossZone;
		public readonly Ore Ore;
		public readonly CraftTemplate CraftTemplate;
		public readonly Gear Gear;
		private readonly TextureRect textureRect;

		

        private static Dictionary<ItemType, Item> getDistinctItems()
        {
            Dictionary<ItemType, Item> distinctItems = new Dictionary<ItemType, Item>();
            foreach (ItemType itemType in Enum.GetValues(typeof(ItemType)))
            {
                if (itemType == ItemType.GROUND_MONSTER_GENERATOR
                || itemType == ItemType.HERO
                || itemType == ItemType.BOSS_ZONE
				|| itemType == ItemType.ORE
				|| itemType == ItemType.CRAFT_TEMPLATE
				|| itemType == ItemType.GEAR)
                {
                    continue;
                }
				if (itemType >= ItemType.DELIMITER_CRAFT_STATION && itemType <= ItemType.DELIMITER_CRAFT_STATION_END)
				{
					continue;
				}
				if (itemType >= ItemType.DELIMITER_TOOL && itemType <= ItemType.DELIMITER_TOOL_END)
				{
					continue;
				}
				if (itemType >= ItemType.DELIMITER_WEAPON && itemType <= ItemType.DELIMITER_WEAPON_END)
				{
					continue;
				}
				Item item = new Item(itemType, (UnicodeEncoding) null);               
                distinctItems.Add(itemType, item);                
            }
            return distinctItems;
        }

        public static Item New(ItemType itemType, params object[] parameters)
        {
			if (itemType == ItemType.ORE)
			{
				if (parameters == null || parameters.Length != 1 || !(parameters[0] is int))
				{
					throw new ArgumentException("Missing lvl parameter for ORE");
				}
				int lvl = (int) parameters[0];
				if (!Item.oreItems.ContainsKey(lvl))
				{
					Item.oreItems[lvl] = new Item(new Ore(lvl));
				}
				return Item.oreItems[lvl];
			}
            if (Item.distinctItems.ContainsKey(itemType))
            {
                return Item.distinctItems[itemType];
            }
            return new Item(itemType, (DuplicateWaitObjectException) null, parameters);
        }

		private Item(ItemType itemType, UnicodeEncoding uniqueItem)
		{
			this.ItemType = itemType;
			this.textureRect = GetTextureRect(this.ItemType);
		}

		private Item(ItemType itemType, DuplicateWaitObjectException useTheMethodItemNew, object[] parameters)
        {
            this.ItemType = itemType;
			this.parameters = parameters;
			if (this.parameters == null)
			{
				this.parameters = new object[0];
			}

			if (this.ItemType == ItemType.GROUND_MONSTER_GENERATOR
				|| this.ItemType == ItemType.HERO
				|| this.ItemType == ItemType.BOSS_ZONE)
			{
				throw new ArgumentException();
			}

			this.WorkStation = WorkStation.TryGetWorkStation(this.ItemType, parameters);
            this.Weapon = Weapon.TryGetWeapon(this.ItemType, parameters);
            this.Pick = Pick.TryGetPick(this.ItemType);
			this.CraftTemplate = CraftTemplate.TryCraftTemplate(this.ItemType, parameters);
			if (this.ItemType == ItemType.GEAR)
			{
				this.Gear = new Gear(parameters);
			}

            this.textureRect = GetTextureRect(this.ItemType);
        }

		public static Item FromComItem(ComItem com)
		{
			if (com == null)
			{
				return null;
			}
			if (com.ItemType == ItemType.GROUND_MONSTER_GENERATOR)
			{
				var m = new MonsterGenerator(com.Parameters);				
				return Item.CreateFrom(m);				
			}
			if (com.ItemType == ItemType.HERO)
			{
				return new Item(Hero.FomComParameter(com.Parameters));
			}
			if (com.ItemType == ItemType.BOSS_ZONE)
			{
				return new Item(new BossZone(com.Parameters));
			}
			return Item.New(com.ItemType, com.Parameters);
		}

        public Item(Hero hero)            
        {
			this.ItemType = ItemType.HERO;
            this.Hero = hero;
            this.textureRect = new TextureRect(this.Hero);
        }

		private Item(MonsterGenerator monsterGenerator)
		{
			this.ItemType = ItemType.GROUND_MONSTER_GENERATOR;
			this.MonsterGenerator = monsterGenerator;
            this.textureRect = GetTextureRect(ItemType.GROUND_GRASS);
		}

		public static Item CreateFrom(MonsterGenerator monsterGenerator)
		{
			if (!distinctMonsterGeneratorItems.ContainsKey(monsterGenerator))
			{
				distinctMonsterGeneratorItems[monsterGenerator] = new Item(monsterGenerator);
			}
			return distinctMonsterGeneratorItems[monsterGenerator];
		}

		public Item(BossZone bossZone)
		{
			this.ItemType = ItemType.BOSS_ZONE;
			this.BossZone = bossZone;
            this.textureRect = GetTextureRect(this.ItemType);
        }
		private Item(Ore ore)
		{
			this.ItemType = ItemType.ORE;
			this.Ore = ore;
		}
        public override string ToString()
        {
            return this.ItemType.ToString();
        }

		public static ComItem ToComItem(Item item)
		{
			if (item == null)
			{
				return null;
			}
			if (item.MonsterGenerator != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = item.MonsterGenerator.getComParameter() };
			}
			if (item.Hero != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = item.Hero.getComParameter() };
			}
			if (item.BossZone != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = item.BossZone.getComParameter() };
			}
			if (item.Ore != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = new object[]{item.Ore.Level}};
			}
			if (item.WorkStation != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = item.WorkStation.getComParameter() };			
			}
			if (item.CraftTemplate != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = item.CraftTemplate.getComParameter() };			
			}
			if (item.Weapon != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = item.Weapon.getComParameter() };
			}
			if (item.Gear != null)
			{
				return new ComItem { ItemType = item.ItemType, Parameters = item.Gear.getComParameter() };
			}
			return new ComItem { ItemType = item.ItemType };
		}

		public void fillComItemSet(List<ComItemSet> allItemSet)
		{
			if (this.WorkStation != null)
			{
				this.WorkStation.fillComItemSet(allItemSet);
			}
		}
		public void initFromComItemSet(ComItemSet[] allItemSet)
		{
			if (this.WorkStation != null)
			{
				this.WorkStation.initFromComItemSet(allItemSet);
			}
		}

		public static TextureRect GetTextureRect(ItemType itemType)
        {

			switch (itemType)
			{
				///Ground
				case ItemType.GROUND_EARTH: return TextureRect.NewGround(0, 0);
				case ItemType.GROUND_GRASS: return TextureRect.NewGround(1, 0);
				case ItemType.GROUND_MONSTER_GENERATOR: return GetTextureRect(ItemType.GROUND_GRASS);
				case ItemType.GROUND_WATER: return TextureRect.NewGround(2, 0);
				case ItemType.GROUND_BAT: return TextureRect.NewGround(3, 0);
				case ItemType.GROUND_WOOD: return TextureRect.NewGround(5, 0);
				case ItemType.GROUND_WATER_BRIDGE: return TextureRect.NewGround(6, 0);
				case ItemType.BLOCK_EARTH: return TextureRect.NewGround(0, 1);
				case ItemType.TREE: return TextureRect.NewGround(1, 1);
				case ItemType.BLOCK_STONE: return TextureRect.NewGround(2, 1);
				case ItemType.WORKSTATION_TABLE: return TextureRect.NewGround(3, 1);
				case ItemType.GROUND_EARTH_YOUNG_TREE: return TextureRect.NewGround(4, 1);
				case ItemType.APPLE_TREE_NO_FRUIT: return TextureRect.NewGround(5, 1);
				case ItemType.APPLE_TREE_WITH_FRUIT: return TextureRect.NewGround(6, 1);
				case ItemType.BLOCK_BRICK_EARTH: return TextureRect.NewGround(0, 2);
				case ItemType.BLOCK_SULFER: return TextureRect.NewGround(1, 2);
				case ItemType.BED: return TextureRect.NewGround(4, 2);
				case ItemType.COMMODE: return TextureRect.NewGround(5, 2);
				case ItemType.DOOR: return TextureRect.NewGround(6, 2);
				case ItemType.WORKSTATION_SMALL_TABLE: return TextureRect.NewGround(7, 2);
				case ItemType.WORKSTATION_SMALL_TABLE_WATER: return TextureRect.NewGround(0, 3);
				case ItemType.BOSS_ZONE: return TextureRect.NewGround(1, 3);
				case ItemType.ORE_SPOT: return TextureRect.NewGround(0, 4);
				case ItemType.ORE: return TextureRect.NewGround(0,4);
				case ItemType.WORKSTATION_ORE: return TextureRect.NewGround(0, 5);
				
				///Tools
				case ItemType.TEETH: return TextureRect.NewItems32(0, 0);
				case ItemType.BONE: return TextureRect.NewItems32(1, 0);
				case ItemType.TOOL_PICK1: return TextureRect.NewItems32(2, 0);
				case ItemType.GUANA: return TextureRect.NewItems32(3, 0);
				case ItemType.EMOTICON_WORKING: return TextureRect.NewItems32(4, 0);
				case ItemType.EMOTICON_SLEEPING: return TextureRect.NewItems32(5, 0);
				case ItemType.TOOL_SPADE: return TextureRect.NewItems32(6, 0);
				case ItemType.WOOD_LOG: return TextureRect.NewItems32(7, 0);
				case ItemType.APPLE : return TextureRect.NewItems32(8, 0);
				case ItemType.EMOTICON_SWORD: return TextureRect.NewItems32(0, 1);
				case ItemType.EMOTICON_FLAIL: return TextureRect.NewItems32(1, 1);
				case ItemType.EMOTICON_FORBID: return TextureRect.NewItems32(2, 1);
				case ItemType.TOOL_PICK2: return TextureRect.NewItems32(3, 1);
				case ItemType.GEAR: return TextureRect.NewItems32(2, 3);
			}

            return null;
        }  

        public TextureRect GetTextureRect()
        {
            if (this.BossZone != null)
            {
                if (this.BossZone.BossDefeated)
                {
                    return BossZone.TextureRectBossDefeated;
                }
                if (this.BossZone.BossAlive)
                {
                    return BossZone.TextureRectBossAlive;
                }
                return BossZone.TextureRectBossWaiting;
            }
			if (this.Ore != null)
			{
				return this.Ore.GetTextureRect();
			}
			if (this.ItemType == ItemType.WORKSTATION_ORE)
			{
				return TextureRect.NewGround(this.WorkStation.OreLevel % 15, 5);
			}
			if (this.CraftTemplate != null)
			{
				return this.CraftTemplate.GetTextureRect();
			}
			if (this.Weapon != null)
			{
				return this.Weapon.GetTextureRect();
			}
			if (this.Gear != null)
			{
				return this.Gear.GetTextureRect();
			}
			
            return this.textureRect;
        }
       

		public bool IsCleverBlock()
		{
			if (this.BossZone != null) 
			{ 
				return true; 
			}
			if (this.WorkStation != null)
			{
				return true;
			}
			return false;
		}
        /// <summary>
        /// return isAlive
        /// </summary>
        public bool Update(World world, LongVector2 position, int tickSinceObjectCreation)
        {           
			if (this.BossZone != null)
			{
				this.BossZone.Update(world, position);				
			}
			if (this.WorkStation != null)
			{
				this.WorkStation.Update(world, position);		
			}
            return true;
        }



        public void GenerateDropWhenPlayerPicked(World world, LongVector2 position)
        {
            if (this.ItemType == ItemType.TREE)
            {
				Drop.AddDropAround(world, Item.New(ItemType.WOOD_LOG), position);              
				if(Tools.RandomWin(5))
                {
					Drop.AddDropAround(world, Item.New(ItemType.GROUND_EARTH_YOUNG_TREE), position);					
                }
                return;
            }
			if (this.ItemType == ItemType.APPLE_TREE_NO_FRUIT || this.ItemType == ItemType.APPLE_TREE_WITH_FRUIT)
			{
				///When it is a player that drop the item, it probably only get the fruit.
				if (this.ItemType == ItemType.APPLE_TREE_WITH_FRUIT)
				{
					Drop.AddDropAround(world, Item.New(ItemType.APPLE), position);	
				}
				if (Tools.RandomWin(5))
				{
					Drop.AddDropAround(world, Item.New(ItemType.APPLE_TREE_NO_FRUIT), position);
				}
				else
				{
					Drop.AddDropAround(world, Item.New(ItemType.WOOD_LOG), position);
				}
				return;
			}
            ///Default            
			Drop.AddDropAround(world, this, position);	
        }

		public Item GatherByNpc(World world, LongVector2 position)
		{
			if (this.ItemType == ItemType.APPLE_TREE_WITH_FRUIT)
			{
				world.Ground.SetFurniture(Item.New(ItemType.APPLE_TREE_NO_FRUIT), position);
				return Item.New(ItemType.APPLE);
			}
			return null;
		}


        public void UpdateItemOnGround(World world, LongVector2 position)
        {            
			if (this.ItemType == ItemType.GROUND_GRASS || this.ItemType == ItemType.GROUND_WATER)
            {
                var around = new  []{ new IntVector2(0, 1), new IntVector2(1, 0), new IntVector2(0, -1), new IntVector2(-1, 0) };
                var neightbor = position + around[Tools.Rand.Next(4)] * Tools.CellSizeInCoord;
                Item carpet = world.Ground.GetCarpet(neightbor);
                if (carpet != null)
                {
                    if (carpet.ItemType == ItemType.GROUND_EARTH)
                    {
                        Item furniture = world.Ground.GetFurniture(neightbor);
                        if (furniture == null)
                        {
							world.Ground.SetCarpet(Item.New(this.ItemType), neightbor);
                        }
						else if (this.ItemType == ItemType.GROUND_GRASS && furniture.ItemType == ItemType.TREE)
                        {
							world.Ground.SetCarpet(Item.New(ItemType.GROUND_GRASS), neightbor);
                        }
                    }
                }
            }
			if (this.ItemType == ItemType.GROUND_EARTH_YOUNG_TREE)
			{
				Item carpet = world.Ground.GetCarpet(position);
				if (carpet != null && carpet.ItemType == this.ItemType)
				{
					world.Ground.SetCarpet(Item.New(ItemType.GROUND_GRASS), position);
					world.Ground.SetFurniture(Item.New(ItemType.TREE), position);
				}
			}
			if (this.ItemType == ItemType.APPLE_TREE_NO_FRUIT)
			{				
				world.Ground.SetFurniture(Item.New(ItemType.APPLE_TREE_WITH_FRUIT), position);				
			}
        }

		public void OnGroundLoad(World world, LongVector2 position)
		{
			if (this.MonsterGenerator != null)
			{
				this.MonsterGenerator.GenerateMonster(world, position);
			}
			if (this.Hero != null)
			{
				world.Add(new Npc(world, this.Hero, position));
				world.Ground.SetFurniture(null, position);
				Trace.WriteLine("Added NPC in the world");
			}
			if (this.IsCleverBlock())
			{
				world.AddCleverBlock(this, position);///Register to see if player near the statue.
			}
			if (this.ItemType == ItemType.ORE_SPOT)
			{
				int oreLevel = world.GameDifficulty.GenerateOreLevel(world.Ground.GetGroundLevel(position));
				foreach (IntVector2 around in Tools.AroundPoints)
				{
					Item item = world.Ground.GetFurniture(position + around * Tools.CellSizeInCoord);
					if (item != null && item.Ore != null)
					{
						oreLevel = item.Ore.Level;
						break;
					}
				}

				world.Ground.SetFurniture(Item.New(ItemType.ORE, oreLevel), position);
			}
		}

		public Item CraftClone()
		{
			if (this.CraftTemplate != null)
			{
				return this.CraftTemplate.Craft();
			}
			return Item.New(this.ItemType, this.parameters);
		}

		public static bool HaveSameAttributes(Item i1, Item i2)
		{
			if (i1 == null)
			{
				return i2 == null;
			}
			if (i2 == null)
			{
				return i1 == null;
			}
			if (i1.ItemType != i2.ItemType)
			{
				return false;
			}            
			if (i1.parameters.Length != i2.parameters.Length)
			{
				return false;
			}
			for (int i = 0; i < i1.parameters.Length; i++)
			{
				if(!object.Equals(i1.parameters[i],i2.parameters[i]))
				{
					return false;
				}
			}
			return true;
		}

		public void GetTooltip(ref string header, ref string subText, ref int improvement, Player p1, Player p2)
		{

			if (this.CraftTemplate != null)
			{
				header = this.CraftTemplate.GeneratedItemDisplayName;
				int itemLevel = this.CraftTemplate.CraftLevel * 5;				
				if (this.CraftTemplate.CraftLevel == 0)
				{
					itemLevel = 3;
					if (this.CraftTemplate.CraftType == 3)
					{
						subText = "level ~3";
					}
					else
					{
						subText = "Add life";
					}
				}
				else
				{
					subText = "Level ~" + itemLevel;
				}
				int playerLevel;
				if (this.CraftTemplate.CraftType == 3)
				{
					playerLevel = getBestWeaponLevel(p1, p2);
				}
				else
				{
					playerLevel = getBestGearLevel(this.CraftTemplate.CraftType, p1, p2);
				}
				if (playerLevel < itemLevel)
				{
					improvement = 1;
				}
				if (playerLevel > itemLevel)
				{
					improvement = -1;
				}
				return;
			}
			if (this.Weapon != null)
			{
				header = this.Weapon.DisplayName;
				subText = "Weapon level " + (this.Weapon.Level + 1);
				int maxLevel = getBestWeaponLevel(p1, p2);
				if (maxLevel < this.Weapon.Level)
				{
					improvement = 1;
				}
				if (maxLevel > this.Weapon.Level)
				{
					improvement = -1;
				}
				return;
			}
			if (this.Gear != null)
			{
				header = this.Gear.DisplayName;
				subText = "Gear level " + (this.Gear.Level + 1);

				int maxLevel = getBestGearLevel(this.Gear.GearType, p1, p2);
				if (maxLevel < this.Gear.Level)
				{
					improvement = 1;
				}
				if (maxLevel > this.Gear.Level)
				{
					improvement = -1;
				}
				return;
			}
			if (this.Ore != null)
			{
				header = Ore.GetOreName(this.Ore.Level);
				if (this.Ore.Level == 0)
				{
					subText = "Ore";
				}
				else
				{
					subText = "Ore of level " + (this.Ore.Level * 5);
				}
				return;
			}					
			if (this.ItemType == ItemType.WORKSTATION_ORE)
			{
				header = Ore.GetOreName(this.WorkStation.OreLevel) + " station";
				subText = "Workstation";
				return;
			}
					
			if (this.ItemType == DigCraftFight.ItemType.GROUND_EARTH_YOUNG_TREE)
			{
				header = "Young tree";
				subText = "Seed";
				return;
			}
			if (this.Hero != null)
			{
				header = this.Hero.DisplayName;
				subText = "Hero";
				return;
			}
			string itemName = this.ItemType.ToString();
			if (itemName.Contains("_"))
			{
				subText = itemName.Substring(0, itemName.IndexOf("_"));
				header = itemName.Substring(itemName.IndexOf("_") + 1);
			}
			else
			{
				header = itemName;
			}
		}

		private static int getBestWeaponLevel(Player p1, Player p2)
		{
			int maxLevel = -1;
			if (p1 != null)
			{
				maxLevel = p1.GetBestWeaponLevel();
			}
			if (p2 != null)
			{
				maxLevel = Math.Max(maxLevel, p2.GetBestWeaponLevel());
			}
			return maxLevel;
		}

		private int getBestGearLevel(int gearType, Player p1, Player p2)
		{
			int maxLevel = -1;
			if (p1 != null && !p1.NeedToPressStartToPlay)
			{
				maxLevel = p1.GetBestGearLevel(gearType);
			}
			if (p2 != null && !p2.NeedToPressStartToPlay)
			{
				maxLevel = Math.Max(maxLevel, p2.GetBestGearLevel(gearType));
			}
			return maxLevel;
		}



    }
}
