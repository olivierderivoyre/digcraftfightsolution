﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	/// <summary>
	/// Item factory
	/// </summary>
	public class CraftTemplate
	{
		public readonly int CraftType;
		public readonly int CraftLevel;
		private TextureRect textureRect;
		public string GeneratedItemDisplayName;

		public CraftTemplate(int craftType, int level)
		{
			this.CraftType = craftType;
			this.CraftLevel = level;
			this.textureRect = TextureRect.NewItems32(this.CraftLevel % 15, 2 + this.CraftType);
			this.GeneratedItemDisplayName = GetItemDisplayName(craftType, level);
		}

		public static CraftTemplate TryCraftTemplate(ItemType itemType, object[] parameters)
		{
			if (itemType == ItemType.CRAFT_TEMPLATE)
			{
				return new CraftTemplate((int)parameters[0], (int)parameters[1]);
			}
			return null;
		}

		public object[] getComParameter()
		{
			return new object[] { this.CraftType, this.CraftLevel };
		}

		
		public TextureRect GetTextureRect()
		{
			return this.textureRect;
		}

		public Item Craft()
		{

			int Level = Math.Abs(this.CraftLevel * 5 + Tools.RandDiceZeroCentered(5));
			if (this.CraftLevel == 0)
			{
				Level = Tools.ThrowDices(2, 2);///[2-4]
			}
			string displayName = GetItemDisplayName(this.CraftType, this.CraftLevel);
			if (this.CraftType == 3)///SWORD
			{
				int TextureX = this.CraftLevel % 15;
				int TextureY = 5;
				double Damage = GameDifficulty.GetWeaponDamage(Level);
				int Speed = 10 + Tools.RandDiceZeroCentered(5);
				int Range = (20 + Tools.RandDiceZeroCentered(12)) * Tools.QuaterCellSizeInCoord;
				int ExplosionRange = 0;
				int Arg1 = 0;
				int Arg2 = 0;

				if (this.CraftLevel % 2 == 0)
				{
					Speed = 50;
					ExplosionRange = 4 + Tools.RandDiceZeroCentered(2);
				}
				return Item.New(ItemType.WEAPON_SIMPLE_BULLET, TextureX, TextureY, displayName, Level, Damage, Speed, Range, ExplosionRange, Arg1, Arg2);
			}
			else
			{
				int TextureX = this.CraftLevel % 15;
				int TextureY = 2 + this.CraftType;
				int GearType = this.CraftType;///Armor=0, shield=1, boot=2
				double Health = GameDifficulty.GetArmorHealth(Level);
				return Item.New(ItemType.GEAR, TextureX, TextureY, displayName, Level, GearType, Health);
			}
		}

		public static string GetItemTypeName(int craftType)
		{
			switch (craftType)
			{
				case 0: return "Armor";
				case 1: return "Shield";
				case 2: return "Boot";
				case 3: return "Sword";
				default: throw new NotImplementedException();
			}
		}
		public static string GetItemDisplayName(int craftType, int craftLevel)
		{
			string oreName = Ore.GetOreName(craftLevel);
			return oreName + " " + GetItemTypeName(craftType).ToLower();
		}

		public override string ToString()
		{
			switch (this.CraftType)
			{
				case 0: return "Craft template for armor (0) of level " + this.CraftLevel;
				case 1: return "Craft template for shield (1) of level " + this.CraftLevel;
				case 2: return "Craft template for boot (2) of level " + this.CraftLevel;
				case 3:	return "Craft template for weapon (3) of level " + this.CraftLevel;
				default: return "Craft template for type " + this.CraftType  + ", level " + this.CraftLevel;
			}
		}

	}
}
