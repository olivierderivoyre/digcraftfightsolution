﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	public class Ore
	{
		public readonly int Level;

		public Ore(int level)
		{
			this.Level = level;
		}


		public TextureRect GetTextureRect()
		{
			return TextureRect.NewGround(this.Level % 15, 4);
		}

		#region ore names
		private static readonly string[] oreNames = {
								 "Iron",
								 "Araplese",
								 "Odiaprum",
								 "Podroni",
								 "Femulium",
								 "Ixefralt",
								 "Egosnalt",
								 "Xoliasten",
								 "Oplulalt",
								 "Synarium",
								 "Lapletium",
								 "Fumoilium",
								 "Solonium",
								 "Ubashum",
								 "Eweysmum",
								 "Badosium",
								 "Eveipralt",
								 "Someisten",
								 "Baglynese",
								 "Soayucium",
								 "Pearalium",
								 "Giutucium",
								 "Ejeistrum",
								 "Loutrunese",								 
								 "Iustruis",
								 "Oblufalt",
								 "Seteolium",
								 "Anothium",
								 "Iuidronese",
								 "Ajayclalt",
								 "Siojesium",
								 "Istrejium",
								 "Poagiatre",
								 "Uhioshium",
								 "Lucluatre",
								 "Ofrazalt",
								 "Papuitre",
								 "Otegrese",
								 "Waywhunese",
								 "Ajoiflum",
								 "Soimucium",
								 "Aswedese",
								 "Sovuitium",
								 "Okeygrium",
								 "Wushuise",
								 "Efriogum",
								 "Faioirium",
								 "Uhedrese",
								 "Joychocium",
								 "Ageochium",
								 "Toyfolium",
								 "Uswuhalt",
								 "Caiveinese",
								 "Ipiesnium",
								 "Rocresten",
								 "Asheuyalt",
								 "Sahiosten",
								 "Eyublium",
								 "Kiefrolium",
								 "Etuaspum",
								 "Wounocium",
								 "Odrodalt",
								 "Saijiutium",
								 "Oseplese",
								 "Qogroirium",
								 "Osnuawium",
								 "Sakanese",
								 "Ofeblalt",
								 "Beathecium",
								 "Efuplese",
								 "Peitocium",
								 "Afrejese",
								 "Peoberium",
								 "Okaestrese",	
								 "Dafruitium",
								 "Acleosum",
								 "Cogoisten",
								 "Agatrese",
								 "Zoicrosten",
								 "Ujaotrum",
								 "Siequsium",
								 "Astexalt",
								 "Guicianese",
								 "Ekoaclum",								 
								 "Hobluanium",
								 "Uchiyzese",
								 "Goyeycium",
								 "Uleskium",
								 "Cethacium",
								 "Opeopralt",
								 "Biupalium",
								 "Ufrowese",
								 "Soypiyrium",
								 "Isueblese"
							 };
		#endregion ore names

		public static string GetOreName(int oreLevel)
		{
			return Ore.oreNames[oreLevel % oreNames.Length];
		}
		

	}
}
