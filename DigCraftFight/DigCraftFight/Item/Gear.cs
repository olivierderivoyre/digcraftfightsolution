﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	public class Gear
	{
		public readonly int TextureX;
		public readonly int TextureY;
		public readonly string DisplayName;
		public readonly int Level;
		public readonly int GearType;///Armor=0, shield=1, boot=2
		public readonly double Health;
		private readonly TextureRect textureRect;


		public Gear(object[] parameters)
		{
			this.TextureX = (int)parameters[0];
			this.TextureY = (int)parameters[1];
			this.DisplayName = (string)parameters[2];
			this.Level = (int)parameters[3];
			this.GearType = (int)parameters[4];
			this.Health = Tools.ContertToDouble(parameters[5]);
			this.textureRect = TextureRect.NewItems32(this.TextureX, this.TextureY);
			if (this.GearType < 0 || this.GearType >= 3)
			{
				throw new ArgumentException();
			}
		}

		public object[] getComParameter()
		{
			return new object[] { this.TextureX, this.TextureY, this.DisplayName, this.Level, this.GearType, this.Health };
		}

		public TextureRect GetTextureRect()
		{
			return this.textureRect;
		}


		public int GetPositionInPlayerGearBag()
		{			
			switch(this.GearType)
			{
				case 1: return 0;///shield
				case 0: return 1;///Armor				
				case 2: return 2;///boot
			}
			throw new Exception();
		}
		
	}
}
