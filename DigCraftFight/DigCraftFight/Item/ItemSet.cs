﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace DigCraftFight
{
    public class ItemSet
    {
        public readonly int Width;
        public readonly int Height;
        private readonly ItemStack[] ItemContainers;
		

        public ItemSet(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.ItemContainers = new ItemStack[Width * Height];
        }
        
        public void SetEmptyCell(int index, Item item)
        {
            if (this.GetBagItem(index) != null)
            {
                throw new ArgumentException();
            }
            this.ItemContainers[index] = new ItemStack(item, 1);
        }

		private bool tryAdd(Item item, int quantity, bool simulate)
		{
			///Try stack
			for (int i = 0; i < this.ItemContainers.Length; i++)
			{
				if (this.ItemContainers[i] != null && this.ItemContainers[i].Item == item)
				{
					if (this.ItemContainers[i].Quantity + quantity < 999)
					{
						if (!simulate)
						{
							this.ItemContainers[i].Quantity += quantity;
						}
						return true;
					}
				}
			}
			///create new stack
			for (int i = 0; i < this.ItemContainers.Length; i++)
			{
				if (this.ItemContainers[i] == null)
				{
					if (!simulate)
					{
						this.ItemContainers[i] = new ItemStack(item, quantity);
					}
					return true;
				}
			}
			return false;
		}
        public bool TryAdd(Item item, int quantity = 1)
        {
			return tryAdd(item, quantity, false);
        }

		public bool MainBagTryAddByPlayerLoot(Item item, bool isPlayer1)
		{
			int playerZoneX = 0;
			int counterpartPlayerZoneX = this.Width - 3;
			if (!isPlayer1)
			{
				playerZoneX = this.Width - 3;
				counterpartPlayerZoneX = playerZoneX;
			}
			///xStart, xEnd, addOnExistingStack
			Tuple<int, int, bool>[] subParts = new []
			{
				new Tuple<int, int, bool>(playerZoneX, playerZoneX + 2, true),///My bag
				new Tuple<int, int, bool>(2, this.Width - 3, true),///common bag
				new Tuple<int, int, bool>(playerZoneX, playerZoneX + 2, false),///My bag
				new Tuple<int, int, bool>(2, this.Width - 3, false),///common bag
				new Tuple<int, int, bool>(counterpartPlayerZoneX, counterpartPlayerZoneX + 2, true),///counter part bag
				new Tuple<int, int, bool>(counterpartPlayerZoneX, counterpartPlayerZoneX + 2, false)

			};
			
			foreach(Tuple<int, int, bool> part in subParts)
			{
				int xStart = part.Item1;
				int xEnd = part.Item2;
				bool addOnExistingStack = part.Item3;
				for (int y = 0; y < this.Height; y++)
				{
					for (int x = xStart; x < xEnd; x++)
					{
						if (addOnExistingStack)
						{
							if (mainBagTryAddOnExistingStack(item, new IntVector2(x, y)))
							{
								return true;
							}
						}
						else
						{
							if (mainBagTryAddOnEmptyStack(item, new IntVector2(x, y)))
							{
								return true;
							}
						}
					}
				}
			}
			return false;
		}
		private bool mainBagTryAddOnExistingStack(Item item, IntVector2 p)
		{
			int i = this.getIndex(p);
			if (!this.ItemContainers[i].IsEmpty() && this.ItemContainers[i].Item == item)
			{
				if (this.ItemContainers[i].Quantity + 1 < 999)
				{
					this.ItemContainers[i].Quantity++;
					return true;
				}
			}
			return false;
		}
		private bool mainBagTryAddOnEmptyStack(Item item, IntVector2 p)
		{
			int i = this.getIndex(p);
			if (this.ItemContainers[i].IsEmpty())
			{
				this.ItemContainers[i] = new ItemStack(item, 1);
				return true;
			}
			return false;
		}

		public bool HasSpaceFor(Item item, int quantity = 1)
		{
			return tryAdd(item, quantity, true);
		}
        private int getIndex(int x, int y)
        {
            if (x < 0 || x >= this.Width) throw new ArgumentException("x=" + x);
            if (y < 0 || y >= this.Height) throw new ArgumentException("y=" + y);
            return y * this.Width + x;
        }

        private int getIndex(IntVector2 position)
        {
            return getIndex(position.X, position.Y);
        }

        public ItemStack GetBagItem(int i)
        {
            return this.ItemContainers[i];
        }
        public ItemStack GetBagItem(int x, int y)
        {
            return this.ItemContainers[this.getIndex(x, y) ];
        }
        public ItemStack GetBagItem(IntVector2 position)
        {
            return this.ItemContainers[this.getIndex(position)];
        }
        public void Move(ItemSet fromSet, IntVector2 from, IntVector2 to)
        {
            if (fromSet == this && from == to)
            {
                return;
            }
            int fromIndex = fromSet.getIndex(from);
            int toIndex = this.getIndex(to);
            if (fromSet.ItemContainers[fromIndex] == null)
            {
                return;
            }
           
            if (this.ItemContainers[toIndex] == null)
            {
                this.ItemContainers[toIndex] = fromSet.ItemContainers[fromIndex];
                fromSet.ItemContainers[fromIndex] = null;
            }
            else if (this.ItemContainers[toIndex].Item == fromSet.ItemContainers[fromIndex].Item)
            {

                int newToQuantity = Math.Min(999, this.ItemContainers[toIndex].Quantity + fromSet.ItemContainers[fromIndex].Quantity);
                int newFromQuantity = this.ItemContainers[toIndex].Quantity + fromSet.ItemContainers[fromIndex].Quantity - newToQuantity;
                this.ItemContainers[toIndex].Quantity = newToQuantity;
                if (newFromQuantity > 0)
                {
                    fromSet.ItemContainers[fromIndex].Quantity = newFromQuantity;
                }
                else
                {
                    fromSet.ItemContainers[fromIndex] = null;
                }

            }
            else
            {
                ItemStack previousItem = this.ItemContainers[toIndex];
                this.ItemContainers[toIndex] = fromSet.ItemContainers[fromIndex];
                fromSet.ItemContainers[fromIndex] = previousItem;
              
            }            
        }

        public bool IsEmpty()
        {
            return !this.ItemContainers.Any(i => i != null);            
        }

        public void Refresh()
        {
            for (int i = 0; i < this.ItemContainers.Length; i++)
            {
                if (this.ItemContainers[i] != null && this.ItemContainers[i].Quantity <= 0)
                {
                    this.ItemContainers[i] = null;
                }
            }
        }

        public bool Contains(Item item, int minQuantity = 1)
        {
            for (int i = 0; i < this.ItemContainers.Length; i++)
            {
                if (this.ItemContainers[i] != null && this.ItemContainers[i].Item == item)
                {
                    if (this.ItemContainers[i].Quantity >= minQuantity)
                    {
                        return true;
                    }
                    else
                    {
                        minQuantity -= this.ItemContainers[i].Quantity;
                    }
                }
            }
            return false;
        }

		public int GetQuantityOf(Item item)
		{
			int r = 0;
			for (int i = 0; i < this.ItemContainers.Length; i++)
			{
				if (this.ItemContainers[i] != null && this.ItemContainers[i].Item == item)
				{					
					r += this.ItemContainers[i].Quantity;					
				}
			}
			return r;
		}
		public List<Item> GetQuantityOfFood(out int totalFoodItemCount)
		{
			List<Item> foodItems = new List<Item>();
			int total = 0;
			for (int i = 0; i < this.ItemContainers.Length; i++)
			{
				
				if (this.ItemContainers[i] != null)
				{
					Item item = this.ItemContainers[i].Item;
					if (item.ItemType > ItemType.DELIMITER_FOOD && item.ItemType < ItemType.DELIMITER_FOOD_END)
					{
						total += this.ItemContainers[i].Quantity;
						foodItems.Add(item);
					}
				}
			}
			totalFoodItemCount = total;
			return foodItems;
		}
        public void Remove(Item item, int quantityToRemove = 1)
        {
            for (int i = 0; i < this.ItemContainers.Length; i++)
            {
                if (this.ItemContainers[i] != null && this.ItemContainers[i].Item == item)
                {
                    if (this.ItemContainers[i].Quantity <= quantityToRemove)
                    {
                        quantityToRemove -= this.ItemContainers[i].Quantity;
                        this.ItemContainers[i] = null;
                        if (quantityToRemove <= 0)
                        {
                            return;
                        }
                    }
                    else
                    {
                        this.ItemContainers[i].Quantity -= quantityToRemove;
                        return;
                    }
                }
            }
        }
        public void RemoveAt(IntVector2 cellPosition)
        {
            this.ItemContainers[this.getIndex(cellPosition)] = null;
        }
		public void RemoveQuantityAt(IntVector2 cellPosition, int quantity)
		{
			int index = this.getIndex(cellPosition);
			ItemStack stack = this.ItemContainers[index];
			if (stack.IsEmpty())
			{
				throw new Exception();
			}
			if (stack.Quantity < quantity)
			{
				throw new Exception();
			}
			if (stack.Quantity == quantity)
			{
				this.ItemContainers[index] = null;
			}
			else
			{
				this.ItemContainers[index].Quantity -= quantity;
			}
		}
		public IEnumerable<IntVector2> GetAllSlots()
		{
			for (int i = 0; i < this.ItemContainers.Length; i++)
			{
				yield return new IntVector2(i % this.Width, i / this.Width);
			}
		}

		public IEnumerable<ItemStack> GetAllNonEmptyItemStacks()
		{
			for (int i = 0; i < this.ItemContainers.Length; i++)
			{
				ItemStack stack = this.ItemContainers[i];
				if (!stack.IsEmpty())
				{
					yield return stack;
				}
			}
		}
		public Guid ToCom(List<ComItemSet> allItemSet, Guid guid)
		{
			ComItemSet com = new ComItemSet();
			com.Id = guid;
			List<ComItemStack> comItemContainers = new List<ComItemStack>();			
            for (int i = 0; i < this.ItemContainers.Length; i++)
            {
				ItemStack stack = GetBagItem(i);
				if (stack == null)
				{
					comItemContainers.Add(null);
				}
				else
				{
					ComItemStack comItemStack = new ComItemStack();
					comItemStack.Item = Item.ToComItem(stack.Item);
					comItemStack.Quantity = stack.Quantity;
					comItemContainers.Add(comItemStack);
				}
			}
			com.ItemContainers = comItemContainers.ToArray();
			allItemSet.Add(com);
			return com.Id;
		}

		private ComItemSet findComItemSet(ComItemSet[] allItemSet, Guid id)
		{
			if(id == Guid.Empty)
			{
				throw new Exception("Empty ItemSetGuid");				
			}
			foreach (ComItemSet comItemSet in allItemSet)
			{
				if (comItemSet.Id == id)
				{
					return comItemSet;
				}
			}
			throw new Exception("ItemSet not found: " + id);			
		}
		public void FillFromCom(ComItemSet[] allItemSet, Guid id)
		{
			ComItemSet comItemSet = findComItemSet(allItemSet, id);
			if (comItemSet == null)
			{
				return;
			}
			for (int i = 0; i < this.ItemContainers.Length && i < comItemSet.ItemContainers.Length; i++)
			{
				ItemStack stack = GetBagItem(i);
				if (stack != null)
				{
					throw new Exception("ItemStack not null");
				}
				ComItemStack comItemStack = comItemSet.ItemContainers[i];
				if (comItemStack == null)
				{
					continue;
				}
				Item item = Item.FromComItem(comItemStack.Item);
				this.ItemContainers[i] = new ItemStack(item, comItemStack.Quantity);
			}
		}

    }
}
