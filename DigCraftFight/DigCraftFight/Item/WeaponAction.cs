﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
    public class WeaponAction
    {

		public readonly Cooldown Cooldown;
		public readonly Bullet.BulletAttributes bulletAttributes;
		public readonly Func<IntVector2, IntVector2> bulletDirectionFunc;
		
        public WeaponAction(Cooldown cooldown, Bullet.BulletAttributes bulletAttributes, Func<IntVector2, IntVector2> bulletDirectionFunc)
        {
			this.Cooldown = cooldown;
			this.bulletAttributes = bulletAttributes;
			this.bulletDirectionFunc = bulletDirectionFunc;
            
        }
      
		public void TryActivate(World world, LongVector2 characterPosition, IntVector2 targetDirection)        
        {			
            if (!this.Cooldown.TryStartCooldown(world.Tick))
            {
                return;
            }			
			int range = bulletAttributes.Range;
			int speed = this.bulletAttributes.SpeedCoord;
			int randAngus = Tools.RandDiceZeroCentered(10);			
			IntVector2 bulletDirection = this.bulletDirectionFunc(Weapon.RotateCoord(targetDirection, randAngus));
			LongVector2 gunPosition = Tools.CenterCharacter(characterPosition);
			Bullet bullet = new Bullet(bulletAttributes, new LinearMovingPoint(gunPosition, bulletDirection, speed, range));
			world.Add(bullet);
        }		
    }
}
