﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class Recipe
    {
		
		public static Recipe GROUND_WATER_BRIDGE = new Recipe(ItemType.GROUND_WATER_BRIDGE, 1, 60 * 3, ItemType.WOOD_LOG, 3);

        public static Recipe BLOCK_BRICK_EARTH = new Recipe(ItemType.BLOCK_BRICK_EARTH, 1, 60 * 2, ItemType.BLOCK_EARTH, 1);
        public static Recipe GROUND_WOOD = new Recipe(ItemType.GROUND_WOOD, 1, 60 * 5, ItemType.WOOD_LOG, 1);
        public static Recipe BED = new Recipe(ItemType.BED, 1, 60 * 15, ItemType.WOOD_LOG, 10);
        public static Recipe DOOR = new Recipe(ItemType.DOOR, 1, 60 * 15, ItemType.WOOD_LOG, 2);
      
        public static Recipe TOOL_PICK1 = new Recipe(ItemType.TOOL_PICK1, 1, 60 * 60, ItemType.WOOD_LOG, 1);
		public static Recipe STATION_TABLE = new Recipe(ItemType.WORKSTATION_TABLE, 1, 60 * 20, ItemType.WOOD_LOG, 20, ItemType.BLOCK_STONE, 1);
        public static Recipe TOOL_PICK2 = new Recipe(ItemType.TOOL_PICK2, 1, 60 * 3, ItemType.WOOD_LOG, 1, ItemType.BONE, 1);
        public static Recipe TOOL_SPADE = new Recipe(ItemType.TOOL_SPADE, 1, 60 * 3, ItemType.WOOD_LOG, 3, ItemType.BLOCK_STONE, 2);
        public static Recipe WEAPON_FLAIL = new Recipe(Weapon.NewNoobFlail(), 1, 60 * 10, ItemType.WOOD_LOG, 1, ItemType.BLOCK_STONE, 3);

        public List<Tuple<Item, int>> Ingredients = new List<Tuple<Item, int>>();
        public Item Result;
		public int ResultQuantity;
		public int WorkTime;

		private Recipe(ItemType result, int resultQuantity, int workTime, ItemType ingredient, int quantity)
        : this (new object[]{result, resultQuantity, workTime, ingredient, quantity})
        {
            
        }

		public static Recipe GetRecipeForOreWorkStation(int lvl)
		{			
			return new Recipe(
				Item.New(ItemType.WORKSTATION_ORE, lvl), 1,
				60 * (10 * lvl + 1), 
				Item.New(ItemType.ORE, lvl), 5 * (lvl + 1));
			
		}
		public static Recipe GetRecipeForOreCraft(int craftType, int lvl)
		{
			return new Recipe(
				Item.New(ItemType.CRAFT_TEMPLATE, craftType, lvl), 1,
				60 * (5 * lvl + 1),
				Item.New(ItemType.ORE, lvl), 1 + 5 * lvl + 2 * craftType);

		}

		private static Item toItem(object o)
		{
			if (o is ItemType)
			{
				return Item.New((ItemType)o);
			}
			else
			{
				return (Item)o;
			}
		}
        private Recipe(params object[] ingredient_quantities)
        {
			this.Result = toItem(ingredient_quantities[0]);
			this.ResultQuantity = (int)ingredient_quantities[1];
			this.WorkTime = (int)ingredient_quantities[2];
			for (int i = 3; i < ingredient_quantities.Length; i += 2)
            {
                this.Ingredients.Add(new Tuple<Item, int>(toItem(ingredient_quantities[i]), (int)ingredient_quantities[i + 1]));
            }
            if (ingredient_quantities.Length % 2 != 1)
            {
                throw new ArgumentException();
            }
        }
		
        public bool TryCraft(ItemSet bag)
        {
            foreach (var tuple in this.Ingredients)
            {
                if (!bag.Contains(tuple.Item1, tuple.Item2))
                {
                    return false;
                }
            }
            if (!bag.TryAdd(Item.New(this.Result.ItemType), this.ResultQuantity))
            {
                return false;
            }
            foreach (var tuple in this.Ingredients)
            {
                bag.Remove(tuple.Item1, tuple.Item2);
            }
            return true;

        }
    }
}
