﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
    public class Weapon
    {

		public static Func<Item> NewNoobSword = () => Item.New(ItemType.WEAPON_SIMPLE_BULLET, 0, 1, "Knife", 0,		  GameDifficulty.GetWeaponDamage(0), 8, 12, 0, 0, 0);
		public static Func<Item> NewNoobFlail = () => Item.New(ItemType.WEAPON_TRIPLE_LINEAR_SHOOT, 1, 1, "Flail", 1, GameDifficulty.GetWeaponDamage(1), 30, 7, 0, 0, 0);

		private readonly bool isMobWeapon;
		private readonly ItemType? itemType;
		private readonly WeaponAction[] Actions;
		public readonly int TextureX;
		public readonly int TextureY;
		public readonly string DisplayName;
		public readonly int Level;
		public readonly double DamagePerSecond;
		public readonly double BulletDamage;
		public readonly int FirePeriodicity;
		public readonly int Range;
		public readonly int ExplosionRange;
		public readonly int Arg1;
		public readonly int Arg2;
		private readonly TextureRect textureRect;
		
		
		public int RangeInCoord { get { return this.Range * Tools.QuaterCellSizeInCoord; } }

		private const int speed_index = 5;


		private Weapon(bool isMobWeapon, ItemType itemType, int textureX, int textureY, string displayName, int level, double damagePerSecond, int firePeriodicity, int range, int explosionRange, int arg1, int arg2)
		{
			this.isMobWeapon = isMobWeapon;
			this.itemType = itemType;
			
			this.TextureX = textureX;
			this.TextureY = textureY;
			this.DisplayName = displayName;
			this.Level = level;
			this.DamagePerSecond = damagePerSecond;
			this.FirePeriodicity = firePeriodicity;			
			this.Range = range;
			this.ExplosionRange = explosionRange;
			this.Arg1 = arg1;
			this.Arg2 = arg2;
			
			this.BulletDamage = this.DamagePerSecond * this.FirePeriodicity / 60;
			if (this.ExplosionRange > 0)
			{
				this.BulletDamage *= 0.65;///AOE Malus
			}
			this.textureRect = TextureRect.NewItems32(this.TextureX, this.TextureY);
			if (itemType == ItemType.WEAPON_SIMPLE_BULLET)
			{
				var bulletAttributes = getBasicBulletAttributes();				
				this.Actions = new []{new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => d)};
			}
			else if (itemType == ItemType.WEAPON_TRIPLE_LINEAR_SHOOT)
			{
				var bulletAttributes = getBasicBulletAttributes();				
				this.Actions = new []
				{
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => d),
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => RotateCoord(d, 30)),
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes,  d => RotateCoord(d, -30))
				};
			}
			else if (itemType == ItemType.WEAPON_BULLET_RANDOM_DIRECTION)
			{
				var bulletAttributes = getBasicBulletAttributes();
				this.Actions = new[]
				{
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => RandomDirection())					
				};
			}
			else if (itemType == ItemType.WEAPON_CARDINAL_DIAGONAL_EXPLOSION)
			{
				var bulletAttributes = getBasicBulletAttributes();
				this.Actions = new[]
				{
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => new IntVector2(Tools.Cos45Cood, Tools.Sin45Cood)),
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => new IntVector2(-Tools.Cos45Cood, Tools.Sin45Cood)),
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => new IntVector2(Tools.Cos45Cood, -Tools.Sin45Cood)),
					new WeaponAction(new Cooldown(1, this.FirePeriodicity), bulletAttributes, d => new IntVector2(-Tools.Cos45Cood, -Tools.Sin45Cood))				
				};
			}
			else
			{
				throw new NotImplementedException("itemType: " + itemType);
			}

		}

		private Bullet.BulletAttributes getBasicBulletAttributes()
		{
			var bulletAttributes = new Bullet.BulletAttributes();
			bulletAttributes.Range = this.RangeInCoord;
			bulletAttributes.Damage = this.BulletDamage;
			bulletAttributes.ExplosionRange = this.ExplosionRange * Tools.QuaterCellSizeInCoord;			
			bulletAttributes.SpeedCoord = this.isMobWeapon ? 600 : 2000;
			bulletAttributes.TargetType = this.isMobWeapon ? TargetType.Player : TargetType.Monster;
			Rectangle sourceRectangle = new Rectangle(0, this.isMobWeapon ? 0 : 32, 32, 32);
			bulletAttributes.TextureRect = new TextureRect(TextureRect.SpriteFile.TilesetBullet32, sourceRectangle);
			return bulletAttributes;
		}

		
		private static bool isWeapon(ItemType itemType)
		{
			if (itemType == ItemType.WEAPON_SIMPLE_BULLET)
			{
				return true;				
			}
			if (itemType == ItemType.WEAPON_TRIPLE_LINEAR_SHOOT)
			{
				return true;				
			}
			return false;
		}
        public static Weapon TryGetWeapon(ItemType itemType, object[] parameters)
        {
			if (!isWeapon(itemType))
			{
				return null;
			}
			return new Weapon(false, itemType, (int)parameters[0], (int)parameters[1], (string)parameters[2], 
				(int)parameters[3], Tools.ContertToDouble(parameters[4]),
				(int)parameters[5], (int)parameters[6], (int)parameters[7], (int)parameters[8], (int)parameters[9]);            
        }
		public object[] getComParameter()
		{
			return new object[] { this.TextureX, this.TextureY, this.DisplayName, this.Level, this.DamagePerSecond, this.FirePeriodicity, this.Range, this.ExplosionRange, this.Arg1, this.Arg2 };
		}

		public static Weapon CreateMobWeapon(ItemType itemType, double damage, int bulletPeriodicity, int range)
		{
			Weapon weapon = new Weapon(true, itemType, 0, 0, "mob fake weapon", 0, damage, bulletPeriodicity, range, 0, 0, 0);
			return weapon;
		}
		

		public TextureRect GetTextureRect()
		{
			return this.textureRect;
		}

		public void TryActivate(World world, Character source, Character target)
        {			
			IntVector2 weaponDirection =  source.GetWeaponDirection(target);
			foreach (WeaponAction action in this.Actions)
			{
				action.TryActivate(world, source.Position, weaponDirection);
			}
        }

		public override string ToString()
		{
			return this.DisplayName + ", lvl " + this.Level + ", itemType: " + this.itemType;
		}
		public static IntVector2 RotateCoord(IntVector2 d, int angus)
		{
			int positiveAngus = ((angus % 360) + 360) % 360;
			int cos = Tools.CosCoord[positiveAngus];
			int sin = Tools.SinCoord[positiveAngus];
			return new IntVector2(
				 (d.X * cos + d.Y * sin) / 256,
				  (d.Y * cos - d.X * sin) / 256);
		}

		public static IntVector2 RandomDirection()
		{
			int angus = Tools.Rand.Next(360);
			var direction = new IntVector2(Tools.CosCoord[angus], Tools.SinCoord[angus]);
			return direction;
		}

    }
}
