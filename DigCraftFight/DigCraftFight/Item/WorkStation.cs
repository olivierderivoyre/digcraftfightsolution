﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigCraftFight
{
	public class WorkStation
	{
		/// <summary>
		/// For debug
		/// </summary>
		private ItemType itemType;
		public readonly ItemSet Bag = new ItemSet(7, 2);

		private List<Recipe> recipesLateLoaded = null;
		public List<Recipe> Recipes
		{
			get
			{
				if (this.recipesLateLoaded == null)
				{
					Recipe[] list = this.getRecipeForOreWorkStation();
					this.recipesLateLoaded = new List<Recipe>(list);
				}
				return this.recipesLateLoaded;
			}
		}

		public int CurrentCraftProgress;
		public List<Tuple<Recipe, int>> CraftList = new List<Tuple<Recipe, int>>();
		public readonly int OreLevel = -1;
		private Guid bagGuid;
		private Guid craftListGuid;
		private List<Drop> displayedDropList = new List<Drop>();
		private LongVector2? lastObservedPosition = null;

		private WorkStation(ItemType itemType, object[] parameters, params Recipe[] recipeList)
		{
			this.itemType = itemType;
			if (recipeList.Length > 0)
			{
				this.recipesLateLoaded = new List<Recipe>(recipeList);
			}
			if (parameters != null && parameters.Length > 0)
			{
				this.OreLevel = (int)parameters[0];
			}
			else if (itemType == ItemType.WORKSTATION_ORE)
			{
				throw new Exception();
			}

			if (parameters != null && parameters.Length > 1)
			{
				this.bagGuid = Guid.Parse((string)parameters[1]);
				this.craftListGuid = Guid.Parse((string)parameters[2]);
			}
			else
			{
				///New stuff
				this.bagGuid = Guid.NewGuid();
				this.craftListGuid = Guid.NewGuid();
			}
		}


		public static WorkStation TryGetWorkStation(ItemType itemType, object[] parameters)
		{
			if (itemType == ItemType.WORKSTATION_SMALL_TABLE)
			{
				return new WorkStation(itemType, parameters,
						Recipe.TOOL_PICK2,
						Recipe.WEAPON_FLAIL,
						Recipe.STATION_TABLE,
						Recipe.GetRecipeForOreWorkStation(0)
					);
			}
			if (itemType == ItemType.WORKSTATION_TABLE)
			{
				return new WorkStation(itemType, parameters,
						Recipe.TOOL_SPADE,
						Recipe.BLOCK_BRICK_EARTH,
						Recipe.GROUND_WOOD,
						Recipe.DOOR,
						Recipe.BED,
						Recipe.GetRecipeForOreWorkStation(0)
					);
			}

			if (itemType == ItemType.WORKSTATION_SMALL_TABLE_WATER)
			{
				return new WorkStation(itemType, parameters,
						Recipe.GROUND_WATER_BRIDGE
					);
			}
			if (itemType == ItemType.WORKSTATION_ORE)
			{
				return new WorkStation(itemType, parameters);
			}
			return null;
		}

		public object[] getComParameter()
		{
			return new object[] { this.OreLevel, this.bagGuid.ToString(), this.craftListGuid.ToString() };
		}

		public void fillComItemSet(List<ComItemSet> allItemSet)
		{
			this.Bag.ToCom(allItemSet, this.bagGuid);
			///Serialize as an itemSet.
			ItemSet fakeItemSet = new ItemSet(this.CraftList.Sum(t => t.Item2), 1);
			foreach (var task in this.CraftList)
			{
				fakeItemSet.TryAdd(task.Item1.Result, task.Item2);
			}
			fakeItemSet.ToCom(allItemSet, this.craftListGuid);
		}

		public void initFromComItemSet(ComItemSet[] allItemSet)
		{
			this.Bag.FillFromCom(allItemSet, this.bagGuid);
			ItemSet fakeItemSet = new ItemSet(10000, 1);
			fakeItemSet.FillFromCom(allItemSet, this.craftListGuid);
			foreach (var slot in fakeItemSet.GetAllSlots())
			{
				ItemStack stack = fakeItemSet.GetBagItem(slot);
				if (stack == null)
				{
					continue;
				}
				Recipe targetRecipe = this.Recipes.FirstOrDefault(r => Item.HaveSameAttributes(r.Result, stack.Item));
				if (targetRecipe != null)
				{
					this.CraftList.Add(new Tuple<Recipe, int>(targetRecipe, stack.Quantity));
				}
			}
		}

		private Recipe[] getRecipeForOreWorkStation()
		{
			if (this.OreLevel < 0)
			{
				throw new ArgumentException();
			}
			List<Recipe> recipeList = new List<Recipe>();

			recipeList.Add(Recipe.GetRecipeForOreCraft(0, this.OreLevel));
			recipeList.Add(Recipe.GetRecipeForOreCraft(1, this.OreLevel));
			recipeList.Add(Recipe.GetRecipeForOreCraft(2, this.OreLevel));
			recipeList.Add(Recipe.GetRecipeForOreCraft(3, this.OreLevel));

			recipeList.Add(Recipe.GetRecipeForOreWorkStation(this.OreLevel + 1));
			return recipeList.ToArray();
		}



		public bool HasIngredient(World world, Recipe recipe)
		{
			foreach (var ingredient in recipe.Ingredients)
			{
				if (!world.Bag.Contains(ingredient.Item1, ingredient.Item2))
				{
					return false;
				}
			}
			return true;
		}

		public bool TryEnqueueJob(World world, int recipeIndex)
		{
			Recipe recipe = this.Recipes[recipeIndex];
			if (!HasIngredient(world, recipe))
			{
				return false;
			}
			if (this.CraftList.Count >= 6)
			{
				if (this.CraftList.Last().Item1 != recipe)
				{
					return false;
				}
			}
			foreach (var ingredient in recipe.Ingredients)
			{
				world.Bag.Remove(ingredient.Item1, ingredient.Item2);
			}
			int quantity = 1;
			if (this.CraftList.Count != 0 && this.CraftList.Last().Item1 == recipe)
			{
				///Replace the last item
				quantity = this.CraftList.Last().Item2 + 1;
				this.CraftList.RemoveAt(this.CraftList.Count - 1);
			}
			this.CraftList.Add(new Tuple<Recipe, int>(recipe, quantity));
			return true;
		}

		public void CancelJob(World world, int jobIndex)
		{
			if (jobIndex >= 0 && jobIndex < this.CraftList.Count)
			{
				var job = this.CraftList[jobIndex];
				if (job != null && job.Item2 > 0)
				{
					foreach (var ingredient in job.Item1.Ingredients)
					{
						world.Bag.TryAdd(ingredient.Item1, ingredient.Item2 * job.Item2);
					}
				}
				this.CraftList.RemoveAt(jobIndex);
			}
		}

		public bool HasWork()
		{
			if (this.CraftList.Count == 0)
			{
				return false;
			}
			Recipe r = this.CraftList[0].Item1;
			if (!this.Bag.HasSpaceFor(r.Result, r.ResultQuantity))
			{
				return false;
			}
			return true;
		}

		public bool Work(World world, LongVector2 workStationPosition)
		{
			this.CurrentCraftProgress++;
			Recipe r = this.CraftList[0].Item1;
			if (this.CurrentCraftProgress >= r.WorkTime)
			{
				Item result = r.Result.CraftClone();
				if (this.Bag.TryAdd(result, r.ResultQuantity))
				{
					this.CurrentCraftProgress = 0;
					if (this.CraftList[0].Item2 <= 1)
					{
						this.CraftList.RemoveAt(0);
					}
					else
					{
						this.CraftList[0] = new Tuple<Recipe, int>(this.CraftList[0].Item1, this.CraftList[0].Item2 - 1);
					}
					world.Add(new SpeechBalloon(world, workStationPosition, result.GetTextureRect(), "Object crafted."));

					this.addDecorativeDrop(world, workStationPosition, result);
					return true;
				}
			}
			return false;
		}



		public void Update(World world, LongVector2 workStationPosition)
		{

			if (this.lastObservedPosition == null || this.lastObservedPosition != workStationPosition)
			{
				///Game start or workstation has just been placed on the ground
				this.lastObservedPosition = workStationPosition;
				this.refreshDecorativeDrops(world, workStationPosition);
			}


			Player p = world.GetAlivePlayerTopLeftInside(workStationPosition + new IntVector2(-4, -4) * Tools.QuaterCellSizeInCoord, 9 * Tools.QuaterCellSizeInCoord, 9 * Tools.QuaterCellSizeInCoord);
			if (p == null)
			{
				return;///no player to loot
			}
			///Loot all
			foreach (var slot in this.Bag.GetAllSlots())
			{
				ItemStack loot = this.Bag.GetBagItem(slot);
				if (!loot.IsEmpty())
				{
					int movedQuantity = world.MainBagAddByPlayerLoot(loot.Item, p, loot.Quantity);
					this.Bag.RemoveQuantityAt(slot, movedQuantity);
				}
			}
			this.refreshDecorativeDrops(world, workStationPosition);
		}

		private void refreshDecorativeDrops(World world, LongVector2 workStationPosition)
		{
			///Clean
			foreach (Drop drop in this.displayedDropList)
			{
				world.Remove(drop);
			}
			foreach (var slot in this.Bag.GetAllSlots())
			{
				ItemStack loot = this.Bag.GetBagItem(slot);
				if (!loot.IsEmpty())
				{
					int nbDropToDraw = getDecorativeDropNumber(loot.Quantity);
					for (int i = 0; i < nbDropToDraw; i++)
					{
						this.addDecorativeDrop(world, workStationPosition, loot.Item);
					}
				}
			}
		}

		private int getDecorativeDropNumber(int realQuantity)
		{
			if (realQuantity < 7)
			{
				return realQuantity;
			}
			if (realQuantity < 15)
			{
				return 7;
			}
			if (realQuantity < 50)
			{
				return 10;
			}
			if (realQuantity < 100)
			{
				return 13;
			}
			return 16;
		}

		private void addDecorativeDrop(World world, LongVector2 workStationPosition, Item result)
		{
			List<IntVector2> freeCellAroundList = new List<IntVector2>();
			foreach (IntVector2 point in Tools.AroundPoints)
			{
				LongVector2 cellPosition = workStationPosition + point * Tools.CellSizeInCoord;
				if (!world.Ground.HasBlock(cellPosition))
				{
					freeCellAroundList.Add(point);
				}
			}
			if (freeCellAroundList.Count == 0)
			{
				freeCellAroundList.AddRange(Tools.AroundPoints);
			}
			LongVector2 dropPosition = workStationPosition + Tools.RandPickOne(freeCellAroundList.ToArray()) * Tools.CellSizeInCoord + new IntVector2(Tools.Rand.Next(3), Tools.Rand.Next(3)) * Tools.QuaterCellSizeInCoord;
			Drop drop = Drop.CreateDecorativeDrop(world, result, dropPosition);
			this.displayedDropList.Add(drop);
			world.Add(drop);
		}




		public override string ToString()
		{
			return this.itemType.ToString() + (this.OreLevel >= 0 ? " " + this.OreLevel : "");
		}


	}
}
