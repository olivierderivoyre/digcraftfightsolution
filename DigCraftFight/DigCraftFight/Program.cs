using System;
using System.Diagnostics;

namespace DigCraftFight
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {			
			try
			{
				if (args.Length == 1)
				{
					DigCraftFightGame.DebugTestMap = int.Parse(args[0]);
				}
				using (DigCraftFightGame game = new DigCraftFightGame())
				{
					game.Run();
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex);
				System.Windows.Forms.MessageBox.Show(ex.ToString(), "Error");
			}
        }
    }
#endif
}

