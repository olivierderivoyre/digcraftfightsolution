﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	/// <summary>
	/// Tools to go on local map
	/// </summary>
	public class MapReferencial
	{

		private const int SizeInCell = 64;///Limited for perf
		private readonly LongVector2 TopLeft;


		public MapReferencial(LongVector2 center)
		{
			this.TopLeft = Tools.FloorToCell(center) - new IntVector2(Tools.CellSizeInCoord, Tools.CellSizeInCoord) * SizeInCell / 2;
		}

		public bool[] NewBool2DArray()
		{
			return new bool[SizeInCell * SizeInCell];
		}

		public IEnumerable<IntVector2> GetAllMapCoord()
		{
			for (int y = 0; y < MapReferencial.SizeInCell; y++)
			{
				for (int x = 0; x < MapReferencial.SizeInCell; x++)
				{					
					yield return new IntVector2(x, y);
				}
			}
		}

		public LongVector2 ToWorldPosition(int x, int y)
		{
			return this.TopLeft + new IntVector2(x, y) * Tools.CellSizeInCoord;
		}
		public LongVector2 ToWorldPosition(IntVector2 mapCoord)
		{
			return this.TopLeft + mapCoord * Tools.CellSizeInCoord;
		}

		public IEnumerable<LongVector2> GetAllWorldPositions()
		{
			for (int y = 0; y < MapReferencial.SizeInCell; y++)
			{
				for (int x = 0; x < MapReferencial.SizeInCell; x++)
				{
					LongVector2 p = this.TopLeft + new IntVector2(x, y) * Tools.CellSizeInCoord;
					yield return p;
				}
			}
		}

		public IntVector2 GetLocalCoord(LongVector2 localCoord)
		{
			return IntVector2.Delta(this.TopLeft, Tools.FloorToCell(localCoord)) / Tools.CellSizeInCoord;
		}

		public IntVector2 GetLocalCoordFromIndex(int i)
		{
			return new IntVector2(i % SizeInCell, i / SizeInCell);
		}
		public int GetIndex(IntVector2 localCoord)
		{
			return localCoord.Y * SizeInCell + localCoord.X;
		}
		public int GetLocalMapSize()
		{
			return MapReferencial.SizeInCell;
		}

		public IntVector2 GetARandomCoord()
		{
			return new IntVector2(Tools.Rand.Next(MapReferencial.SizeInCell), Tools.Rand.Next(MapReferencial.SizeInCell));
		}



		public bool IsInRange(LongVector2 position)
		{
			IntVector2 local = GetLocalCoord(position);
			return Tools.IsInRange(local.X, 0, SizeInCell)
				&& Tools.IsInRange(local.Y, 0, SizeInCell);
		}
	}
}
