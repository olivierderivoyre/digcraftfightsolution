﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace DigCraftFight
{
	public class Screen
	{

		private readonly SpriteBatch spriteBatch;
		public readonly Ressource Ressource;

		public LongVector2 TopLeft;
		public int WidthCoord;
		public int HeightCoord;


		public Screen(SpriteBatch spriteBatch, Ressource ressource, LongVector2 center, int pxWidth, int pxHeight)
		{
			this.spriteBatch = spriteBatch;
			this.Ressource = ressource;
			this.WidthCoord = pxWidth * Tools.PixelSizeInCoord;
			this.HeightCoord = pxHeight * Tools.PixelSizeInCoord;
			this.CenterOn(center);
		}

		public void CenterOn(LongVector2 center)
		{
			this.TopLeft = Tools.FloorToCell(center + new IntVector2(-this.WidthCoord / 2, -this.HeightCoord / 2));
		}

		public LongVector2 GetCenter()
		{
			return this.TopLeft + new IntVector2(WidthCoord, HeightCoord) / 2;
		}

		public void Update(int pxWidth, int pxHeight, Player p1, Player p2)
		{
			this.WidthCoord = pxWidth * Tools.PixelSizeInCoord;
			this.HeightCoord = pxHeight * Tools.PixelSizeInCoord;
			LongVector2 newTopLeft;

			LongVector2 p1Position = p1.Position;
			LongVector2 p2Position = p2.Position;
			if (p1.IsActive && !p2.IsActive)
			{
				p2Position = p1Position;
			}
			if (!p1.IsActive && p2.IsActive)
			{
				p1Position = p2Position;
			}
			newTopLeft.X = getNewBorder(this.TopLeft.X, this.WidthCoord, p1Position.X, p2Position.X);
			newTopLeft.Y = getNewBorder(this.TopLeft.Y, this.HeightCoord, p1Position.Y, p2Position.Y);
			this.TopLeft = newTopLeft;
		}

		private static long getNewBorder(long currentBorder, long size, long p1, long p2)
		{
			int nbCellBorder = 3;
			long minP = Math.Min(p1, p2);
			long maxP = Math.Max(p1, p2);
			long borderForLeft = minP - nbCellBorder * Tools.CellSizeInCoord;
			///minBorderForRight + width == player + 2 cells
			long borderForRight = maxP + nbCellBorder * Tools.CellSizeInCoord + 1 * Tools.CellSizeInCoord - size;

			long minBorder = Math.Min(borderForLeft, borderForRight);
			long maxBorder = Math.Max(borderForLeft, borderForRight);
			bool mustChangeForMax = currentBorder >= maxBorder;
			bool mustChangeForMin = currentBorder <= minBorder;
			if (!mustChangeForMax && !mustChangeForMin)
			{
				return currentBorder;///No need to change
			}
			else if (mustChangeForMax && mustChangeForMin)
			{
				return currentBorder;///Players disagree on direction
			}
			else if (mustChangeForMax)
			{
				return maxBorder;
			}
			else
			{
				return minBorder;
			}
		}

		public IntVector2 LimitPlayerMove(LongVector2 newPosition, IntVector2 direction)
		{
			IntVector2 constrainedDirection = direction;
			if (limitPlayerMove(this.TopLeft.X, this.WidthCoord, newPosition.X, direction.X))
			{
				constrainedDirection.X = 0;
			}
			if (limitPlayerMove(this.TopLeft.Y, this.HeightCoord, newPosition.Y, direction.Y))
			{
				constrainedDirection.Y = 0;
			}
			return constrainedDirection;
		}

		private static bool limitPlayerMove(long screen, long size, long player, int move)
		{
			if (move < 0)
			{
				return player < screen;
			}
			if (move > 0)
			{
				return player + 1 * Tools.CellSizeInCoord > screen + size;
			}
			return false;
		}



		public void Draw(Texture2D texture, LongVector2 destinationWorld, Rectangle sourceRectangle, Color color)
		{
			Rectangle destinationScreen = new Rectangle(
				Tools.WorldCoordToPixel(destinationWorld.X - this.TopLeft.X),
				 Tools.WorldCoordToPixel(destinationWorld.Y - this.TopLeft.Y),
				 sourceRectangle.Width, sourceRectangle.Height);

			this.spriteBatch.Draw(texture, destinationScreen, sourceRectangle, color);
		}
		public void Draw32(TextureRect textureRect, LongVector2 destinationWorld, Color color)
		{
			DrawScale(textureRect.GetTileset(this.Ressource), destinationWorld, 32, textureRect.Rectangle, color);
		}
		public void Draw64(TextureRect textureRect, LongVector2 destinationWorld, Color color)
		{
			DrawScale(textureRect.GetTileset(this.Ressource), destinationWorld, 64, textureRect.Rectangle, color);
		}

		public void Draw64(Texture2D texture, LongVector2 destinationWorld, Rectangle sourceRectangle, Color color)
		{
			DrawScale(texture, destinationWorld, 64, sourceRectangle, color);
		}
		public void DrawScale(Texture2D texture, LongVector2 destinationWorld, int size, Rectangle sourceRectangle, Color color)
		{
			Rectangle destinationScreen = new Rectangle(
				Tools.WorldCoordToPixel(destinationWorld.X - this.TopLeft.X),
				 Tools.WorldCoordToPixel(destinationWorld.Y - this.TopLeft.Y),
				 size, size);

			this.spriteBatch.Draw(texture, destinationScreen, sourceRectangle, color);
		}
		public void DrawString(string text, LongVector2 destinationWorld, Color color)
		{
			Vector2 destinationScreen = new Vector2(
				Tools.WorldCoordToPixel(destinationWorld.X - this.TopLeft.X),
				 Tools.WorldCoordToPixel(destinationWorld.Y - this.TopLeft.Y));
			this.spriteBatch.DrawString(this.Ressource.Font, text, destinationScreen, color);

		}

		public bool IsCharacterOutside(LongVector2 position)
		{
			return !position.IsInside(this.TopLeft - new IntVector2(1, 1) * Tools.CellSizeInCoord,
				this.WidthCoord + 2 * Tools.CellSizeInCoord,
				this.HeightCoord + 2 * Tools.CellSizeInCoord);
		}

		public void DrawIndicator(TextureRect textureRect, LongVector2 position, Color color)
		{
			if (!IsCharacterOutside(position))
			{
				return;
			}
			LongVector2? plot = null;
			LongVector2? xplot = this.getRadarCoordX(position);
			if (xplot != null)
			{
				plot = xplot;
			}
			else
			{
				LongVector2? yPlot = this.getRadarCoordY(position);
				if (yPlot != null)
				{
					plot = yPlot;
				}
			}
			if (plot != null)
			{
				this.DrawScale(this.Ressource.TilesetInterface40, plot.Value - new IntVector2(4,4) * Tools.PixelSizeInCoord, 24, new Rectangle(3 * 41, 2 * 41, 40, 40), color);
				this.DrawScale(textureRect.GetTileset(this.Ressource), plot.Value, 16, textureRect.Rectangle, Color.White);
			}
		}

		private LongVector2? getRadarCoordX(LongVector2 position)
		{	
			LongVector2 topLeft = this.TopLeft + new IntVector2(4,4) * Tools.PixelSizeInCoord;
			LongVector2 downRight = this.TopLeft + new IntVector2(this.WidthCoord, this.HeightCoord) - new IntVector2(4 + 16, 4 + 16) * Tools.PixelSizeInCoord;
			LongVector2 center = this.GetCenter();
			long xAxis;
			int centerCorectif = 1;
			if(position.X < topLeft.X)
			{
				xAxis = topLeft.X;
			} 
			else if(position.X > downRight.X)
			{
				xAxis = downRight.X;
				centerCorectif = -1;
			} else	{
				return null;
			}
			//  o
			//  |     |-------------|
			//  |_____|      .      |
			//        |-------------|
			//  w   xAxis  Center

			long radarYFromCenter = (position.Y - center.Y) * (topLeft.X - center.X) / (position.X - center.X);
			long radarYAbs = center.Y + centerCorectif * radarYFromCenter;
			if (radarYAbs < topLeft.Y || radarYAbs >= downRight.Y)
			{
				return null;
			}
			return new LongVector2(xAxis, radarYAbs);
		}

		private LongVector2? getRadarCoordY(LongVector2 position)
		{
			LongVector2 topLeft = this.TopLeft + new IntVector2(4, 4) * Tools.PixelSizeInCoord;
			LongVector2 downRight = this.TopLeft + new IntVector2(this.WidthCoord, this.HeightCoord) - new IntVector2(4 + 16, 4 + 16) * Tools.PixelSizeInCoord;
			LongVector2 center = this.GetCenter();
			long yAxis;
			int centerCorectif = 1;
			if (position.Y < topLeft.Y)
			{
				yAxis = topLeft.Y;
			}
			else if (position.Y > downRight.Y)
			{
				yAxis = downRight.Y;
				centerCorectif = -1;
			}
			else
			{
				return null;
			}
			long radarXFromCenter = (position.X - center.X) * (topLeft.Y - center.Y) / (position.Y - center.Y);
			long radarXAbs = center.X + centerCorectif * radarXFromCenter;
			if (radarXAbs < topLeft.X)
			{
				radarXAbs = topLeft.X;
			}
			if (radarXAbs >= downRight.X)
			{
				radarXAbs = downRight.X;
			}
			//if (radarXAbs < topLeft.X || radarXAbs >= downRight.X)
			//{
			//    return null;
			//}
			return new LongVector2(radarXAbs, yAxis);
		}
	}
}
