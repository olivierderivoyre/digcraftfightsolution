﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class FixedTargetMoveAnimation
    {
        private readonly LongVector2 initialPosition;
		private readonly LongVector2 targetPosition;
		private readonly IntVector2 sourceToDestination;
		private readonly int cellDuration;
		private readonly int totalTick;
		private int nbTick = 0;

		private FixedTargetMoveAnimation(LongVector2 initialPosition, LongVector2 targetPosition, int cellDuration)
        {
			this.targetPosition = targetPosition;
            this.initialPosition = initialPosition;
			this.sourceToDestination = new IntVector2((int)(this.targetPosition.X - this.initialPosition.X), (int)(this.targetPosition.Y - this.initialPosition.Y));
			this.cellDuration = cellDuration;
			int nbPx = Math.Max(Math.Abs(this.sourceToDestination.X), Math.Abs(this.sourceToDestination.Y));
			this.totalTick = nbPx * cellDuration / Tools.CellSizeInCoord;
			if (this.sourceToDestination == IntVector2.Zero)
			{
				throw new ArgumentException();
			}
        }

		public static FixedTargetMoveAnimation CreateQuarterCellMove(LongVector2 initialPosition, IntVector2 direction, int cellDuration)
		{
			return new FixedTargetMoveAnimation(initialPosition, initialPosition + direction * Tools.QuaterCellSizeInCoord, cellDuration);
		}
		public static FixedTargetMoveAnimation CreateTargetMove(LongVector2 initialPosition, LongVector2 targetPosition, int cellDuration)
		{

			return new FixedTargetMoveAnimation(initialPosition, targetPosition, cellDuration);
		}

        public bool IsFinish()
        {
            return this.nbTick >= this.totalTick;
        }

        public LongVector2 GetUpdatedPosition()
        {
            this.nbTick++;
			if (this.IsFinish())
			{
				return this.targetPosition;
			}
			return this.initialPosition + this.sourceToDestination * this.nbTick / this.totalTick;
        }

		public LongVector2 GetUpdatedPosition(ref int walkTickCount)
		{
			LongVector2 p = GetUpdatedPosition();
			if (this.nbTick % (this.cellDuration / 8) == 0)
			{
				walkTickCount++;
			}
			return p;
			
		}
    }
}
