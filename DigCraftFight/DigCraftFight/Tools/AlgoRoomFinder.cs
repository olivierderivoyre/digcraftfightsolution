﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace DigCraftFight
{
	public static class AlgoRoomFinder
	{

        public class RoomFinderReturn
        {
            public HashSet<IntVector2> RoomCells;
            public IntVector2? roomId;
        }


        public static IEnumerable GetRoomFromAnyCell(bool[] mapWall, int mapSize, IntVector2 anyCellInsideTheRoom, RoomFinderReturn roomFinderReturn)
		{
            yield return null;
			///We run the algo 2 times to be sure the result is the same for all cell inside the room.
			///
			///If you have a large room of size=240 (with a 160 room limit), 
			/// starting to a border will return that the room is too big 
			/// while starting on the center will say the room is ok.
			/// 
			/// We use roomId in the 2nd check to assert we use always the same referencial for all the cell in the room.			           
            foreach (object obj in GetRoom(mapWall, mapSize, anyCellInsideTheRoom, 330, roomFinderReturn))
            {
                yield return null;
            }
            yield return null;
            if (roomFinderReturn.RoomCells != null)
			{
                if (roomFinderReturn.roomId != null)
				{
                    foreach (object obj in GetRoom(mapWall, mapSize, roomFinderReturn.roomId.Value, 160, roomFinderReturn))
                    {
                        yield return null;
                    }
                    yield break;
				}
			}
            yield return null;
            roomFinderReturn.roomId = null;
            roomFinderReturn.RoomCells = null;          
		}

        private static IEnumerable GetRoom(bool[] mapWall, int mapSize, IntVector2 firstCellAfterDoor, int maxSize, RoomFinderReturn roomFinderReturn)
		{
            roomFinderReturn.roomId = null;
            roomFinderReturn.RoomCells = null;
			AlgoMapPaths map = new AlgoMapPaths(mapWall, mapSize, firstCellAfterDoor);
			HashSet<IntVector2> returned = new HashSet<IntVector2>();
			for (int y = 0; y < mapSize; y++)
			{
                yield return null;
				for (int x = 0; x < mapSize; x++)
				{
					int distance = map.GetDistance(x, y);
					if(distance < 0)
					{
						continue;
					}
					if (distance > maxSize)
					{
                        roomFinderReturn.RoomCells = null;
                        yield return null;
                        yield break;
					}
					returned.Add(new IntVector2(x, y));
                    if (roomFinderReturn.roomId == null)
					{
						if (!mapWall[y * mapSize + x])
						{
                            roomFinderReturn.roomId = new IntVector2(x, y);
						}
					}
				}
			}
            
            roomFinderReturn.RoomCells = returned;          
		}

	}
}
