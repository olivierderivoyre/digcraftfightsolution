﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	public interface IAlive
	{
		bool Update();	
		void Draw(Screen screen);
		LongVector2 Position { get; }
	}
}
