﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class Cooldown
    {
        private List<TickLimiter> timeLimiters = new List<TickLimiter>();

        public Cooldown(int maxOccuranceDuringTime, int timeRange)
            : this(new int[] { maxOccuranceDuringTime, timeRange })
        {
        }
        public Cooldown(params int[] tickLimiterParameters)
        {
            for (int i = 0; i < tickLimiterParameters.Length; i += 2)
            {
                this.Add(new TickLimiter(tickLimiterParameters[i], tickLimiterParameters[i + 1]));
            }
        }

        public void Add(TickLimiter limiter)
        {
            this.timeLimiters.Add(limiter);
        }
        public bool AllowAction(int now)
        {
            foreach (TickLimiter limiter in this.timeLimiters)
            {
                if(!limiter.AllowAction(now))
                {
                    return false;
                }
            }
            return true;
        }
        public void StartCooldown(int now)
        {
            foreach (TickLimiter limiter in this.timeLimiters)
            {
                limiter.StartCooldown(now);
            }
        }

        public bool TryStartCooldown(int now)
        {
            if (this.AllowAction(now))
            {
                this.StartCooldown(now);
                return true;
            }
            return false;
        }
    }
}
