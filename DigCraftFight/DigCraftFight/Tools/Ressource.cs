﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DigCraftFight
{
    public class Ressource
    {
        public readonly SpriteFont Font;
        public readonly Texture2D TilesetBullet32;
        public readonly Texture2D TilesetMob64;
        public readonly Texture2D TilesetGround;
        public readonly Texture2D TilesetItems32;
        public readonly Texture2D TilesetInterface40;

		public Dictionary<string, Texture2D> heroTexureList = new Dictionary<string, Texture2D>();

        public Ressource(ContentManager content)
        {
            this.Font = content.Load<SpriteFont>("font");
            this.TilesetBullet32 = content.Load<Texture2D>("Tileset/TilesetBullet32");
            this.TilesetMob64 = content.Load<Texture2D>("Tileset/TilesetMob64");
            this.TilesetGround = content.Load<Texture2D>("Tileset/TilesetGround64");
            this.TilesetItems32 = content.Load<Texture2D>("Tileset/TilesetItems32");
            this.TilesetInterface40 = content.Load<Texture2D>("Tileset/TilesetInterface40");

			foreach (string heroSprite in Hero.heroSprites)
			{
				Texture2D texture = content.Load<Texture2D>("Sprite/" + heroSprite);
				this.heroTexureList.Add(heroSprite, texture);
			}
        }
    }
}
