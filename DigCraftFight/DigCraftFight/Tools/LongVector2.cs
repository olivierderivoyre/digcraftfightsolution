﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight 
{
    [System.Diagnostics.DebuggerStepThrough]
	public struct LongVector2
	{
		public long X;
        public long Y;
        public LongVector2(long x, long y)
		{
			this.X = x;
			this.Y = y;
		}

		public static bool operator ==(LongVector2 v1, LongVector2 v2)
		{
			return v1.X == v2.X && v1.Y == v2.Y;
		}
		public static bool operator !=(LongVector2 v1, LongVector2 v2)
		{
			return !(v1 == v2);
		}
		public override bool Equals(object obj)
		{
			return obj is LongVector2 && this == (LongVector2)obj;
		}
		public override int GetHashCode()
		{
			return (int) (this.X + 104729 * this.Y);
		}
        public static LongVector2 operator +(LongVector2 v1, IntVector2 v2)
        {
            return new LongVector2(v1.X + v2.X , v1.Y + v2.Y);
        }
        public static LongVector2 operator -(LongVector2 v1, IntVector2 v2)
        {
            return new LongVector2(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static LongVector2 operator *(int i, LongVector2 v)
        {
            return new LongVector2(v.X * i, v.Y * i);
        }
        public static LongVector2 operator *(LongVector2 v, int i)
        {
            return new LongVector2(v.X * i, v.Y * i);
        }
        public static LongVector2 operator /(LongVector2 v, int i)
        {
            return new LongVector2(v.X / i, v.Y / i);
        }
        public static LongVector2 operator %(LongVector2 v, int i)
        {
            return new LongVector2(v.X % i, v.Y % i);
        }

        public bool IsInside(LongVector2 topLeft, int width, int height)
        {
            return this.X >= topLeft.X && this.X < topLeft.X + width &&
                this.Y >= topLeft.Y && this.Y < topLeft.Y + height;
        }
		public bool HitRectangle(LongVector2 topLeft, int width, int height, int hitRange)
		{
			return this.X + hitRange >= topLeft.X && this.X < topLeft.X + width + hitRange &&
				this.Y + hitRange >= topLeft.Y && this.Y < topLeft.Y + height + hitRange;
		}
        public override string ToString()
        {
            return Tools.CoordToString(this.X) + ", " + Tools.CoordToString(this.Y);
        }

	}
}
