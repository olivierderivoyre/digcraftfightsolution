﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    [System.Diagnostics.DebuggerStepThrough]
    public struct IntVector2
    {
        public static IntVector2 Zero = new IntVector2(0, 0);
        public int X;
        public int Y;

        public IntVector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public static bool operator ==(IntVector2 v1, IntVector2 v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }
        public static bool operator !=(IntVector2 v1, IntVector2 v2)
        {
            return !(v1 == v2);
        }
        public override bool Equals(object obj)
        {
            return obj is IntVector2 && this == (IntVector2)obj;
        }
        public override int GetHashCode()
        {
            return (int)(this.X + 104729 * this.Y);
        }
        public static IntVector2 operator *(int i, IntVector2 v)
        {
            return new IntVector2(v.X * i, v.Y * i);
        }
        public static IntVector2 operator *( IntVector2 v, int i)
        {
            return new IntVector2(v.X * i, v.Y * i);
        }
        public static IntVector2 operator /(IntVector2 v, int i)
        {
            return new IntVector2(v.X / i, v.Y / i);
        }
        public static IntVector2 operator +(IntVector2 v1, IntVector2 v2)
        {
            return new IntVector2(v1.X + v2.X, v1.Y + v2.Y);
        }
		public static IntVector2 Delta(LongVector2 from, LongVector2 to)
		{			
			return new IntVector2(checked((int)(to.X - from.X)), checked((int)(to.Y - from.Y)));
		}

		public int GetDistance()
		{
			return Math.Max(Math.Abs(this.X), Math.Abs(this.Y));
		}

        public override string ToString()
        {
            return this.X + ", " + this.Y;
        }
       
    }
}
