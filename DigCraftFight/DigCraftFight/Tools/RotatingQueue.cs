﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class RotatingQueue
    {
        private readonly int size;
        private readonly int[] items;
        private int lastItemIndex;

        public RotatingQueue(int size)
        {
            if (size <= 0)
            {
                throw new ArgumentException();
            }
            this.size = size;
            this.items = new int[size];
        }

        public int LastItem
        {
            get { return this.items[this.lastItemIndex]; }
        }

        public void Push(int value)
        {
            /// [0] = t3
            /// [1] = t4
            /// [last] = t1
            /// [2] = t2
            this.items[this.lastItemIndex] = value;
            this.lastItemIndex = (this.lastItemIndex + 1) % this.size;
        }
    }
}
