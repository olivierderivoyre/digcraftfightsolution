﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class Dijkstra
    {
        public static List<IntVector2> FindPath(bool[] map, int mapSize, IntVector2 start, IntVector2 end)
        {
            if (start == end)
            {
                return new List<IntVector2> { end };
            }
            Dictionary<IntVector2, IntVector2> previous = new Dictionary<IntVector2, IntVector2>(mapSize * 4);
            
            PriorityQueue<IntVector2> nodesToExplore = new PriorityQueue<IntVector2>();
            nodesToExplore.Enqueue(start, 0);
            
            while (!nodesToExplore.IsEmpty())
            {
                int distanceFromSource;
                IntVector2 current = nodesToExplore.Dequeue(out distanceFromSource);
                foreach (var direction in new int[][] {
                    new int[]{0,1}, new int[]{1,0}, new int[]{-1,0}, new int[]{0,-1},
                    new int[]{1,1}, new int[]{-1,1}, new int[]{-1,-1}, new int[]{1,-1}
                })
                {
                    IntVector2 next = current + new IntVector2(direction[0], direction[1]);
					if (!isInRange(mapSize, next))
					{
						continue;
					}
                    if (!canGo(map, mapSize, next))
                    {
						if (next != end)///Special feature: allow to go to a blocked item only if it is the destination in order to go in the neightbourhood of a station
						{
							continue;
						}
                    }
                    if (previous.ContainsKey(next))
                    {						
                        continue;///already visited
                    }
                    bool isDiagonalMove = direction[0] != 0 && direction[1] != 0;
                    if (isDiagonalMove)
                    {
                        IntVector2 verticalNeighbour = current + new IntVector2(direction[0], 0);
                        IntVector2 horizontalNeighbour = current + new IntVector2(0, direction[1]);
                        if (!canGo(map, mapSize, verticalNeighbour) || !canGo(map, mapSize, horizontalNeighbour))
                        {
                            continue;
                        }
                    }

                    previous.Add(next, current);
                    if (next == end)
                    {
                        return readPreviousPath(previous, mapSize, start, end);
                    }
                    else
                    {
                        nodesToExplore.Enqueue(next, distanceFromSource + (isDiagonalMove ? 14 : 10));
                    }
                }
            }
            return null;
        }


		private static bool isInRange(int mapSize, IntVector2 cell)
		{
			if (cell.X < 0 || cell.X >= mapSize || cell.Y < 0 || cell.Y >= mapSize)
			{
				return false;
			}
			return true;
		}
		private static bool canGo(bool[] map, int mapSize, IntVector2 cell)
        {
            if (map[cell.Y * mapSize + cell.X])
            {
                return false;
            }
            return true;
        }
        private static List<IntVector2> readPreviousPath(Dictionary<IntVector2, IntVector2> previous, int mapSize, IntVector2 start, IntVector2 end)
        {			
            List<IntVector2> r = new List<IntVector2>();
            int i = 0;
            int max = mapSize * mapSize;
            IntVector2 current = end;
            while (current != start)
            {
                if (i++ > max)
                {
                    throw new Exception();
                }
                r.Add(current);
                current = previous[current];
            }
            r.Reverse();
            return r;
        }
    }
}
