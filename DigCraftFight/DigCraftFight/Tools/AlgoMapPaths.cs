﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
	/// <summary>
	/// Compute a map of distance from a start point inside a 2D bit array map.
	/// </summary>
	public class AlgoMapPaths
	{

		private readonly int mapSize;
		private readonly int[] distance;
		private readonly IntVector2 start;
		private readonly IntVector2[] previous;

		/// <summary>
		/// The constructor method cost CPU.
		/// </summary>
		public AlgoMapPaths(bool[] map, int mapSize, IntVector2 start)
		{
			this.mapSize = mapSize;
			this.start = start;
			this.distance = new int[map.Length];
			this.previous = new IntVector2[map.Length];
			for (int i = 0; i < map.Length; i++)
			{
				this.distance[i] = -1;
				this.previous[i].X = -1;
				this.previous[i].Y = -1;
			}

			var nodesToExplore = new PriorityQueue<Tuple<IntVector2, IntVector2>>();
			nodesToExplore.Enqueue(new Tuple<IntVector2, IntVector2>(start, new IntVector2(-1,-1)), 0);

			while (!nodesToExplore.IsEmpty())
			{
				int distanceFromSource;
				Tuple<IntVector2, IntVector2> t = nodesToExplore.Dequeue(out distanceFromSource);
				IntVector2 current = t.Item1;
				IntVector2 previous = t.Item2;				
				
				int currentIndex = current.Y * mapSize + current.X;
				if (this.distance[currentIndex] != -1)
				{
					continue;///already visited
				}
				this.distance[currentIndex] = distanceFromSource;
				this.previous[currentIndex] = previous;
				if (current != start)///NPC can move out of a block.
				{
					if (!canGo(map, mapSize, current))
					{
						///Special trick to allow NPC to go near a block
						continue;
					}
				}
				foreach (var direction in new int[][] {
                    new int[]{0,1}, new int[]{1,0}, new int[]{-1,0}, new int[]{0,-1},
                    new int[]{1,1}, new int[]{-1,1}, new int[]{-1,-1}, new int[]{1,-1}
                })
				{
					IntVector2 next = current + new IntVector2(direction[0], direction[1]);
					if (!isInRange(mapSize, next))
					{
						continue;
					}
					int nextIndex = next.Y * mapSize + next.X;
					if (this.distance[nextIndex] != -1)
					{
						continue;///already visited
					}					
					bool isDiagonalMove = direction[0] != 0 && direction[1] != 0;
					if (isDiagonalMove)
					{
						IntVector2 verticalNeighbour = current + new IntVector2(direction[0], 0);
						IntVector2 horizontalNeighbour = current + new IntVector2(0, direction[1]);
						if (!canGo(map, mapSize, verticalNeighbour) || !canGo(map, mapSize, horizontalNeighbour))
						{
							continue;
						}
					}													
					nodesToExplore.Enqueue(new Tuple<IntVector2, IntVector2>(next, current), distanceFromSource + (isDiagonalMove ? 14 : 10));					
				}
			}

		}

		private static bool isInRange(int mapSize, IntVector2 cell)
		{
			if (cell.X < 0 || cell.X >= mapSize || cell.Y < 0 || cell.Y >= mapSize)
			{
				return false;
			}
			return true;
		}
		private static bool canGo(bool[] map, int mapSize, IntVector2 cell)
		{
			if (map[cell.Y * mapSize + cell.X])
			{
				return false;
			}
			return true;
		}

		public int GetDistance(IntVector2 cell)
		{
			return this.distance[cell.Y * mapSize + cell.X];
		}
		public int GetDistance(int x, int y )
		{
			return this.distance[y * mapSize + x];
		}
		public List<IntVector2> GetPath(IntVector2 destination)
		{
			if (this.distance[destination.Y * this.mapSize + destination.X] < 0)
			{
				return null;
			}
			List<IntVector2> r = new List<IntVector2>();
			int i = 0;
			int max = mapSize * mapSize;
			IntVector2 current = destination;
			while (current != this.start)
			{
				if (i++ > max)
				{
					throw new Exception();
				}
				r.Add(current);
				current = previous[current.Y * this.mapSize + current.X];
			}
			r.Reverse();
			return r;
		}
	}
}
