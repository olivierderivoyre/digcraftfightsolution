﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace DigCraftFight
{
    public class InputController
    {
        private int gamePadIndex;
        
        private Keys up;
        private Keys down;
        private Keys left;
        private Keys right;
        private Keys buttonA;
        private Keys buttonB;
        private Keys buttonX;
        private Keys buttonY;


		private int directInputButtonStart = Tools.GetAppSettingInt("DirectInput.GamePad.ButtonStart");
		private int directInputButtonA = Tools.GetAppSettingInt("DirectInput.GamePad.ButtonA");
		private int directInputButtonB = Tools.GetAppSettingInt("DirectInput.GamePad.ButtonB");
		private int directInputButtonX = Tools.GetAppSettingInt("DirectInput.GamePad.ButtonX");
		private int directInputButtonY = Tools.GetAppSettingInt("DirectInput.GamePad.ButtonY");



		private Keys getAppSettingKeys(string buttonName)
		{
			string key = (this.gamePadIndex == 0) ? "Keyboard.Player1." : "Keyboard.Player2.";
			key += buttonName;
			return Tools.GetAppSettingKeys(key);
		}
       


        public InputController(PlayerIndex playerIndex)
        {
            this.gamePadIndex = playerIndex == PlayerIndex.Two ? 1 : 0;
            this.up = this.getAppSettingKeys("Up");
            this.down = this.getAppSettingKeys("Down");
            this.left = this.getAppSettingKeys("Left");
            this.right = this.getAppSettingKeys("Right");
			this.buttonA = this.getAppSettingKeys("ButtonA");
			this.buttonB = this.getAppSettingKeys("ButtonB");
			this.buttonX = this.getAppSettingKeys("ButtonX");
			this.buttonY = this.getAppSettingKeys("ButtonY");
            
            this.ButtonPressedTaskList = new HasButtonPressed[]
            {
                this.HasButtonAPreseed,
                this.HasButtonBPreseed,
                this.HasButtonXPreseed,
                this.HasButtonYPreseed,               
            };           
        }

        public bool HasDirection(InputState inputState)
        {
            return GetMoveDirection(inputState) != IntVector2.Zero;
        }

        public IntVector2 GetMoveDirection(InputState inputState)
        {
            IntVector2 d = new IntVector2();
            GamePadState gamePad = inputState.PlayerGamePads[this.gamePadIndex];
            if (gamePad.ThumbSticks.Left.LengthSquared() > 0.01)
            {
                if (gamePad.ThumbSticks.Left.X < -0.2)
                {
                    d.X--;
                }
                else if (gamePad.ThumbSticks.Left.X > 0.2)
                {
                    d.X++;
                }
                if (gamePad.ThumbSticks.Left.Y < -0.2)
                {
                    d.Y++;
                }
                else if (gamePad.ThumbSticks.Left.Y > 0.2)
                {
                    d.Y--;
                }
            }
           
            if (gamePad.DPad.Left == ButtonState.Pressed)
            {
                d.X--;
            }
            else if (gamePad.DPad.Right == ButtonState.Pressed)
            {
                d.X++;
            }
            if (gamePad.DPad.Down == ButtonState.Pressed)
            {
                d.Y++;
            }
            else if (gamePad.DPad.Up == ButtonState.Pressed)
            {
                d.Y--;
            }
            
			var directInput = inputState.DirectInputJoystick[this.gamePadIndex];
			if (directInput.Left)
			{
				d.X--;
			}
			else if (directInput.Right)
			{
				d.X++;
			}
			if (directInput.Down)
			{
				d.Y++;
			}
			else if (directInput.Up)
			{
				d.Y--;
			}
			

            KeyboardState keyboard = inputState.Keyboard;
            if (keyboard.IsKeyDown(this.left))
            {
                d.X--;
            }
            if (keyboard.IsKeyDown(this.right))
            {
                d.X++;
            }
            if (keyboard.IsKeyDown(this.up))
            {
                d.Y--;
            }
            if (keyboard.IsKeyDown(this.down))
            {
                d.Y++;
            }

            return d;
        }
        public bool HasButtonAPreseed(InputState inputState)
        {
			return inputState.Keyboard.IsKeyDown(this.buttonA)
				|| inputState.PlayerGamePads[this.gamePadIndex].Buttons.A == ButtonState.Pressed
				|| inputState.DirectInputJoystick[this.gamePadIndex].IsButtonPressed(this.directInputButtonA);
			
        }
        public bool HasButtonBPreseed(InputState inputState)
        {
            return inputState.Keyboard.IsKeyDown(this.buttonB)
                || inputState.PlayerGamePads[this.gamePadIndex].Buttons.B == ButtonState.Pressed
				|| inputState.DirectInputJoystick[this.gamePadIndex].IsButtonPressed(this.directInputButtonB);
        }
        public bool HasButtonXPreseed(InputState inputState)
        {
            return inputState.Keyboard.IsKeyDown(this.buttonX)
                || inputState.PlayerGamePads[this.gamePadIndex].Buttons.X == ButtonState.Pressed
				|| inputState.DirectInputJoystick[this.gamePadIndex].IsButtonPressed(this.directInputButtonX);
        }
        public bool HasButtonYPreseed(InputState inputState)
        {
            return inputState.Keyboard.IsKeyDown(this.buttonY)
                || inputState.PlayerGamePads[this.gamePadIndex].Buttons.Y == ButtonState.Pressed
				|| inputState.DirectInputJoystick[this.gamePadIndex].IsButtonPressed(this.directInputButtonY);
        }
        public delegate bool HasButtonPressed(InputState inputState);
        public readonly HasButtonPressed[] ButtonPressedTaskList;

        public bool HasAnyActionButtonPressed(InputState inputState)
        {
            return this.ButtonPressedTaskList.Any(f => f(inputState));
        }


        public bool HasPausePressed(InputState inputState)
        {
            return inputState.Keyboard.IsKeyDown(Keys.Escape)
                || inputState.PlayerGamePads[this.gamePadIndex].Buttons.Start == ButtonState.Pressed
				|| inputState.DirectInputJoystick[this.gamePadIndex].IsButtonPressed(this.directInputButtonStart);
        }
        public IntVector2 GetOrientation(InputState inputState)
        {
            
            GamePadState gamePad = inputState.PlayerGamePads[this.gamePadIndex];
            Vector2 orientation = gamePad.ThumbSticks.Right;
            if (orientation.LengthSquared() > 0.01)
            {

                orientation.Normalize();
                return new IntVector2((int)(orientation.X * 256), (int)(orientation.Y * -256));
            }
            IntVector2 d = new IntVector2();
            KeyboardState keyboard = inputState.Keyboard;
            if (keyboard.IsKeyDown(this.left))
            {
                d.X--;
            }
            if (keyboard.IsKeyDown(this.right))
            {
                d.X++;
            }
            if (keyboard.IsKeyDown(this.up))
            {
                d.Y--;
            }
            if (keyboard.IsKeyDown(this.down))
            {
                d.Y++;
            }
            return d * 256;
        }
    }
}
