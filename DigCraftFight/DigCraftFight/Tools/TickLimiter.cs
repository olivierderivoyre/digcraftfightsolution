﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class TickLimiter
    {
        private readonly int maxOccuranceDuringTime;
        private readonly int timeRange;
        private int nbOccurance;
        private int lastTick;
        public TickLimiter(int maxOccuranceDuringTime, int timeRange)
        {
            this.maxOccuranceDuringTime = maxOccuranceDuringTime;
            this.timeRange = timeRange;            
        }

        public bool AllowAction(int now)
        {
            if (this.nbOccurance < this.maxOccuranceDuringTime)
            {
                return true;
            }
            return now - this.lastTick >= this.timeRange;
        }
        public void StartCooldown(int now)
        {
            if (now - this.lastTick >= this.timeRange)
            {
                this.nbOccurance = 1;
                this.lastTick = now;
            }
            else
            {
                this.nbOccurance++;              
            }
           
        }
    }
}
