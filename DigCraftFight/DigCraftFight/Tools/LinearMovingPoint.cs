﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigCraftFight
{
    public class LinearMovingPoint
    {
        private readonly LongVector2 initialPosition;
        private readonly IntVector2 directionInCoord;
        private readonly int speedInCoord;
        private readonly int rangeInCoord;
        private readonly int maxTick;
        public LongVector2 Position;
        private int tick = 0;
        private int testedDistance = 0;
        

        public LinearMovingPoint(LongVector2 initialPosition, IntVector2 directionInCoord, int speedInCoord, int rangeInCoord)
        {
            this.initialPosition = initialPosition;
            this.Position = initialPosition;
            this.directionInCoord = directionInCoord;
            this.speedInCoord = speedInCoord;
            this.rangeInCoord = rangeInCoord;
            this.maxTick = this.rangeInCoord / this.speedInCoord;
            if (this.speedInCoord <= 0)
            {
                throw new ArgumentException();
            }
        }

        public bool IsFisnish()
        {
            return this.tick >= this.maxTick;
        }

        public void UpdateInitIterate()
        {
            this.tick++;
        }

        public bool UpdateIterate()
        {
            int nextTestedDistance = this.testedDistance + 8 * Tools.PixelSizeInCoord;
            if (nextTestedDistance >= this.speedInCoord * this.tick)
            {
                this.Position = this.initialPosition + this.directionInCoord * this.speedInCoord * this.tick / 256;
                return false;
            }
            else
            {
                this.testedDistance = nextTestedDistance;
                this.Position = this.initialPosition + this.directionInCoord * this.testedDistance / 256;
                return true;
            }
        }
    }
}
