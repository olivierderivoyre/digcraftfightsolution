﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DigCraftFight
{
    public class TextureRect
    {
        public enum SpriteFile { Undefined, TilesetBullet32, TilesetMob64,TilesetGround, TilesetItems32, TilesetInterface40 };
		public readonly SpriteFile spriteFile;
		private readonly Hero hero;
		public readonly Rectangle Rectangle;
        
       
        public Texture2D GetTileset(Ressource ressource)
        {
			if (this.hero != null)
            {
				return this.hero.GetSprite(ressource);
            }
            switch (this.spriteFile)
            {
                case SpriteFile.TilesetBullet32: return ressource.TilesetBullet32;
                case SpriteFile.TilesetMob64: return ressource.TilesetMob64;
                case SpriteFile.TilesetGround: return ressource.TilesetGround;
                case SpriteFile.TilesetItems32: return ressource.TilesetItems32;
                case SpriteFile.TilesetInterface40: return ressource.TilesetInterface40;
            }
            throw new NotImplementedException(this.spriteFile.ToString());
        }

        public TextureRect(SpriteFile spriteFile, Rectangle rectangle)
        {
            this.spriteFile = spriteFile;
            this.Rectangle = rectangle;
        }
        public TextureRect(Hero hero)
        {
			this.hero = hero;
            this.Rectangle = new Rectangle(0, 0, 64, 64);
        }
        public static TextureRect NewGround(int x, int y)
        {
            return new TextureRect(SpriteFile.TilesetGround, new Rectangle(x * 64, y * 64, 64, 64));
        }
        public static TextureRect NewItems32(int x, int y)
        {
            return new TextureRect(SpriteFile.TilesetItems32, new Rectangle(x * 32, y * 32, 32, 32));
        }
    }
}
