﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace DigCraftFight
{
    public static class Tools
    {
        public static readonly Random Rand = new Random();

        public const int PixelSizeInCoord = 256;
        public const int CellSizeInCoord = 64 * PixelSizeInCoord;
        public const int QuaterCellSizeInCoord = 16 * PixelSizeInCoord;

        public static int WorldCoordToPixel(long l)
        {
            return (int)(l >> 8);
        }
        public static long WorldCoordToCell(long l)
        {
            return l / CellSizeInCoord;
        }
		/// <summary>
		/// Top left coord of the cell for the position
		/// </summary>
		public static LongVector2 FloorToCell(LongVector2 position)
		{
			return (position / CellSizeInCoord) * CellSizeInCoord;
		}
		/// <summary>
		/// Round to the cell the character seem to be.
		/// </summary>
		public static LongVector2 CenterCharacterCell(LongVector2 topLeft)
        {
            return FloorToCell(topLeft + new IntVector2(Tools.CellSizeInCoord / 2,Tools.CellSizeInCoord / 2));
        }
		/// <summary>
		/// Center coord of the character
		/// </summary>
		public static LongVector2 CenterCharacter(LongVector2 topLeft)
		{
			return topLeft + new IntVector2(Tools.CellSizeInCoord / 2, Tools.CellSizeInCoord / 2);
		}
        public static string CoordToString(long x)
        {
            int subPixel = (int)(x % PixelSizeInCoord);
            int pixelInCell = (int)(x % CellSizeInCoord) / PixelSizeInCoord;
            int cell = (int)(x % PlotOfLand.SizeInCoord) / CellSizeInCoord;
            long screen = x / PlotOfLand.SizeInCoord;
            return screen + "." + cell + "." + pixelInCell + "." + subPixel;
        }

        public static int InRange(int value, int minInclude, int maxInclude)
        {
            return Math.Min(Math.Max(value, minInclude), maxInclude);
        }
		public static bool IsInRange(int value, int minInclude, int maxNotInclude)
		{
			return minInclude <= value && value < maxNotInclude;
		}

		public static bool RandomWin(int proba)
		{
			return Tools.Rand.Next(proba) == 0;
		}
		/// <summary>
		/// Return a number between [nbDices, nbDices*diceFaces]
		/// </summary>
		public static int ThrowDices(int nbDices, int diceFaces)
		{
			int r = 0;
			for (int i = 0; i < nbDices; i++)
			{
				r += 1 + Tools.Rand.Next(diceFaces);
			}
			return r;
		}
		public static int RandDiceZeroCentered(int maxValue)
		{
			return ThrowDices(maxValue, 3) - 2 * maxValue;
		}

		public static T RandPickOne<T>(params T[] args)
		{
			if (args.Length == 0)
			{
				return default(T);
			}
			return args[Tools.Rand.Next(args.Length)];
		}

		public static readonly int Cos15Cood = (int)(256 * Math.Cos(5 * Math.PI / 180));
        public static readonly int Sin15Cood = (int)(256 * Math.Sin(5 * Math.PI / 180));
        public static readonly int Cos45Cood = (int)(256 * Math.Cos(45 * Math.PI / 180));
        public static readonly int Sin45Cood = (int)(256 * Math.Sin(45 * Math.PI / 180));

        public static readonly int[] CosCoord = Enumerable.Range(0, 360).Select(i => (int)(256 * Math.Cos(i * Math.PI / 180))).ToArray();
        public static readonly int[] SinCoord = Enumerable.Range(0, 360).Select(i => (int)(256 * Math.Sin(i * Math.PI / 180))).ToArray();

		public static string SaveDirectory = Path.Combine(Path.GetTempPath(), "DigCraftFight");

		/// <summary>
		/// Right, Up, Left and  Down
		/// </summary>
		public static readonly IntVector2[] CardinalPoints = new[] { new IntVector2(1, 0), new IntVector2(0, 1), new IntVector2(-1, 0), new IntVector2(0, -1) };
		public static readonly IntVector2[] DiagonalPoints = new[] { new IntVector2(1, 1), new IntVector2(-1, 1), new IntVector2(-1, -1), new IntVector2(1, -1) };
		/// <summary>
		/// The 3x3 points excluding center
		/// </summary
		public static readonly IntVector2[] NeighborgPoints = CardinalPoints.Union(DiagonalPoints).ToArray();
		/// <summary>
		/// The 3x3 points including center
		/// </summary>
		public static readonly IntVector2[] AroundPoints = NeighborgPoints.Union(new []{new IntVector2(0,0)}).ToArray();


		public static double ContertToDouble(object o)
		{
			if (o is double)
			{
				return (double)o;
			}
			if (o is float)
			{
				return (float)o;
			}
			if (o is int)
			{
				return (int)o;
			}
			throw new ArgumentException();
		}

		public static string GetAppSetting(string keyName)
		{
			string v = System.Configuration.ConfigurationManager.AppSettings[keyName];
			if (string.IsNullOrEmpty(v))
			{
				throw new ApplicationException("Empty AppSettings '" + keyName + "'");
			}
			return v;
		}
		public static int GetAppSettingInt(string keyName)
		{
			int v;
			if(!int.TryParse(GetAppSetting(keyName), out v))
			{
				throw new ApplicationException("Unable to convert to int AppSettings '" + keyName + "'");
			}
			return v;
		}

		private static string keyboardLayoutName = null;
		const int KL_NAMELENGTH = 9;
		/// <summary>
		/// http://stackoverflow.com/questions/869609/keyboard-type-qwerty-or-dvorak-detection
		/// </summary>
		[System.Runtime.InteropServices.DllImport("user32.dll")]
		private static extern long GetKeyboardLayoutName(System.Text.StringBuilder pwszKLID);

		private static Keys[] getWasdKeys()
		{
			if (Tools.keyboardLayoutName == null)
			{
				try
				{
					StringBuilder name = new StringBuilder(KL_NAMELENGTH);
					GetKeyboardLayoutName(name);
					Tools.keyboardLayoutName = name.ToString();
				}
				catch (Exception ex)
				{
					Trace.WriteLine(ex);
				}
			}
			Keys[] WASDERT = new[] { Keys.W, Keys.Q, Keys.S, Keys.D, Keys.E, Keys.R, Keys.T };
			if (Tools.keyboardLayoutName == "0000040C" || Tools.keyboardLayoutName == "0000080c")///http://stackoverflow.com/questions/23616205/how-to-detect-keyboard-layout
			{
				WASDERT = new[] { Keys.Z, Keys.Q, Keys.S, Keys.D, Keys.E, Keys.R, Keys.T };
			}
			return WASDERT;
		}


		public static Keys GetAppSettingKeys(string keyName)
		{
			string keyValue = GetAppSetting(keyName);
			if (keyValue.StartsWith("WASD"))
			{
				Keys[] WASDERT = getWasdKeys();
				switch (keyValue)
				{
					case "WASD-UP" : return WASDERT[0];
					case "WASD-LEFT": return WASDERT[1];
					case "WASD-DOWN": return WASDERT[2];
					case "WASD-RIGHT": return WASDERT[3];
					case "WASD-A1": return Keys.Space;
					case "WASD-A2": return WASDERT[4];
					case "WASD-A3": return WASDERT[5];
					case "WASD-A4": return WASDERT[6];
					default: throw new ApplicationException("Unable to convert to WASD keys the AppSettings '" + keyName + "': " + keyValue);
				}
			}
			Keys v;
			if (!Enum.TryParse(keyValue, true, out v))
			{
				throw new ApplicationException("Unable to convert to keys the AppSettings '" + keyName + "': " + keyValue);
			}
			return v;
		}



		/// <summary>
		/// Compute a transform-matrix to stretch to the screen my fixed-size zone.
		/// 
		/// If the ratio does not fit the screen, it will add a black band in order to not deform the image.				
		/// </summary>
		public static Matrix GetTransformMatrixForMyBound(int desiredWidth, int desiredHeight, int screenWidth, int screenHeight)
		{

			///Assuming we want to emulate a screen size of 1x1
			float widthScale = ((float)screenWidth) / desiredWidth;
			float heightScale = ((float)screenHeight) / desiredHeight;
			///Does the screen is larger or longer?
			bool isLargerThanLonger = widthScale > heightScale;
			float offerSetX = 0;
			float offerSetY = 0;
			float scale;
			if (isLargerThanLonger)
			{
				///We scale until the Height take full space
				scale = heightScale;
				/// We will have vertical black-bar (at left and right)
				offerSetX = (screenWidth - desiredWidth * scale) / 2;
			}
			else
			{
				///Horizontal bar (at top and bottom)
				scale = widthScale;
				offerSetY = (screenHeight - desiredHeight * scale) / 2;
			}
			Matrix transform = Matrix.CreateTranslation(offerSetX, offerSetY, 0);
			transform = Matrix.CreateScale(scale) * transform;
			return transform;
		}


		public static IEnumerable<IntVector2> GetSpiralInterator(int maxdistance)
		{
			IntVector2 current = new IntVector2(0, 0);
			yield return current;
			int size = 0;
			IntVector2 direction;
			while(size < maxdistance * 2)
			{
				size++;
				direction = new IntVector2(1, 0);
				for(int i = 0; i < size; i++)
				{
					current += direction;
					yield return current;
				}
				direction = new IntVector2(0,-1);
				for (int i = 0; i < size; i++)
				{
					current += direction;
					yield return current;
				}
				size++;
				direction = new IntVector2(-1, 0);
				for (int i = 0; i < size; i++)
				{
					current += direction;
					yield return current;
				}
				direction = new IntVector2(0, 1);
				for (int i = 0; i < size; i++)
				{
					current += direction;
					yield return current;
				}				
			}
		}
		
    }
}
